var subdoshmvalues = [];
$(function() {

    $(".loadericon").hide();

    if (localStorage.editphase6data == undefined) {
        geteditphase6data();
    } else {
        var currentdate = new Date().getDate();
        if (currentdate != localStorage.datastoreddate_ep6) {
            localStorage.removeItem("editphase6data");
            geteditphase6data();
        } else {
            geteditp6datafromlocal(1); //this fn can happen 
        }
    }

    usergivendata();

}); //initial fn ends here

// geteditphase6data fn starts here
function geteditphase6data() {

    $.ajax({
        url: editprofilep6_dd_api,
        type: 'get',
        headers: {
            "Authorization": 'Token ' + localStorage.wutkn + ''
        },
        success: function(data) {

            localStorage.editphase6data = JSON.stringify(data);
            localStorage.datastoreddate_ep6 = new Date().getDate();

            geteditp6datafromlocal(0); //this fn is common for all
        },
        error: function(edata) {
            console.log("error occured in get edit phase 6 dd and others data");
        }
    });

} // geteditphase6data fn ends here

// localstorage data processing
function geteditp6datafromlocal(type) {

    var data = JSON.parse(localStorage.editphase6data);

    //relogion data - dyn
    $("#ep6_religion").empty();
    for (var i = 0; i < data.religion.length; i++) {
        $("#ep6_religion").append("<option value='" + data.religion[i].id + "'>" + data.religion[i].name + "</option>");
    }

    //mother tongue dyn loading
    $("#ep6_mothertongue").empty();
    for (var i = 0; i < data.mother_tongue.length; i++) {
        $("#ep6_mothertongue").append("<option value='" + data.mother_tongue[i].id + "'>" + data.mother_tongue[i].name + "</option>");
    }

    //caste dyn append
    $('#ep6_caste').empty().append('<option value="null">Select Your Caste</option>');
    $('#ep6_subcaste').empty().append('<option value="null">Select Your Subcaste</option>')
    for (var i = 0; i < data.caste.length; i++) {
        $('#ep6_caste').append("<option value='" + data.caste[i].id + "'>" + data.caste[i].name + "</option>");
    }
    localStorage.selectedcastecode = data.caste[0].id;

    //star dyn append
    $('#ep6_star').empty().append('<option value="null">Select Your Star</option>');
    $('#ep6_raasi').empty().append('<option value="null">Select Your Raasi</option>');
    for (var i = 0; i < data.star.length; i++) {
        $('#ep6_star').append("<option value='" + data.star[i].id + "'>" + data.star[i].name + "</option>");
    }
    localStorage.selectedstarcode = data.star[0].id;

    $(".dynamicdoshamyes").empty();
    for (var i = 0; i < data.dosham.length; i++) {
        $(".dynamicdoshamyes").append("<label class='control control--checkbox displayinline doshacheckbox'>" + data.dosham[i].name + "<input type='checkbox' name='ep6_doshamsub' attrval='" + data.dosham[i].id + "' value='" + data.dosham[i].id + "' /><div class='control__indicator'></div></label>");
    }

} // localstorage data processing

//get edit phase 6 details from local
function usergivendata() {

    var data = JSON.parse(localStorage.myprofiledata);

    $('#ep6_religion option[value="' + data.userdetails.religion.id + '"]').attr("selected", true);
    $('#ep6_mothertongue option[value="' + data.userdetails.mother_tongue.id + '"]').attr("selected", true);

    //if user already selected the caste
    if (data.user_religion_details != null) {

        localStorage.selectedcastecode = data.user_religion_details.caste.id;

        $('#ep6_caste option[value="' + data.user_religion_details.caste.id + '"]').attr("selected", true);

        if (data.user_religion_details.subcaste != null) {
            localStorage.userselectedsubcaste = data.user_religion_details.subcaste.id;
            getsubcastelist(0);
        }

        //gothram , dosham dyn loading
        if (data.gothram != null) {
            $("#ep6_gothram").val(data.user_religion_details.gothram);
        }
        //check whether the user clicked dosham - yes when they registered
        if (data.user_religion_details.dosham_choices != null) {
            $('input:radio[name="ep6_dosham"]').filter('[value="' + data.user_religion_details.dosham_choices + '"]').attr('checked', true);
        }
    }


    //checking the sub doshams dynamically if user clicked the dosham - yes
    if ($(".doshamyestoshow").is(":checked") == true) {
        $(".doshamsection").show();
        for (var i = 0; i < data.user_dosham.length; i++) {
            $('input:checkbox[name="ep6_doshamsub"]').filter('[value="' + data.user_dosham[i].dosham.id + '"]').attr('checked', true);
        }
    }

    //if user already selected the star
    if (data.religion_info != null) {
        localStorage.selectedstarcode = data.religion_info.star.id;
        $('#ep6_star option[value="' + data.religion_info.star.id + '"]').attr("selected", true);
        localStorage.userselectedraasi = data.religion_info.raasi.id;
        getraasilist(0);

    }


}

//caste change <-> subcaste change fn , star change <-> raasi change fn
$("#ep6_caste").change(function() {
    if ($("#ep6_caste").val() != "null") {
        localStorage.selectedcastecode = $("#ep6_caste").val();
        getsubcastelist(1);
    } else {
        $("#ep6_subcaste option").removeAttr("selected");
        $("#ep6_subcaste option[value='" + null + "']").attr("selected", "selected");
        $("#select2-ep6_subcaste-container").text("Select Your Subcaste");
    }

});
$("#ep6_star").change(function() {

    if ($("#ep6_star").val() != "null") {
        localStorage.selectedstarcode = $("#ep6_star").val();
        getraasilist(1);
    } else {
        $("#ep6_raasi option").removeAttr("selected");
        $("#ep6_raasi option[value='" + null + "']").attr("selected", "selected");
        $("#select2-ep6_raasi-container").text("Select Your Raasi");
    }

});

//get sucastes list fn starts here
function getsubcastelist(type) {
    $.ajax({
        url: geteditphase6subcastedata_api + localStorage.selectedcastecode + '/',
        type: 'get',
        success: function(data) {
            //states dyn append
            $('#ep6_subcaste').empty();
            for (var i = 0; i < data.length; i++) {
                $("#ep6_subcaste").append("<option value='" + data[i].id + "'>" + data[i].name + "</option>");
            }

            //if type == 0 then it comes from initial fn to dynamically select the user choosen data
            if (type == 0) {
                $('#ep6_subcaste option[value="' + localStorage.userselectedsubcaste + '"]').attr("selected", true);
            }

        },
        error: function(edata) {
            console.log("error occured in get edit phase 6 sucastes dd data");
        }
    });
} //get sucastes list fn ends here

//get raasi list fn starts here
function getraasilist(type) {
    $.ajax({
        url: geteditphase6raasidata_api + localStorage.selectedstarcode + '/',
        type: 'get',
        success: function(data) {
            //states dyn append
            $('#ep6_raasi').empty();
            for (var i = 0; i < data.length; i++) {
                $("#ep6_raasi").append("<option value='" + data[i].id + "'>" + data[i].name + "</option>");
            }
            //if type == 0 then it comes from initial fn to dynamically select the user choosen data
            if (type == 0) {
                if (localStorage.userselectedraasi != null) {
                    $('#ep6_raasi option[value="' + localStorage.userselectedraasi + '"]').attr("selected", true);
                }
            }
        },
        error: function(edata) {
            console.log("error occured in get edit phase 6 raasi dd data");
        }
    });
} //get sucastes list fn ends here

//form click fn to hide errors
$(".editphase6").click(function() {
    $("input,select,textarea").removeClass("iserror");
    $(".select2-selection__rendered").removeClass("iserror");
});

//send edit phase 6 data to server
function editp6_fn() {

    if ($("#ep6_caste").val() == '' || $("#ep6_caste").val() == null || $("#ep6_caste").val() == "null") {
        $("#snackbarerror").text("Caste is required");
        $('#select2-ep6_caste-container').addClass("iserror");
        $("html, body").animate({ scrollTop: 350 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('input[name="ep6_dosham"]:checked').length == 0) {
        $("#snackbarerror").text("Dosham Details is required");
        $("html, body").animate({ scrollTop: 350 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('input[name="ep6_dosham"]:checked').val() == "Yes" && $('input[name="ep6_doshamsub"]:checked').length == 0) {
        $("#snackbarerror").text("Dosham Sub Details is required");
        $("html, body").animate({ scrollTop: 450 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($("#ep6_star").val() == '' || $("#ep6_star").val() == null || $("#ep6_star").val() == "null") {
        $("#snackbarerror").text("Star is required");
        $('#select2-ep6_star-container').addClass("iserror");
        $("html, body").animate({ scrollTop: 450 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($("#ep6_raasi").val() == '' || $("#ep6_raasi").val() == null || $("#ep6_raasi").val() == "null") {
        $("#snackbarerror").text("Raasi is required");
        $('#select2-ep6_raasi-container').addClass("iserror");
        $("html, body").animate({ scrollTop: 450 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $('.loadericon').show();
    $(".updateBtn").attr("disabled", true);

    if ($('input[name="ep6_dosham"]:checked').val() == "Yes" && $('input[name="ep6_doshamsub"]:checked').length != 0) {
        for (var i = 0; i < $('input[name="ep6_doshamsub"]:checked').length; i++) {
            subdoshmvalues.push($('[name="ep6_doshamsub"]:checked').eq(i).val());
        }
    }

    // if ($("#ep6_subcaste").val() == null) {
    //     var subcaste = "";
    // }else{
    //     var subcaste = $("#ep6_subcaste").val();
    // }

    var Datatosend = {
        "religion": $("#ep6_religion").val(),
        "mother_tongue": $("#ep6_mothertongue").val(),
        "caste": $("#ep6_caste").val(),
        "subcaste": $("#ep6_subcaste").val(),
        "gothram": $("#ep6_gothram").val(),
        "dosham_choices": $('input[name="ep6_dosham"]:checked').val(),
        "user_dosham": subdoshmvalues,
        "star": $("#ep6_star").val(),
        "raasi": $("#ep6_raasi").val()
    }

    // if ($("#ep6_subcaste").val() == null) {
    //     delete Datatosend.subcaste;
    // }
    if ($("#ep6_gothram").val() == "") {
        delete Datatosend.gothram;
    }
    if ($('input[name="ep6_dosham"]:checked').val() != "Yes") {
        delete Datatosend.subdosham;
    }

    var postData = JSON.stringify(Datatosend);

    $.ajax({
        url: sendeditp6data_api,
        type: 'PUT',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {

            $('.loadericon').hide();
            $(".updateBtn").attr("disabled", false);

        },
        error: function(data) {

            $('.loadericon').hide();
            $(".updateBtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {
        // alert("success")
        window.location.href = "myprofile.html";
    });

} //send edit phase 6 data to server
