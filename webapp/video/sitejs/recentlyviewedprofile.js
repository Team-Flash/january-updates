$(function() { //initial fn starts here

    $(".suggestioncount,.loadmoreBtn,.loadericon,.loadericonsearch").hide();

    dynamicloadforsuggestion(0);


});

//list users - keywords fn starts here
function dynamicloadforsuggestion(type) {
    $("#loaderspinner").show();
    if (type == 0) {
        var url = recentlyviewdprofile_api;
    } else {
        var url = recentlyviewdprofile_api + "?page=" + sessionStorage.gotopage_no;
    }

    $.ajax({
        url: url,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {},
        error: function(data) {

            $(".loadmoreBtn").hide();
            $(".suggestioncount").show().text("(0)");

            if (type == 0) {
                $(".loadericonsearch").hide();
                $(".suggestionlist").empty().append('<div class="col-md-12 col-sm-12 col-xs-12 pr0"><center><img src="img/assets/nodata.svg" style="width:40%"></center></div>');
            } else {
                $('.loadericon').hide();
            }
            $(".dynpagination").empty();

        }
    }).done(function(data) {

        $(".loadmoreBtn").hide();
        $(".suggestionlist").empty();

        if (type == 0) {
            $(".loadericonsearch").hide();
            pagination(data.count);
        }

        if (data['results'].length == 0) {

            $(".suggestionlist").empty().append('<div class="col-md-12 col-sm-12 col-xs-12 pr0"><center><img src="img/assets/nodata.svg" style="width:40%"></center></div>');

            $(".suggestioncount").show().text("(0)");

            if (type == 1) {
                $('.loadericon').hide();
            }
             if (type == 0) { $(".dynpagination").empty(); }

        } else {

            if (data.next_url != null) {
                sessionStorage.ldmrthis = data.next_url;
                $(".loadmoreBtn").show();
            }

            for (var i = 0; i < data['results'].length; i++) {
                var dyn_img = (data['results'][i].userprofile.profile_pic == null) ? "img/assets/coupleindex.svg" : data['results'][i].userprofile.profile_pic;
                var gender = (data['results'][i].userdetails.gender == "Male") ? "Mr. " : "Ms. ";
                var usercity = (data['results'][i].user_address == null) ? "Not Mentioned" : data['results'][i].user_address.city.name;

                var liked = (data['results'][i].is_liked == true) ? "actionactive" : "";
                var shortlisted = (data['results'][i].is_shortlisted == true) ? "actionactive" : "";
                var ignored = (data['results'][i].is_ignored == true) ? "actionactive" : "";

                $(".suggestionlist").append('<div class="col-lg-2 col-md-2 col-sm-4 col-xs-6"><div class="card card-2 text-center heightauto"><div class="card__top"><a class="pointer" onclick="gotouserprofile(' + 1 + ',' + data['results'][i].id + ')" target="_blank" style="background: url(' + dyn_img + ') top center / cover no-repeat;display: block;height: 140px;"></a></div><div class="card__body p10"><p class="username pointer newmatchusername1' + data['results'][i].id + '" onclick="gotouserprofile(' + 1 + ',' + data['results'][i].id + ')">' + data['results'][i].first_name + '</p><p class="livesin">Lives in <span class="spanlivesin">' + usercity + '</span></p></div><div class="card__bottom text-center ptb3"><div class="card__action"><a class="pointer" onclick="acceptanddenyusers(this,' + data['results'][i].id + ',' + 2 + ',' + 1 + ')"><i class="fa fa-heart hoverpink accepticon interests ' + liked + '" aria-hidden="true"></i></a></div><div class="card__action"><a class="pointer" onclick="acceptanddenyusers(this,' + data['results'][i].id + ',' + 0 + ',' + 1 + ')"><i class="fa fa-star hoverpink accepticon interests ' + shortlisted + '" aria-hidden="true"></i></a></div><div class="card__action"><a class="pointer" onclick="acceptanddenyusers(this,' + data['results'][i].id + ',' + 1 + ',' + 1 + ')"><i class="fa fa-thumbs-down hoverblack rejecticon reject ' + ignored + '" aria-hidden="true"></i> </a></div></div></div></div>');
            }

            if (type == 1) {
                $('.loadericon').hide();
            } else {
                $(".loadericonsearch").hide();
            }

            $(".suggestioncount").show().text("(" +  data.count + ")");
        } //else cond ends here

        $("#loaderspinner").hide();
    });

} //list drivers fn ends here

//load more fn starts here
function loadmore() {
    $('.loadericon').show();
    dynamicloadforsuggestion(1);
}

//goto user prfoile fn starts here
function gotouserprofile(type, userid) {
    localStorage.fromtype = type;
    localStorage.userid_toseeprof = userid;
    window.open("profile.html", '_blank');
}

// ajax call for request in new matches page
function acceptanddenyusers(elem, id, type, functiontype) {
    if (type == 0) {
        var url = doshortlist_api;
    } else if (type == 1) {
        var url = doignore_api;
    } else {
        var url = dolikes_api;
    }
    var postData = JSON.stringify({
        "actor": id
    });
    $.ajax({
        url: url,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            if (type == 0) {
                $("#snackbarsuccs").text("Your request has been send to " + $(".newmatchusername" + functiontype + id).text());
            } else if (type == 1) {
                $("#snackbarsuccs").text("Your request has been send to " + $(".newmatchusername" + functiontype + id).text());
            } else {
                $("#snackbarsuccs").text("Your request has been send to " + $(".newmatchusername" + functiontype + id).text());
            }
            showsuccesstoast();
            dynamicloadforsuggestion(0);
        },
        error: function(data) {
            console.log("error occured during accept request");
        }
    });
} // ajax call for request in new matches page


//pagination fn starts here
function pagination(count) {
    $(".dynpagination").empty().show().append(`<li class="mright10"><a onclick="previouspage()" class="cptr font12 paginationtext">« Previous</a></li>`);
    // var totalpagescount = Math.round(count / 3);

    if ((count / 12) % 1 == 0) { //should change here only for how many box should come // count
        var totalpagescount = (count / 12);
    } else {
        var totalpagescount = parseInt((count / 12), 10) + 1;
    }

    sessionStorage.totalpagescount = totalpagescount;
    for (var i = 0; i < totalpagescount; i++) {
        $(".dynpagination").append(`<li class="pageli mright10 pageli${i+1} ${(i == 0 ? "active" : "")}" pageno="${i+1}"> <a class="cptr font12 paginationtext ${(i == 0 ? "pageactive" : "")}" onclick="gotothispage(${i+1})">${i+1}</a> </li>`);
        if (i == (totalpagescount - 1)) {
            $(".dynpagination").append(`<li class="mright10"><a onclick="nextpage()" class="cptr font12 paginationtext"> Next » </a></li>`);
        }
    }
    $(".pageli").hide();
    sessionStorage.showcount = 5;
    for (var i = 1; i <= 5; i++) {
        $(".pageli" + i).show();
    }
}

function gotothispage(pageno) {
    sessionStorage.whichclick = 2;
    $(".pageli").removeClass("active");
    $(".pageli" + pageno).addClass("active");

    $(".pageli a").removeClass("pageactive");
    $(".pageli" + pageno + " a").addClass("pageactive");
    sessionStorage.gotopage_no = pageno;
    dynamicloadforsuggestion(1);
}

function nextpage() {
    sessionStorage.whichclick = 2;
    var currentpageno = parseInt($(".pageli.active").attr("pageno"));
    console.log(currentpageno);

    if ((currentpageno % 6) == 0 && (currentpageno < sessionStorage.totalpagescount)) {
        $(".pageli").hide();
        for (var i = currentpageno; i <= currentpageno + 5; i++) {
            $(".pageli" + i).show();
        }
    }

    currentpageno++;
    console.log(currentpageno);
    if (parseInt(sessionStorage.totalpagescount) >= currentpageno) {
        $(".pageli").removeClass("active");
        $(".pageli" + currentpageno).addClass("active");

        $(".pageli a").removeClass("pageactive");
        $(".pageli" + currentpageno + " a").addClass("pageactive");
        sessionStorage.gotopage_no = currentpageno;
        dynamicloadforsuggestion(1);

        if ((currentpageno % 6) == 0 && (currentpageno < sessionStorage.totalpagescount)) {
            $(".pageli").hide();
            for (var i = currentpageno; i <= currentpageno + 5; i++) {
                $(".pageli" + i).show();
            }
        }
    }
}

function previouspage() {
    sessionStorage.whichclick = 2;
    var currentpageno = parseInt($(".pageli.active").attr("pageno"));
    console.log(currentpageno);

    if ((currentpageno % 6) == 0) {
        $(".pageli").hide();
        for (var i = currentpageno; i >= currentpageno - 5; i--) {
            $(".pageli" + i).show();
        }
    }

    currentpageno--;
    console.log(currentpageno);
    if (currentpageno != 0) {
        $(".pageli").removeClass("active");
        $(".pageli" + currentpageno).addClass("active");

        $(".pageli a").removeClass("pageactive");
        $(".pageli" + currentpageno + " a").addClass("pageactive");
        sessionStorage.gotopage_no = currentpageno;
        dynamicloadforsuggestion(1);

    }
}