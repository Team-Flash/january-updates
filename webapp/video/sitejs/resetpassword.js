$(function() {

    $(".loadericon").hide();

    $(".ipclkrsp").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".etrclkrsp").click();
        }
    });


    //re-password validation starts here
    // $('#password').keyup(function() {

    //     var pswd = $('#password').val();
    //     $('#password').removeClass("iserror");
    //     // keyup code here
    //     if (pswd.length < 8) {
    //         $('#length').removeClass('valid').addClass('invalid');
    //     } else {
    //         $('#length').removeClass('invalid').addClass('valid');
    //     }
    //     //validate letter
    //     if (pswd.match(/[A-z]/)) {
    //         $('#letter').removeClass('invalid').addClass('valid');
    //     } else {
    //         $('#letter').removeClass('valid').addClass('invalid');
    //     }

    //     //validate capital letter
    //     if (pswd.match(/[A-Z]/)) {
    //         $('#capital').removeClass('invalid').addClass('valid');
    //     } else {
    //         $('#capital').removeClass('valid').addClass('invalid');
    //     }

    //     //validate number
    //     if (pswd.match(/\d/)) {
    //         $('#number').removeClass('invalid').addClass('valid');
    //     } else {
    //         $('#number').removeClass('valid').addClass('invalid');
    //     }

    //     if ($("#letter").hasClass('valid') && $("#capital").hasClass('valid') && $("#number").hasClass('valid') && $("#length").hasClass('valid')) {
    //         $('#pswd_info').hide();
    //     } else {
    //         $('#pswd_info').show();
    //     }

    // }).focus(function() {
    //     $('#pswd_info').show();
    // }).blur(function() {
    //     $('#pswd_info').hide();
    // });
    //reset password div ends here

    $("#retype_password").keyup(function() {
        if ($("#retype_password").val() == "") {
            $('#retype_password').addClass("iserror");
            $("#snackbarerror").text("Re-Enter Password is Required");
            showiperrtoast();
        } else {
            $('#retype_password').removeClass("iserror");
        }

    });

    //get query string fn starts here
    function getQueryStrings() {
        var assoc = {};
        var decode = function(s) {
            return decodeURIComponent(s.replace(/\+/g, " "));
        };
        var queryString = location.search.substring(1);
        var keyValues = queryString.split('&');
        for (var i in keyValues) {
            var key = keyValues[i].split('=');
            if (key.length > 1) {
                assoc[decode(key[0])] = decode(key[1]);
            }
        }
        return assoc;
    }
    var qs = getQueryStrings();
    var uid = qs["id"];
    var token = qs["token"];
    sessionStorage.resettoken = token;
    sessionStorage.resetuser_id = uid;


}); //initila fn endss here

// reset password fn starts here
function rspcheck() {

     if ($('#password').val().trim() == '') {
        $('#password').addClass("iserror");
        $("#snackbarerror").text("Enter Password is Required!");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#retype_password').val().trim() == '') {
        $('#retype_password').addClass("iserror");
        $("#snackbarerror").text("Re-Enter Password is Required!");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#retype_password').val() != $('#password').val()) {
        $("#snackbarerror").text("Passwords Do Not Match!");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    // if ($("#letter").hasClass('valid') && $("#capital").hasClass('valid') && $("#number").hasClass('valid') && $("#length").hasClass('valid')) {
    //     $('#pswd_info').hide();
    // } else {
    //     $('#password').addClass("iserror");
    //     event.stopPropagation();
    //     return;
    // }
    // if ($('#retype_password').val().length < 8) {
    //     $('#retype_password').addClass("iserror");
    //     $("#snackbarerror").text("Min 6 Characters is Required for Re-Enter Password!");
    //     showiperrtoast();
    //     event.stopPropagation();
    //     return;
    // }

    $(".loadericon").show();
    $(".rspbtn").attr("disabled", true);

    var uniqueclient = Math.random() * 10000000000000000;

    var postData = JSON.stringify({
        "password": $('#password').val(),
        "uid": sessionStorage.getItem('resetuser_id'),
        "token": sessionStorage.getItem('resettoken'),
        "client": uniqueclient
    });

    $.ajax({
        url: resetpassword_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {
            $(".loadericon").hide();
            $(".rspbtn").attr("disabled", false);
        },
        error: function(data) {
            $(".loadericon").hide();
            $(".rspbtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();
        }
    }).done(function(dataJson) {

        $("#snackbarsuccs").text("Your Password Has Been Set Successfully!");
        showsuccesstoast();

        localStorage.logincred = JSON.stringify(dataJson);
        localStorage.wufname = dataJson.first_name;
        localStorage.wulname = dataJson.last_name;
        localStorage.wutkn = dataJson.token;
        localStorage.wuuser_id = dataJson.id;
        localStorage.wuemail = dataJson.email;
        localStorage.wuphone = dataJson.username;
        localStorage.wupropic = dataJson.userprofile.profile_pic;

        setTimeout(function() {
            window.location.href = "dashboard.html";
        }, 2000);

    });

} // reset password fn ends here