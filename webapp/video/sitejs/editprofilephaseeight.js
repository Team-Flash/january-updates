$(function() {

    $(".loadericon").hide();

    if (localStorage.editphase8data == undefined) {
        geteditphase8data();
    } else {
        var currentdate = new Date().getDate();
        if (currentdate != localStorage.datastoreddate_ep8) {
            localStorage.removeItem("editphase8data");
            geteditphase8data();
        } else {
            geteditp8datafromlocal(1); //this fn can happen 
        }
    }

    usergivendata();

}); //initial fn ends here

// geteditphase8data fn starts here
function geteditphase8data() {

    $.ajax({
        url: editprofilep8_dd_api,
        type: 'get',
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {

            localStorage.editphase8data = JSON.stringify(data);
            localStorage.datastoreddate_ep8 = new Date().getDate();

            geteditp8datafromlocal(0); //this fn is common for all
        },
        error: function(edata) {
            console.log("error occured in get edit phase 8 dd and others data");
        }
    });

} // geteditphase8data fn ends here

// localstorage data processing
function geteditp8datafromlocal(type) {

    var data = JSON.parse(localStorage.editphase8data);

    //country dyn append
    $('#ep8_country').empty().append('<option value="null">Select Your Country</option>');
    $('#ep8_state').empty().append('<option value="null">Select Your State</option>');
    $('#ep8_city').empty().append('<option value="null">Select Your City</option>');
    for (var i = 0; i < data.country.length; i++) {
        $('#ep8_country').append("<option value='" + data.country[i].id + "'>" + data.country[i].name + "</option>");
    }
    localStorage.selectedcountrycode = data.country[0].id;

} // localstorage data processing

function usergivendata() {

    var data = JSON.parse(localStorage.myprofiledata);

    //if user already selected the country
    if (data.user_address != null) {
        localStorage.selectedcountrycode = data.user_address.city.state.country.id;
        localStorage.usergivenstatecode = data.user_address.city.state.id;
        localStorage.usergivencitycode = data.user_address.city.id;
        $('#ep8_country option[value="' + data.user_address.city.state.country.id + '"]').attr("selected", true);

        $("#ep8_street").val(data.user_address.street);
        $("#ep8_area").val(data.user_address.area);
        $("#ep8_zipcode").val(data.user_address.zipcode);
        getstateslist(0);
    }


}

//country change <-> state change fn , state change <-> city change fn
$("#ep8_country").change(function() {
    if ($("#ep8_country").val() != "null") {
        localStorage.selectedcountrycode = $("#ep8_country").val();
        getstateslist(1);
    } else {
        $("#ep8_state option").removeAttr("selected");
        $("#ep8_state option[value='" + null + "']").attr("selected", "selected");
        $("#select2-ep8_state-container").text("Select Your State");

        $("#ep8_city option").removeAttr("selected");
        $("#ep8_city option[value='" + null + "']").attr("selected", "selected");
        $("#select2-ep8_city-container").text("Select Your City");
    }

});
$("#ep8_state").change(function() {

    if ($("#ep8_state").val() != "null") {
        localStorage.selectedstatecode = $("#ep8_state").val();
        getcitieslist(1);
    } else {
        $("#ep8_city option").removeAttr("selected");
        $("#ep8_city option[value='" + null + "']").attr("selected", "selected");
        $("#select2-ep8_city-container").text("Select Your City");
    }

});


//get states list fn starts here
function getstateslist(type) {
    $.ajax({
        url: geteditphase8statesdata_api + localStorage.selectedcountrycode + '/',
        type: 'get',
        success: function(data) {
            //states dyn append
            $('#ep8_state,#ep8_city').empty();
            if (data.length != 0) {
                for (var i = 0; i < data.length; i++) {
                    $("#ep8_state").append("<option value='" + data[i].id + "'>" + data[i].name + "</option>");
                }
                localStorage.selectedstatecode = data[0].id;
                if (type == 1) {
                    getcitieslist(1);
                }
            }

            //if type == 0 then it comes from initial fn to dynamically select the user choosen data
            if (type == 0) {
                $('#ep8_state option[value="' + localStorage.usergivenstatecode + '"]').attr("selected", true);
                localStorage.selectedstatecode = localStorage.usergivenstatecode;
                getcitieslist(0);
            }


        },
        error: function(edata) {
            console.log("error occured in get edit phase 8 states dd data");
        }
    });
} //get states list fn ends here

//get cities list fn starts here
function getcitieslist(type) {
    $.ajax({
        url: geteditphase8citiesdata_api + localStorage.selectedstatecode + '/',
        type: 'get',
        success: function(data) {
            //states dyn append
            $('#ep8_city').empty();
            if (data.length != 0) {
                for (var i = 0; i < data.length; i++) {
                    $("#ep8_city").append("<option value='" + data[i].id + "'>" + data[i].name + "</option>");
                }
            }
            //if type == 0 then it comes from initial fn to dynamically select the user choosen data
            if (type == 0) {
                $('#ep8_city option[value="' + localStorage.usergivencitycode + '"]').attr("selected", true);
            }
        },
        error: function(edata) {
            console.log("error occured in get edit phase 8 states dd data");
        }
    });
} //get cities list fn ends here

//send edit phase 8 data to server
function editp8_fn() {

    if ($("#ep8_city").val() == "") {
        $("#snackbarerror").text("City is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#ep8_street").val() == "") {
        $("#snackbarerror").text("Street is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#ep8_area").val() == "") {
        $("#snackbarerror").text("Area is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#ep8_zipcode").val() == "") {
        $("#snackbarerror").text("Zipcode is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $('.loadericon').show();
    $(".updateBtn").attr("disabled", true);

    var Datatosend = {
        "city": $("#ep8_city").val(),
        "street": $("#ep8_street").val(),
        "area": $("#ep8_area").val(),
        "zipcode": $("#ep8_zipcode").val()
    }

    var postData = JSON.stringify(Datatosend);

    $.ajax({
        url: sendeditp8data_api,
        type: 'PUT',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {

            $('.loadericon').hide();
            $(".updateBtn").attr("disabled", false);

        },
        error: function(data) {

            $('.loadericon').hide();
            $(".updateBtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {
        // alert("success");
        window.location.href = "myprofile.html";
    });

} //send edit phase 8 data to server
