var count = 0;
var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"];

$(function() {

    $(".loadmoreBtn,.loadericon").hide();

    loadsidedata();
    loadmaindata(0);

});

function loadmaindata(type) {

    if (type == 0 || type == 1) {
        $('.dynloadsearchresults').empty();
    }

    $(".loadmoreBtn,.loadericon").hide();

    var data = JSON.parse(sessionStorage.searchresults_maindata);

    if (data['results'].length != 0) {

        if (data.next_url != null) {
            sessionStorage.ldmrthis = data.next_url;
            $(".loadmoreBtn").show();
        }

        for (var i = 0; i < data['results'].length; i++) {

            var gender = (data['results'][i].userdetails.gender == "Male") ? "Mr. " : "Ms. ";

            var dyn_img = (data['results'][i].userprofile.profile_pic == null) ? "img/assets/coupleindex.svg" : data['results'][i].userprofile.profile_pic;

            var currentyear = parseInt(new Date().getFullYear());
            var userage = currentyear - parseInt(data['results'][i].userdetails.dob.substring(0, 4));

            $(".dynloadsearchresults").append("<div class='row'><div class='col-md-2 col-sm-3 col-xs-2 latestupdateimg'><a class='pointer' style='background: url("+dyn_img+") top center / cover no-repeat;display: block;height: 70px;'></a> </div><div class='col-md-9 col-sm-8 col-xs-9'> <p class='dashboardusername pointer'><span onclick='gotouserprofile(" + 1 + "," + data['results'][i].id + ")'>" + gender + " " + data['results'][i].first_name + "  " + data['results'][i].last_name + "</span><span class='msgpagedate'><i class='fa fa-calendar-check-o' aria-hidden='true'></i>&nbsp;" + data['results'][i].userdetails.dob + "&emsp;<a class='pointer' onclick='gotouserprofile(" + 1 + " , " + data['results'][i].id + ")'><i class='fa fa-info-circle trashicon' aria-hidden='true'></i></a></span></p><a class='pointer pinkcolor' onclick='gotouserprofile(" + 1 + " , " + data['results'][i].id + ")' class='msgpageanchor'><i class='fa fa-info-circle viewnumbericon' aria-hidden='true'></i>&emsp;See " + gender + " " + data['results'][i].first_name + "  " + data['results'][i].last_name + " full deatils here</a> <p class='f13'><span class=''>" + userage + " years</span> | <span class=''>" + data['results'][i].userdetails.dob + "</span> | <span class=''>" + data['results'][i].userdetails.religion.name + "</span> | <span class=''>" + data['results'][i].userdetails.mother_tongue.name + "</span></p></div></div><hr class='mtb10'>");

        }

        $("#loaderspinner").hide();

    } else {

        $(".loadmoreBtn").hide();

        if (type == 0) {
            $(".dynloadsearchresults").append('<div class="row"><div class="col-md-12 col-sm-12 col-xs-12 pr0"><center><img src="img/assets/nodata.svg" style="width:40%"></center></div></div>');
        } else {
            $(".dynloadsearchresults").append('<div class="row"><div class="col-md-12 col-sm-12 col-xs-12 pr0"><center><img src="img/assets/nodata.svg" style="width:40%"></center></div></div>');
            $("#loaderspinner").hide();

        }

    } //else cond ends here

} //main cntnt loading ends here

//goto user prfoile fn starts here
function gotouserprofile(type, userid) {
    localStorage.fromtype = type;
    localStorage.userid_toseeprof = userid;
    window.open("profile.html", '_blank');
}

//show user phono no fn starts here
function showuserpno(columntype, userid, userphoneno) {
    var getdetails = getObjects(JSON.parse(sessionStorage.searchresults_maindata), 'username', userphoneno.toString());
    var data = getdetails[0];

    var dyn_img = (data.userprofile.profile_pic == null) ? "img/assets/coupleindex.svg" : data.userprofile.profile_pic;
    $(".userimginmodal").attr("src", dyn_img);

    var gender = (data.userdetails.gender == "Male") ? "Mr. " : "Ms. ";
    $(".userdynnameinmdl").text(gender + data.first_name + " " + data.last_name);

    $(".usermtrimonyidinmdl").text("(" + data.userprofile.uid + ")");
    $(".phonenoinmdl").text(data.username);

    var currentyear = parseInt(new Date().getFullYear());
    var userage = currentyear - parseInt(data.userdetails.dob.substring(0, 4));
    $(".userageinmdl").text(userage + " years");

    $(".userdobinmdl").text(data.userdetails.dob);
    $(".userreligioninmdl").text(data.userdetails.religion.name);
    $(".usermothertnginmdl").text(data.userdetails.mother_tongue.name);

}

$(".phonenoshowmdl").click(function() {
    $(".close").click();
});

function getObjects(obj, key, val) {
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key, val));
        } else if (i == key && obj[key] == val) {
            objects.push(obj);
        }
    }
    // console.log(objects);
    return objects;

}


function loadsidedata() {

    if (sessionStorage.comingfromsearch == 1) {
        $(".comingfrom").attr("href", "search-regular.html");
    } else {
        $(".comingfrom").attr("href", "search-soulmate.html");
    }

    var data = JSON.parse(sessionStorage.searchresults_sidedata);


    //Religion data
    if (data.religion.length == 0 || data.religion.length == null) {
        $('.Profiletype').append('<br><span class="filteranchor nodatainsearch"> Not Mentioned </span>');
    } else {
        for (var i = 0; i < data.religion.length; i++) {
            $('.profiletype').append('<br><a class="filteranchor pointer" onclick="searchside(' + 0 + ',' + data.religion[i].id + ')" >' + data.religion[i].name + '&nbsp;<span class="filtercount">(' + data.religion[i].count + ')</span></a>');
        }
    }

    //Marital Status
    if (data.marital_status.length == 0 || data.marital_status.length == null) {
        $('.martialstatus').append('<br><span class="filteranchor nodatainsearch"> Not Mentioned </span>');
    } else {
        for (var i = 0; i < data.marital_status.length; i++) {
            $('.martialstatus').append('<br><a class="filteranchor pointer" onclick="searchside(' + 1 + ',' + data.marital_status[i].id + ')" >' + data.marital_status[i].name + '&nbsp;<span class="filtercount">(' + data.marital_status[i].count + ')</span></a>');
        }
    }

    //mothertongue Status
    if (data.mother_tongue.length == 0 || data.mother_tongue.length == null) {
        $('.mothertongue').append('<br><span class="filteranchor nodatainsearch"> Not Mentioned </span>');
    } else {
        for (var i = 0; i < data.mother_tongue.length; i++) {
            $('.mothertongue').append('<br><a class="filteranchor pointer" onclick="searchside(' + 2 + ',' + data.mother_tongue[i].id + ')" >' + data.mother_tongue[i].name + '&nbsp;<span class="filtercount">(' + data.mother_tongue[i].count + ')</span></a>');
        }
    }

    //caste Status
    if (data.caste.length == 0 || data.caste.length == null) {
        $('.caste').append('<br><span class="filteranchor nodatainsearch"> Not Mentioned </span>');
    } else {
        for (var i = 0; i < data.caste.length; i++) {
            $('.caste').append('<br><a class="filteranchor pointer" onclick="searchside(' + 3 + ',' + data.caste[i].id + ')" >' + data.caste[i].name + '&nbsp;<span class="filtercount">(' + data.caste[i].count + ')</span></a>');
        }
    }

    //star Status
    if (data.star.length == 0 || data.star.length == null) {
        $('.star').append('<br><span class="filteranchor nodatainsearch"> Not Mentioned </span>');
    } else {
        for (var i = 0; i < data.star.length; i++) {
            $('.star').append('<br><a class="filteranchor pointer" onclick="searchside(' + 4 + ',' + data.star[i].id + ')" >' + data.star[i].name + '&nbsp;<span class="filtercount">(' + data.star[i].count + ')</span></a>');
        }
    }

    //education Status
    if (data.highest_education.length == 0 || data.highest_education.length == null) {
        $('.education').append('<br><span class="filteranchor nodatainsearch"> Not Mentioned </span>');
    } else {
        for (var i = 0; i < data.highest_education.length; i++) {
            $('.education').append('<br><a class="filteranchor pointer" onclick="searchside(' + 5 + ',' + data.highest_education[i].id + ')" >' + data.highest_education[i].name + '&nbsp;<span class="filtercount">(' + data.highest_education[i].count + ')</span></a>');
        }
    }

    //employedin Status
    if (data.employed_in.length == 0 || data.employed_in.length == null) {
        $('.employedin').append('<br><span class="filteranchor nodatainsearch"> Not Mentioned </span>');
    } else {
        for (var i = 0; i < data.employed_in.length; i++) {
            $('.employedin').append('<br><a class="filteranchor pointer" onclick="searchside(' + 6 + ',' + data.employed_in[i].id + ')" >' + data.employed_in[i].name + '&nbsp;<span class="filtercount">(' + data.employed_in[i].count + ')</span></a>');
        }
    }

    //occupation Status
    if (data.occupation.length == 0 || data.occupation.length == null) {
        $('.occupation').append('<br><span class="filteranchor nodatainsearch"> Not Mentioned </span>');
    } else {
        for (var i = 0; i < data.occupation.length; i++) {
            $('.occupation').append('<br><a class="filteranchor pointer" onclick="searchside(' + 7 + ',' + data.occupation[i].id + ')" >' + data.occupation[i].name + '&nbsp;<span class="filtercount">(' + data.occupation[i].count + ')</span></a>');
        }
    }

    //annualinc Status
    if (data.annual_income.length == 0 || data.annual_income.length == null) {
        $('.annualinc').append('<br><span class="filteranchor nodatainsearch"> Not Mentioned </span>');
    } else {

        if (sessionStorage.annualincome == 1) {
            var annualincomecntnt = "1 lakh to 3 lakhs"
        } else if (sessionStorage.annualincome == 2) {
            var annualincomecntnt = "3 lakh to 5 lakhs"
        } else if (sessionStorage.annualincome == 3) {
            var annualincomecntnt = "5 lakh to 10 lakhs"
        } else {
            var annualincomecntnt = "10 lakhs and more"
        }

        for (var i = 0; i < data.annual_income.length; i++) {
            $('.annualinc').append('<br><a class="filteranchor pointer" onclick="searchside(' + 8 + ',' + sessionStorage.annualincome + ')" >' + annualincomecntnt + '&nbsp;<span class="filtercount">(' + data.annual_income[i].count + ')</span></a>');
        }
    }

    //residingcityrdis Status
    if (data.city.length == 0 || data.city.length == null) {
        $('.residingcityrdis').append('<br><span class="filteranchor nodatainsearch"> Not Mentioned </span>');
    } else {
        for (var i = 0; i < data.city.length; i++) {
            $('.residingcityrdis').append('<br><a class="filteranchor pointer" onclick="searchside(' + 9 + ',' + data.city[i].id + ')" >' + data.city[i].name + '&nbsp;<span class="filtercount">(' + data.city[i].count + ')</span></a>');
        }
    }

    //physicalstatus Status
    if (data.is_physically_challenged.length == 0 || data.is_physically_challenged.length == null) {
        $('.physicalstatus').append('<br><span class="filteranchor nodatainsearch"> Not Mentioned </span>');
    } else {
        for (var i = 0; i < data.is_physically_challenged.length; i++) {
            if (data.is_physically_challenged[i].name == 0) {
                var physicalstatuscntnt = "Normal";
            } else {
                var physicalstatuscntnt = "Physically Challenged";
            }
            $('.physicalstatus').append('<br><a class="filteranchor pointer physicalstcntnt" onclick="searchside(' + 10 + ',' + data.is_physically_challenged[i].name + ')" >' + physicalstatuscntnt + '&nbsp;<span class="filtercount">(' + data.is_physically_challenged[i].count + ')</span></a>');
        }
    }

    //skintone Status
    if (data.skin_tone.length == 0 || data.skin_tone.length == null) {
        $('.skintone').append('<br><span class="filteranchor nodatainsearch"> Not Mentioned </span>');
    } else {
        for (var i = 0; i < data.skin_tone.length; i++) {
            $('.skintone').append('<br><a class="filteranchor pointer" onclick="searchside(' + 11 + ',' + data.skin_tone[i].id + ')" ><span class="skintonetypefltr">' + data.skin_tone[i].name + '</span>&nbsp;<span class="filtercount">(' + data.skin_tone[i].count + ')</span></a>');
        }
    }

    //bodytype Status
    if (data.body_type.length == 0 || data.body_type.length == null) {
        $('.bodytype').append('<br><span class="filteranchor nodatainsearch"> Not Mentioned </span>');
    } else {
        for (var i = 0; i < data.body_type.length; i++) {
            $('.bodytype').append('<br><a class="filteranchor pointer" onclick="searchside(' + 12 + ',' + data.body_type[i].id + ')" ><span class="bodytypefltr">' + data.body_type[i].name + '</span>&nbsp;<span class="filtercount">(' + data.body_type[i].count + ')</span></a>');
        }
    }

    //eatinghabits Status
    if (data.eating_habit.length == 0 || data.eating_habit.length == null) {
        $('.eatinghabits').append('<br><span class="filteranchor nodatainsearch"> Not Mentioned </span>');
    } else {
        for (var i = 0; i < data.eating_habit.length; i++) {
            $('.eatinghabits').append('<br><a class="filteranchor pointer" onclick="searchside(' + 13 + ',' + data.eating_habit[i].id + ')" ><span class="eatingtypefltr">' + data.eating_habit[i].name + '</span>&nbsp;<span class="filtercount">(' + data.eating_habit[i].count + ')</span></a>');
        }
    }

    //smoking Status
    if (data.smoking_habit.length == 0 || data.smoking_habit.length == null) {
        $('.smoking').append('<br><span class="filteranchor nodatainsearch"> Not Mentioned </span>');
    } else {
        for (var i = 0; i < data.smoking_habit.length; i++) {
            $('.smoking').append('<br><a class="filteranchor pointer" onclick="searchside(' + 14 + ',' + data.smoking_habit[i].id + ')" ><span class="smokingtypefltr">' + data.smoking_habit[i].name + '</span>&nbsp;<span class="filtercount">(' + data.smoking_habit[i].count + ')</span></a>');
        }
    }

    //drinking Status
    if (data.drinking_habit.length == 0 || data.drinking_habit.length == null) {
        $('.drinking').append('<br><span class="filteranchor nodatainsearch"> Not Mentioned </span>');
    } else {
        for (var i = 0; i < data.drinking_habit.length; i++) {
            $('.drinking').append('<br><a class="filteranchor pointer" onclick="searchside(' + 15 + ',' + data.drinking_habit[i].id + ')" ><span class="drinkingtypefltr">' + data.drinking_habit[i].name + '</span>&nbsp;<span class="filtercount">(' + data.drinking_habit[i].count + ')</span></a>');
        }
    }

    //familyvalue Status
    if (data.family_values.length == 0 || data.family_values.length == null) {
        $('.familyvalue').append('<br><span class="filteranchor nodatainsearch"> Not Mentioned </span>');
    } else {
        for (var i = 0; i < data.family_values.length; i++) {
            $('.familyvalue').append('<br><a class="filteranchor pointer" onclick="searchside(' + 16 + ',' + data.family_values[i].id + ')" >' + data.family_values[i].name + '&nbsp;<span class="filtercount">(' + data.family_values[i].count + ')</span></a>');
        }
    }

    //familytype Status
    if (data.family_type.length == 0 || data.family_type.length == null) {
        $('.familytype').append('<br><span class="filteranchor nodatainsearch"> Not Mentioned </span>');
    } else {
        for (var i = 0; i < data.family_type.length; i++) {
            $('.familytype').append('<br><a class="filteranchor pointer" onclick="searchside(' + 17 + ',' + data.family_type[i].id + ')" ><span class="familytypefltr">' + data.family_type[i].name + '</span>&nbsp;<span class="filtercount">(' + data.family_type[i].count + ')</span></a>');
        }
    }

    //familystatus Status
    if (data.family_status.length == 0 || data.family_status.length == null) {
        $('.familystatus').append('<br><span class="filteranchor nodatainsearch"> Not Mentioned </span>');
    } else {
        for (var i = 0; i < data.family_status.length; i++) {
            $('.familystatus').append('<br><a class="filteranchor pointer" onclick="searchside(' + 18 + ',' + data.family_status[i].id + ')" >' + data.family_status[i].name + '&nbsp;<span class="filtercount">(' + data.family_status[i].count + ')</span></a>');
        }
    }
}

// var searchkeysidearr = ['userdetails__religion', 'user_personal_details__marital_status__in',
//     'userdetails__mother_tongue__in', 'user_religion_details__caste', 'religion_info__star__in', 'user_professional_details__education__in', 'user_professional_details__employed_in', 'user_professional_details__occupation', 'user_professional_details__annual_income',
//     'user_address__city', 'user_personal_details__is_physically_challenged', 'basic_information__skin_tone',
//     'basic_information__body_type', 'lifestyle__eating_habit', 'lifestyle__drinking_habit', 'lifestyle__smoking_habit',
//     'user_personal_details__family_values', 'user_personal_details__family_type', 'user_personal_details__family_status'
// ]

//side bar search fn starts here
//search fn starts here
function searchside(type, id) {

    $("#loaderspinner").show();
    // var gettypekey = searchkeysidearr[type];
    // console.log(gettypekey);
    if (type == 0) {
        postData = JSON.stringify({
            "userdetails__religion": id
        });
    } else if (type == 1) {
        postData = JSON.stringify({
            "user_personal_details__marital_status": id
        });
    } else if (type == 2) {
        postData = JSON.stringify({
            "userdetails__mother_tongue": id
        });
    } else if (type == 3) {
        postData = JSON.stringify({
            "user_religion_details__caste": id
        });
    } else if (type == 4) {
        postData = JSON.stringify({
            "religion_info__star": id
        });
    } else if (type == 5) {
        postData = JSON.stringify({
            "user_professional_details__highest_education": id
        });
    } else if (type == 6) {
        postData = JSON.stringify({
            "user_professional_details__employed_in": id
        });
    } else if (type == 7) {
        postData = JSON.stringify({
            "user_professional_details__occupation": id
        });
    } else if (type == 8) {
        postData = JSON.stringify({
            "user_professional_details__annual_income": id
        });
    } else if (type == 9) {
        postData = JSON.stringify({
            "user_address__city": id
        });
    } else if (type == 10) {
        postData = JSON.stringify({
            "user_personal_details__is_physically_challenged": id
        });
    } else if (type == 11) {
        postData = JSON.stringify({
            "basic_information__skin_tone": $(".skintonetypefltr").text()
        });
    } else if (type == 12) {
        postData = JSON.stringify({
            "basic_information__body_type": $(".bodytypefltr").text()
        });
    } else if (type == 13) {
        postData = JSON.stringify({
            "lifestyle__eating_habit": $(".eatingtypefltr").text()
        });
    } else if (type == 14) {
        postData = JSON.stringify({
            "lifestyle__smoking_habit": $(".smokingtypefltr").text()
        });
    } else if (type == 15) {
        postData = JSON.stringify({
            "lifestyle__drinking_habit": $(".drinkingtypefltr").text()
        });
    } else if (type == 16) {
        postData = JSON.stringify({
            "user_personal_details__family_values": id
        });
    } else if (type == 17) {
        postData = JSON.stringify({
            "user_personal_details__family_type": $(".familytypefltr").text()
        });
    } else {
        postData = JSON.stringify({
            "user_personal_details__family_status": id
        });
    }


    $("html, body").animate({ scrollTop: 0 }, "slow");

    $.ajax({
        url: filteredsearch_api,
        type: 'POST',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": 'Token ' + localStorage.wutkn + ''
        },
        success: function(data) {},
        error: function(edata) {
            // alert("error2");
            $("#loaderspinner").hide();
            console.log("error occured in loading data for the search - side");
            $("#snackbarerror").text("No profile available.Please search different fields!");
            showerrtoast();

            $(".dynloadsearchresults").empty().append('<div class="row"><div class="col-md-12 col-sm-12 col-xs-12 pr0"><center><img src="img/assets/nodata.svg" style="width:40%"></center></div></div>');
            $(".loadmoreBtn").hide();
        }
    }).done(function(dataJson) {
        // alert("success2");
        sessionStorage.searchresults_maindata = JSON.stringify(dataJson);
        loadmaindata(1);
    });

} //search fn starts here

//side bar search fn starts here

$(".endselectbox").click(function() {
    $(this).removeClass("iserror");
});

//age , height , weight fn starts here
function searchage_height_weight(type) {

    

    if (type == 1) {
        if ($(".sr_agestart").val() == $(".sr_ageend").val()) {
            $("#snackbarerror").text("Age start and end should not be same");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        if ($(".sr_ageend").val() < $(".sr_agestart").val()) {
            $("#snackbarerror").text("End Age Should be higher than Start Age");
            $('.sr_ageend').addClass("iserror");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        postData = JSON.stringify({
            userdetails__age__gte: $(".sr_agestart").val(),
            userdetails__age__lte: $(".sr_ageend").val()
        });
    } else if (type == 2) {
        if ($(".sr_heightstart").val() == $(".sr_heightend").val()) {
            $("#snackbarerror").text("Height start and end should not be same");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        if ($(".sr_heightend").val() < $(".sr_heightstart").val()) {
            $("#snackbarerror").text("End height Should be higher than Start height");
            $('.sr_heightend').addClass("iserror");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        postData = JSON.stringify({
            user_personal_details__height__gte: $(".sr_heightstart").val(),
            user_personal_details__height__lte: $(".sr_heightend").val()
        });
    } else {
        if ($(".sr_weightstart").val() == $(".sr_weightend").val()) {
            $("#snackbarerror").text("Weight start and end should not be same");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        if ($(".sr_weightend").val() < $(".sr_weightstart").val()) {
            $("#snackbarerror").text("End Weight Should be higher than Start Weight");
            $('.sr_weightend').addClass("iserror");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
        postData = JSON.stringify({
            user_personal_details__weight__gte: $(".sr_weightstart").val(),
            user_personal_details__weight__lte: $(".sr_weightend").val()
        });
    }

    $("#loaderspinner").show();

    // console.log(postData);

    $("html, body").animate({ scrollTop: 0 }, "slow");

    $.ajax({
        url: filteredsearch_api,
        type: 'POST',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": 'Token ' + localStorage.wutkn + ''
        },
        success: function(data) {},
        error: function(edata) {
            // alert("error2");
            $("#loaderspinner").hide();
            console.log("error occured in loading data for the search - side");
            $("#snackbarerror").text("No profile available");
            showerrtoast();

            $(".dynloadsearchresults").empty().append('<div class="row"><div class="col-md-12 col-sm-12 col-xs-12 pr0"><center><img src="img/assets/nodata.svg" style="width:40%"></center></div></div>');
            $(".loadmoreBtn").hide();
        }
    }).done(function(dataJson) {
        // alert("success2");
        sessionStorage.searchresults_maindata = JSON.stringify(dataJson);
        loadmaindata(1);
    });

} //age , height , weight fn starts here


//load more fn starts here
function loadmore() {
    $('.loadericon').show();
    loadmaindata_ldmore();
}

//loadmaindata_ldmore fn starts here
function loadmaindata_ldmore() {
    $.ajax({
        url: sessionStorage.ldmrthis,
        type: 'POST',
        headers: {
            "content-type": 'application/json',
            "Authorization": 'Token ' + localStorage.wutkn + ''
        },
        success: function(data) {},
        error: function(edata) {
            // alert("error2");
            console.log("error occured in loading data for the search - mian data - load more");
            $(".loadericon").hide();
        }
    }).done(function(dataJson) {
        // alert("success2");
        sessionStorage.searchresults_maindata = JSON.stringify(dataJson);
        loadmaindata(2);
    });

} //loadmaindata_ldmore fn ends here
