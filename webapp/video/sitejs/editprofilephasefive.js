var phoneno = [];


$(function() {

    $(".loadericon").hide();

    if (localStorage.editphase5data == undefined) {
        geteditphase5data();
    } else {
        var currentdate = new Date().getDate();
        if (currentdate != localStorage.datastoreddate_ep5) {
            localStorage.removeItem("editphase5data");
            geteditphase5data();
        } else {
            geteditp5datafromlocal(1); //this fn can happen 
        }
    }

    usergivendata();

}); //initial fn ends here for editphase5

// geteditphase5data fn starts here
function geteditphase5data() {

    $.ajax({
        url: editprofilep5_dd_api,
        type: 'get',
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {

            localStorage.editphase5data = JSON.stringify(data);
            localStorage.datastoreddate_ep5 = new Date().getDate();

            geteditp5datafromlocal(0); //this fn is common for all
        },
        error: function(edata) {
            console.log("error occured in get edit phase 5 dd and others data");
        }
    });

} // geteditphase5data fn ends here

function geteditp5datafromlocal(type) {

    var data = JSON.parse(localStorage.editphase5data);
    //parent status dyn append
    $('#ep5_fatherstatus,#ep5_motherstatus').empty();
    $('#ep5_fatherstatus').append('<option value="null">Select Your Father Status</option>');
    $('#ep5_motherstatus').append('<option value="null">Select Your Mother Status</option>');
    for (var i = 0; i < data.employment.length; i++) {
        // if (i == 0) {
        //     $('#ep5_fatherstatus,#ep5_motherstatus').append("<option selected='' value='" + data.employment[i].id + "'>" + data.employment[i].name + "</option>");
        // } else {
            $('#ep5_fatherstatus,#ep5_motherstatus').append("<option value='" + data.employment[i].id + "'>" + data.employment[i].name + "</option>");
        // }
    }
}

//get edit phase 5 details from local
function usergivendata() {

    var data = JSON.parse(localStorage.myprofiledata);

    //validation for basic bodytype
    if (data.basic_information != null) {
        $('input:radio[name="bodytype"]').filter('[value="' + data.basic_information.body_type + '"]').attr('checked', true);
        $('input:radio[name="skintone"]').filter('[value="' + data.basic_information.skin_tone + '"]').attr('checked', true);
        $('#ep5_weight option[value="' + data.basic_information.weight + '"]').attr("selected", true);

    }

    //validation for lifestyle-eating,drinking,smoking
    if (data.lifestyle != null) {
        $('input:radio[name="eatinghabit"]').filter('[value="' + data.lifestyle.eating_habit + '"]').attr('checked', true);
        $('input:radio[name="drinkinghabit"]').filter('[value="' + data.lifestyle.drinking_habit + '"]').attr('checked', true);
        $('input:radio[name="smokinghabits"]').filter('[value="' + data.lifestyle.smoking_habit + '"]').attr('checked', true);
    }

    //family info -father status and mother status 
    if (data.family_info != null) {
        $('#ep5_fatherstatus option[value="' + data.family_info.father_status.id + '"]').attr("selected", true);
        $('#ep5_motherstatus option[value="' + data.family_info.mother_status.id + '"]').attr("selected", true);

        var parentcontactno = data.family_info.contact_number.split(",");

        for (var i = 0; i < parentcontactno.length; i++) {
            $(".tags .tagAdd").before('<li class="addedTag">' + parentcontactno[i] + '<span class="tagRemove" onclick="$(this).parent().remove();">x</span><input type="hidden" value="' + parentcontactno[i] + '" name="tags[]"></li>');
        }
    }


    //validation for phone no:
    // if (data.phoneno != null) {
        
    // }

}

//send phase 5 data to server
function editp5_fn() {

    //body type
    if ($('input[name="bodytype"]:checked').length == 0) {
        $("#snackbarerror").text("Body Type is required");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    //eating habit,drinking habit,smoking habits
    if ($('input[name="skintone"]:checked').length == 0) {
        $("#snackbarerror").text("Skin Tone is required");
        $("html, body").animate({ scrollTop: 400 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#ep5_weight').val() == 'null' || $('#ep5_weight').val() == null) {
        $("#snackbarerror").text("Weight is required");
        $("html, body").animate({ scrollTop: 400 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('input[name="eatinghabit"]:checked').length == 0) {
        $("#snackbarerror").text("Eating habit is required");
        $("html, body").animate({ scrollTop: 400 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('input[name="drinkinghabit"]:checked').length == 0) {
        $("#snackbarerror").text("Drinking habit is required");
        $("html, body").animate({ scrollTop: 400 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('input[name="smokinghabits"]:checked').length == 0) {
        $("#snackbarerror").text("Smoking habit is required");
        $("html, body").animate({ scrollTop: 400 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    //familyinfo edit
    if ($('#ep5_fatherstatus').val() == '' || $("#ep5_fatherstatus").val() == null || $("#ep5_fatherstatus").val() == "null") {
        $("#snackbarerror").text("Father status is required");
        $("html, body").animate({ scrollTop: 400 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('#ep5_motherstatus').val() == '' || $("#ep5_motherstatus").val() == null || $("#ep5_motherstatus").val() == "null") {
        $("#snackbarerror").text("Mother status is required");
        $("html, body").animate({ scrollTop: 400 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    // if ($(".tags .addedTag").length == 0) {
    //     $("#snackbarerror").text("Phone number is required");
    //     $("html, body").animate({ scrollTop: 400 }, "slow");
    //     showiperrtoast();
    //     event.stopPropagation();
    //     return;
    // }

    $('.loadericon').show();
    $(".updateBtn").attr("disabled", true);

    // for (var i = 0; i < $('ul .addedTag').length; i++) {
        phoneno.push("0000000000");
    // }

    var Datatosendphase5 = {
        "body_type": $('input[name="bodytype"]:checked').val(),
        "skin_tone": $('input[name="skintone"]:checked').val(),
        "weight": $("#ep5_weight").val(),
        "eating_habit": $('input[name="eatinghabit"]:checked').val(),
        "drinking_habit": $('input[name="drinkinghabit"]:checked').val(),
        "smoking_habit": $('input[name="smokinghabits"]:checked').val(),
        "father_status": $("#ep5_fatherstatus").val(),
        "mother_status": $("#ep5_motherstatus").val(),
        "contact_number": phoneno.toString()
    }


    var postData = JSON.stringify(Datatosendphase5);

    $.ajax({
        url: sendeditp5data_api,
        type: 'PUT',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {

            $('.loadericon').hide();
            $(".updateBtn").attr("disabled", false);

        },
        error: function(data) {

            $('.loadericon').hide();
            $(".updateBtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {
        // alert("success")
        window.location.href = "myprofile.html";
    });

} //edit onclick func ends here
