$(function() {


    $('.loadericon').hide();

    //enter click fn signup/login starts here
    $(".ipclklgn").keyup(function(event) {
        if (event.keyCode == 13) {
            $(".etrclklgn").click();
        }
    });

    $('#loginemailid').focusout(function() {
        var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
        var str = $('#loginemailid').val();
        if (numberRegex.test(str)) {
            var phone = $('#loginemailid').val();
            var phoneNum = phone.replace(/[^\d]/g, '');
            if (phoneNum.length < 10 || phoneNum.length > 11) {
                emailcheck = 0;
            }
        } else {
            var pattern = /^([a-z\d!#$%&'*+\-\/=?^â€‹_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._â€‹~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
            if (!pattern.test($(this).val())) {
                var x = $('#loginemailid').val();
                var atpos = x.indexOf("@");
                var dotpos = x.lastIndexOf(".");
                if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
                    emailcheck = 1;
                    $('#loginemailid').addClass("iserror");
                    event.stopPropagation();
                    return;
                }
            }
        }
    });

    $("#loginemailid").keyup(function() {
        if ($("#loginemailid").val() == "")
            $('#loginemailid').addClass("iserror");
        else
            $('#loginemailid').removeClass("iserror");
    });

    $("#loginpasswrd").keyup(function() {
        if ($("#loginpasswrd").val() == "")
            $('#loginpasswrd').addClass("iserror");
        else
            $('#loginpasswrd').removeClass("iserror");
    });

});

//user login fn starts here
function userlogin() {


    if ($('#loginemailid').val().trim() == '') {
        $("#snackbarerror").text("E-Mail Address / Phone No is required");
        $('#loginemailid').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
    var str = $('#loginemailid').val();
    if (numberRegex.test(str)) {
        var phone = $('#loginemailid').val();
        var phoneNum = phone.replace(/[^\d]/g, '');
        if (phoneNum.length < 10 || phoneNum.length > 11) {
            $('#loginemailid').addClass("iserror");
            $("#snackbarerror").text("Valid Phone No is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
    } else {
        var x = $('#loginemailid').val();
        var atpos = x.indexOf("@");
        var dotpos = x.lastIndexOf(".");
        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
            $('#loginemailid').addClass("iserror");
            $("#snackbarerror").text("Valid E-Mail Address is required");
            showiperrtoast();
            event.stopPropagation();
            return;
        }

    }

    if ($('#loginpasswrd').val().trim() == '') {
        $("#snackbarerror").text("Password is required");
        $('#loginpasswrd').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $('.loadericon').show();
    $(".lgnBtn").attr("disabled", true);

    var uniqueclient = Math.random() * 10000000000000000;

    var postData = JSON.stringify({

        "username": $('#loginemailid').val(),
        "password": $('#loginpasswrd').val(),
        "client": uniqueclient,
        "role" : 3

    });

    $.ajax({
        url: login_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {

            $('.loadericon').hide();
            $(".lgnBtn").attr("disabled", false);

        },
        error: function(data) {

            $(".lgnBtn").html('Login');
            $(".lgnBtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {

        $("#snackbarsuccs").text("Login Success!");
        showsuccesstoast();

        //         {
        //     "id": 8,
        //     "first_name": "Gopi",
        //     "last_name": "Akshay",
        //     "username": "8610011892",
        //     "email": "gopi@billiontags.com",
        //     "token": "ad61e9fc9f2fddf80f6f33bc67972e193b8eec6e",
        //     "userprofile": {
        //         "profile_pic": null,
        //         "uid": "M000008"
        //     },
        //     "userdetails": {
        //         "id": 16,
        //         "gender": "Male",
        //         "relationship": {
        //             "id": 1,
        //             "name": "Myself"
        //         }
        //     }
        // }


        localStorage.useracccred = JSON.stringify(dataJson);
        localStorage.useridactivate = dataJson.id;
        localStorage.wufname = dataJson.first_name;
        localStorage.wulname = dataJson.last_name;
        localStorage.wutkn = dataJson.token;
        localStorage.userregphoneno = dataJson.username;
        localStorage.usermailid = dataJson.email;
        localStorage.matrimonyid = dataJson.userprofile.uid;
        localStorage.wuupldedpropic = dataJson.userprofile.profile_pic;

        localStorage.gender = dataJson.userdetails.gender;
        localStorage.profilecratedforname = dataJson.userdetails.relationship.name;
        localStorage.profilecratedforid = dataJson.userdetails.relationship.id;
        // localStorage.wufbpropic = dataJson.user_info.userprofile.picture;

           if (dataJson.userdetails.quit_status == 0) {
                window.location.href = "register-phase1.html";
            } else if (dataJson.userdetails.quit_status == 1) {
                window.location.href = "register-phase2.html";
            } else if (dataJson.userdetails.quit_status == 2) {
                window.location.href = "register-phase3.html";
            } else if (dataJson.userdetails.quit_status == 3) {
                window.location.href = "register-phase4.html";
            } else if (dataJson.userdetails.quit_status == 4) {
                window.location.href = "register-phase5.html";
            } else if (dataJson.userdetails.quit_status == 5) {
                window.location.href = "register-phase6.html";
            } else {
                window.location.href = "dashboard.html";
            }

    }); //done fn ends here

} //login fn ends here


//loginjs starts here
function _dologinfb() {

    $("#loaderspinner").show();

    var uniqueclient = Math.random() * 10000000000000000;
    var postData = JSON.stringify({
        "access_token": localStorage.fbat,
        "client": uniqueclient
    });

    $.ajax({
        url: login_api_fb,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {

        },
        error: function(data) {
            $("#loaderspinner").hide();
            // var errtext = "";
            // for (var key in JSON.parse(data.responseText)) {
            //     errtext = JSON.parse(data.responseText)[key][0];
            // }
            $("#snackbarerror").text("Error occured.Try Again!");
            showerrtoast();
        }
    }).done(function(dataJson) {

        $("#loaderspinner").hide();



        if (dataJson.user_exists == true) {

            $("#snackbarsuccs").text("Login Success!");
            showsuccesstoast();

            localStorage.useracccred = JSON.stringify(dataJson);
            localStorage.useridactivate = dataJson.id;
            localStorage.wufname = dataJson.first_name;
            localStorage.wulname = dataJson.last_name;
            localStorage.wutkn = dataJson.token;
            localStorage.userregphoneno = dataJson.username;
            localStorage.usermailid = dataJson.email;
            localStorage.matrimonyid = dataJson.userprofile.uid;
            localStorage.wuupldedpropic = dataJson.userprofile.profile_pic;

            localStorage.gender = dataJson.userdetails.gender;
            localStorage.profilecratedforname = dataJson.userdetails.relationship.name;
            localStorage.profilecratedforid = dataJson.userdetails.relationship.id;

            if (dataJson.userdetails.quit_status == 0) {
                window.location.href = "register-phase1.html";
            } else if (dataJson.userdetails.quit_status == 1) {
                window.location.href = "register-phase2.html";
            } else if (dataJson.userdetails.quit_status == 2) {
                window.location.href = "register-phase3.html";
            } else if (dataJson.userdetails.quit_status == 3) {
                window.location.href = "register-phase4.html";
            } else if (dataJson.userdetails.quit_status == 4) {
                window.location.href = "register-phase5.html";
            } else if (dataJson.userdetails.quit_status == 5) {
                window.location.href = "register-phase6.html";
            } else {
                window.location.href = "dashboard.html";
            }
        } else {
            $("#snackbarerror").text("Not already registered.Please do registration!");
            showerrtoast();
        }


    }); //done fn ends here

} //loginjs ends here



function _dologingoogle() {

    $("#loaderspinner").show();

    var uniqueclient = Math.random() * 10000000000000000;
    var postData = JSON.stringify({
        "access_token": localStorage.googleat,
        "client": uniqueclient
    });

    $.ajax({
        url: login_api_google,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {

        },
        error: function(data) {
            $("#loaderspinner").hide();
            // var errtext = "";
            // for (var key in JSON.parse(data.responseText)) {
            //     errtext = JSON.parse(data.responseText)[key][0];
            // }
            $("#snackbarerror").text("Error occured.Try Again!");
            showerrtoast();
        }
    }).done(function(dataJson) {

        $("#loaderspinner").hide();

        if (dataJson.user_exists == true) {

            $("#snackbarsuccs").text("Login Success!");
            showsuccesstoast();

            localStorage.useracccred = JSON.stringify(dataJson);
            localStorage.useridactivate = dataJson.id;
            localStorage.wufname = dataJson.first_name;
            localStorage.wulname = dataJson.last_name;
            localStorage.wutkn = dataJson.token;
            localStorage.userregphoneno = dataJson.username;
            localStorage.usermailid = dataJson.email;
            localStorage.matrimonyid = dataJson.userprofile.uid;
            localStorage.wuupldedpropic = dataJson.userprofile.profile_pic;

            localStorage.gender = dataJson.userdetails.gender;
            localStorage.profilecratedforname = dataJson.userdetails.relationship.name;
            localStorage.profilecratedforid = dataJson.userdetails.relationship.id;

            if (dataJson.userdetails.quit_status == 0) {
                window.location.href = "register-phase1.html";
            } else if (dataJson.userdetails.quit_status == 1) {
                window.location.href = "register-phase2.html";
            } else if (dataJson.userdetails.quit_status == 2) {
                window.location.href = "register-phase3.html";
            } else if (dataJson.userdetails.quit_status == 3) {
                window.location.href = "register-phase4.html";
            } else if (dataJson.userdetails.quit_status == 4) {
                window.location.href = "register-phase5.html";
            } else if (dataJson.userdetails.quit_status == 5) {
                window.location.href = "register-phase6.html";
            } else {
                window.location.href = "dashboard.html";
            }
        } else {

            $("#snackbarerror").text("Not already registered.Please do registration!");
            showerrtoast();

        }

    }); //done fn ends here
}
