$(function () {
  changeYear();
});


function acceptReqSS(me,id){
    $.ajax({
        url: `${domain}user/change-status/${id}/2/`,
        type: 'get',
        headers: {
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function (data) {
            console.log(data);
            $(me).closest('.row').slideUp(250,function () {
              listRequests();
            });
            getMessage(1);

        },
        error: function (edata) {
           console.log(edata);
           $('body').prepend('<h3 class="remove-me" style="background:#ff4081;">oops!! Something went wrong!!</h3>');
           setTimeout(function () { $('.remove-me').fadeOut(function () { $(this).remove();});}, 3000);
        }
    });
    console.log(id);
}


function changeYear () {
  var year=$('#select-year').val();

  var appendHTML=``;
  
    appendHTML+=`<div onclick="listByMonth(this,1,${year});" class="col-lg-1 col-md-1 col-sm-2 col-xs-3 plr4"><p class="month months months_active">Jan'${year.slice(2)}</p></div>`;
    appendHTML+=`<div onclick="listByMonth(this,2,${year});" class="col-lg-1 col-md-1 col-sm-2 col-xs-3 plr4"><p class="month months ">Feb'${year.slice(2)}</p></div>`;
    appendHTML+=`<div onclick="listByMonth(this,3,${year});" class="col-lg-1 col-md-1 col-sm-2 col-xs-3 plr4"><p class="month months">Mar'${year.slice(2)}</p></div>`;
    appendHTML+=`<div onclick="listByMonth(this,4,${year});" class="col-lg-1 col-md-1 col-sm-2 col-xs-3 plr4"><p class="month months">Apr'${year.slice(2)}</p></div>`;
    appendHTML+=`<div onclick="listByMonth(this,5,${year});" class="col-lg-1 col-md-1 col-sm-2 col-xs-3 plr4"><p class="month months">May'${year.slice(2)}</p></div>`;
    appendHTML+=`<div onclick="listByMonth(this,6,${year});" class="col-lg-1 col-md-1 col-sm-2 col-xs-3 plr4"><p class="month months">Jun'${year.slice(2)}</p></div>`;
    appendHTML+=`<div onclick="listByMonth(this,7,${year});" class="col-lg-1 col-md-1 col-sm-2 col-xs-3 plr4"><p class="month months">Jul'${year.slice(2)}</p></div>`;
    appendHTML+=`<div onclick="listByMonth(this,8,${year});" class="col-lg-1 col-md-1 col-sm-2 col-xs-3 plr4"><p class="month months">Aug'${year.slice(2)}</p></div>`;
    appendHTML+=`<div onclick="listByMonth(this,9,${year});" class="col-lg-1 col-md-1 col-sm-2 col-xs-3 plr4"><p class="month months">Sep'${year.slice(2)}</p></div>`;
    appendHTML+=`<div onclick="listByMonth(this,10,${year});" class="col-lg-1 col-md-1 col-sm-2 col-xs-3 plr4"><p class="month months">Oct'${year.slice(2)}</p></div>`;
    appendHTML+=`<div onclick="listByMonth(this,11,${year});" class="col-lg-1 col-md-1 col-sm-2 col-xs-3 plr4"><p class="month months">Nov'${year.slice(2)}</p></div>`;
    appendHTML+=`<div onclick="listByMonth(this,12,${year});" class="col-lg-1 col-md-1 col-sm-2 col-xs-3 plr4"><p class="month months">Dec'${year.slice(2)}</p></div>`;

    

  $('#list-months').html(appendHTML);

  listByMonth ($('.months').eq(0).closest('div.plr4'),1,year);

}

function listByMonth (me,month,year) {
  $('.months').removeClass('months_active');
  $(me).find('.months').addClass('months_active');

  $.ajax({
        url: `${domain}user/list/search-by-date/?year=${year}&month=${month}`,
        type: 'get',
        headers: {
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function (data) {
            console.log(data);
            var appendData=``;
            // data.month_array.length
            var myArray=produceArray(month,data.week_array,data.month_array);
            for (var i = 0; i < myArray.length; i++) {
              if (i%7==0) {
                var date=myArray[i].date.toString().split('-');
                if (i==0) {
                  appendData+=`<tr>`;
                  appendData+=`${typeof myArray[i].profile_views!='undefined'?`<td class="tdhover ${returnColors()}"><div class="daysection"><div class="row"><p class="daynumber floatright">${date.length>1?new Date(myArray[i].date).getDate():myArray[i].date}</p></div><div class="row"><div class="col-md-6"><p class="countnum">${myArray[i].profile_views}</p><p class="countname">Views</p></div><div class="col-md-6"><p class="countnum">${myArray[i].shortlist_profiles}</p><p class="countname">Shortlist</p></div></div><div class="row"><div class="col-md-6"><p class="countnum">${myArray[i].ignored_profiles}</p><p class="countname">Ignored</p></div><div class="col-md-6"></div></div></div></td>`:`<td class="bgwhite"><div class="row"><p class="daynumber floatright onlydate">${new Date(myArray[i].date).getDate()}</p></div></td>`}`;

                }
                else {
                  appendData+=`</tr><tr>`;
                  appendData+=`${typeof myArray[i].profile_views!='undefined'?`<td class="tdhover ${returnColors()}"><div class="daysection"><div class="row"><p class="daynumber floatright">${date.length>1?new Date(myArray[i].date).getDate():myArray[i].date}</p></div><div class="row"><div class="col-md-6"><p class="countnum">${myArray[i].profile_views}</p><p class="countname">Views</p></div><div class="col-md-6"><p class="countnum">${myArray[i].shortlist_profiles}</p><p class="countname">Shortlist</p></div></div><div class="row"><div class="col-md-6"><p class="countnum">${myArray[i].ignored_profiles}</p><p class="countname">Ignored</p></div><div class="col-md-6"></div></div></div></td>`:`<td class="bgwhite"><div class="row"><p class="daynumber floatright onlydate">${new Date(myArray[i].date).getDate()}</p></div></td>`}`;
                }
                
              }
              else{
                appendData+=`${typeof myArray[i].profile_views!='undefined'?`<td class="tdhover ${returnColors()}"><div class="daysection"><div class="row"><p class="daynumber floatright">${date.length>1?new Date(myArray[i].date).getDate():myArray[i].date}</p></div><div class="row"><div class="col-md-6"><p class="countnum">${myArray[i].profile_views}</p><p class="countname">Views</p></div><div class="col-md-6"><p class="countnum">${myArray[i].shortlist_profiles}</p><p class="countname">Shortlist</p></div></div><div class="row"><div class="col-md-6"><p class="countnum">${myArray[i].ignored_profiles}</p><p class="countname">Ignored</p></div><div class="col-md-6"></div></div></div></td>`:`<td class="bgwhite"><div class="row"><p class="daynumber floatright onlydate">${new Date(myArray[i].date).getDate()}</p></div></td>`}`;

              }
              
            }
            appendData+=`</tr>`;


            $('#filter-data').html(appendData);

            console.log(myArray);

        },
        error: function (edata) {
           console.log(edata);
           $('body').prepend('<h3 class="remove-me" style="background:#ff4081;">oops!! Something went wrong!!</h3>');
           setTimeout(function () { $('.remove-me').fadeOut(function () { $(this).remove();});}, 3000);
        }
    });
}

function produceArray(m,w_a,m_a){
  var m_array=[];
  for (var data of w_a) {
    if (parseInt(new Date(data.date).getMonth()+1)!=parseInt(m)) {
      m_array.push({'date':data.date});
    }

    console.log(parseInt(new Date(data.date).getMonth()+1)+'--'+parseInt(m));

  }

  for (var data of m_a) {
    m_array.push({'date':data.date,'ignored_profiles':data.ignored_profiles,'shortlist_profiles':data.shortlist_profiles,'profile_views':data.profile_views});
  }

  return m_array;
}

var colorX=0;
function returnColors () {
  colorX++;
  if (colorX%2==0) {
    return 'bgwhite';
  }
  else {
    return 'greybg';
  }
}