function selectPackage (id) {
    $.ajax({
        url: `${domain}user/package/${id}`,
        type: 'post',
        success: function (data) {
            console.log(data);
        },
        error: function (edata) {
           // console.log(edata);
           $('body').prepend($('<h3 class="remove-me" style="background:#ff4081;">oops!! Something went wrong!!</h3>').hide().fadeIn());
           setTimeout(function () {
                $('.remove-me').fadeOut();
           }, 5000);
        }
    });
}