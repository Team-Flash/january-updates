
$(function () {
    loadChatRoom();
    $('.right').hide()
});
// function denyReq(me,id){
//     console.log(id);
//     $.ajax({
//         url: `${domain}user/change-status/${id}/3/`,
//         type: 'get',
//         headers: {
//             "Authorization": "Token " + localStorage.wutkn
//         },
//         success: function (data) {
//           $(me).closest('.row').slideUp(250,function () {
//           	listRequests();
//           });
          

//         },
//         error: function (edata) {
//            console.log(edata);
//            $('body').prepend('<h3 class="remove-me" style="background:#ff4081;">oops!! Something went wrong!!</h3>');
//            setTimeout(function () { $('.remove-me').fadeOut(function () { $(this).remove();});}, 3000);
//         }
//     });

// }

// $('.chat[data-chat=person3]').addClass('active-chat');

$('.left .person').click(function() {
    if ($(this).hasClass('active')) {
        return false;
    } else {
        var findChat = $(this).attr('data-chat');
        var personName = $(this).find('.name').text();
        $('.right .top .name').html(personName);
        $('.chat').removeClass('active-chat');
        $('.left .person').removeClass('active');
        $(this).addClass('active');
        $('.chat[data-chat = ' + findChat + ']').addClass('active-chat');
    }
        
    console.log($(this).addClass('active'));
});

setInterval(function () {
    loadChatRoom();
}, 5000);
function loadChatRoom() {
    var myself=JSON.parse(localStorage.useracccred).id;
    $.ajax({
        url: `${domain}user/list/chat-rooms/`,
        type: 'get',
        headers: {
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function (data) {
            var appendRooms=``;
            console.log(data);

            for (var roomD of data) {
                if (myself==roomD.sender.id) {
                    appendRooms+=`<li onclick="openChat(${roomD.receiver.id},this,${roomD.id},'${roomD.receiver.first_name} ${roomD.receiver.last_name}');" class="person" data-room-no="${roomD.id}" data-chat="${roomD.receiver.id}"><span class="pendingmsgs" style="background:silver;color:black;border-radius:50px;padding:5px;display: ${roomD.unread_count==0?"none":""};" >${roomD.unread_count}</span><img src="${roomD.receiver.userprofile.profile_pic?roomD.receiver.userprofile.profile_pic:'img/user-128.png'}" alt="${roomD.receiver.first_name} ${roomD.receiver.last_name}" /><span class="name">${roomD.receiver.first_name} ${roomD.receiver.last_name}</span><span class="time room_time">${roomD.created_on?giveMeDuration(roomD.created_on):""}</span><span class="preview room_message">${roomD.recent_message?roomD.recent_message:""}</span></li>`;
                }
                else {
                    appendRooms+=`<li onclick="openChat(${roomD.sender.id},this,${roomD.id},'${roomD.sender.first_name} ${roomD.sender.last_name}');" class="person" data-room-no="${roomD.id}" data-chat="${roomD.sender.id}"><span class="pendingmsgs" style="background:silver;color:black;border-radius:50px;padding:5px;display: ${roomD.unread_count==0?"none":""};">${roomD.unread_count}</span><img src="${roomD.sender.userprofile.profile_pic?roomD.sender.userprofile.profile_pic:'img/user-128.png'}" alt="${roomD.sender.first_name} ${roomD.sender.last_name}" /><span class="name">${roomD.sender.first_name} ${roomD.sender.last_name}</span><span class="time room_time">${roomD.created_on?giveMeDuration(roomD.created_on):""}</span><span class="preview room_message">${roomD.recent_message?roomD.recent_message:""}</span></li>`;
                }
                
            }
            if (data.length<=0) {
                $('#people').html('<center>No accepted requests!!</center>');
            }
            else {
                $('#people').html(appendRooms);
                myFunction();

            }

            if (room) {
                $('[data-room-no="'+room+'"]').addClass('active');
            }
            
            

            
            // $('.person').eq(0).addClass('active')

        },
        error: function (edata) {
           console.log(edata);
           $('body').prepend('<h3 class="remove-me" style="background:#ff4081;">oops!! Something went wrong!!</h3>');
           setTimeout(function () { $('.remove-me').fadeOut(function () { $(this).remove();});}, 3000);
        }
    });
}

var nextURL="";
var myId=undefined;
var room=undefined;
function openChat (meId,me,id,myname) {
        var myself=JSON.parse(localStorage.useracccred).id;
        myId=meId;
        room=id;
        if ($(me).hasClass('active')) {
            return false;
        } else {
            var findChat = id;
            $('.right .top .name').html(myname);
            // $('.chat').removeClass('active-chat');
            $('.left .person').removeClass('active');
            $(me).addClass('active');
            // $('.chat[data-chat = ' + findChat + ']').addClass('active-chat');

            loadChat(nu=0,id);
            startInterval(0);
        }
}


function loadChat (nu,id=0) {
    $.ajax({
        url: nu==0?`${domain}user/list-chats/?pk=${id}`:nextURL,
        type: 'get',
        headers: {
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function (data) {
            var appendRooms=`<br>`;
            nextURL=data.next_url?data.next_url:'';
            var lastMessage=data.results[0];
            // console.log(lastMessage);

            data.results=data.results.reverse();
            for (var room of data.results) {
                

                if (myId==room.sent_by) {
                    appendRooms+=`<div class="bubble you" style="color:black;">${room.message}<div><small style="float:right;">${getHMS(room.created_on)}    ${room.is_read?'':''}</small></div></div>`;
                }
                else {
                    appendRooms+=`<div class="bubble me"  style="color:black;">${room.message}<div><small style="float:right;">${getHMS(room.created_on)}    ${room.is_read?'&#10004;':''}</small></div></div>`;
                }
                
            }
            $('#right-chat').html(appendRooms);

            $('.right').show(function () {
                $('#right-chat').animate({ scrollTop: $('#right-chat').height() }, 500);
                $('[data-room-no='+id+'] .room_time').text(giveMeDuration(lastMessage.created_on));
                $('[data-room-no='+id+'] .room_message').text(lastMessage.message);
            });
            
            // $('.person').eq(0).addClass('active')

        },
        error: function (edata) {
           console.log(edata);
           $('body').prepend('<h3 class="remove-me" style="background:#ff4081;">oops!! Something went wrong!!</h3>');
           setTimeout(function () { $('.remove-me').fadeOut(function () { $(this).remove();});}, 3000);
        }
    });
}

var intervalData=undefined;
var firstload=0;
function startInterval (load=1) {
    clearInterval(intervalData);
    
    intervalData=setInterval(function () {
        if (firstload) {
            loadChat(0,room);
        }
        else {
            firstload=1;
            loadChat(0,room);
        }
    }, 5000);
}



function sendMessage(e){
    e.preventDefault();

    if ($('#message-data').val()!='') {

        var postData={"id": myId,"chat_room": room,"message": $('#message-data').val()};
        $.ajax({
            url: `${domain}user/send-message/`,
            type: 'post',
            data: JSON.stringify(postData),
            headers: {
                "Authorization": "Token " + localStorage.wutkn,
                "content-type":"application/json"
            },
            success: function (data) {
                $('#right-chat').append(`<div class="bubble me" style="color:black;">${$('#message-data').val()}<div><small>${Date.now().toString('hh:mm tt')}</small></div></div>`);
                $('#right-chat').animate({ scrollTop: $('#right-chat').height() }, 500);

                $('[data-room-no='+room+'] .room_time').text(Date.now().toString('hh:mm tt'));
                $('[data-room-no='+room+'] .room_message').text($('#message-data').val());

                $('#message-data').val('');

            },
            error: function (edata) {
               console.log(edata);
               $('body').prepend('<h3 class="remove-me" style="background:#ff4081;">oops!! Something went wrong!!</h3>');
               setTimeout(function () { $('.remove-me').fadeOut(function () { $(this).remove();});}, 3000);
            }
        });    
    }
    
}

$('#right-chat').scroll(function(event) {
    // console.log($(this).offset().top);
    var elementHeight = $(this).height(); 
    var scrollPosition = $(this).height() + $(this).scrollTop(); 
    
    // console.log(elementHeight == scrollPosition);
    if (elementHeight == scrollPosition) {
        
        clearInterval(intervalData);
        
        console.log(nextURL);
        if (nextURL!='') {

            $.ajax({
                url: nextURL,
                type: 'get',
                headers: {
                    "Authorization": "Token " + localStorage.wutkn
                },
                success: function (data) {
                    var appendRooms=`<br>`;
                    console.log(data);
                    nextURL=data.next_url?data.next_url:'';
                    data.results=data.results.reverse();

                    for (var room of data.results) {
                    
                        if (myId==room.sent_by) {
                            appendRooms+=`<div class="bubble you" style="color:black;">${room.message}<div><small>${getHMS(room.created_on)}    ${room.is_read?'&#10004;':''}</small></div></div>`;
                        }
                        else {
                            appendRooms+=`<div class="bubble me"  style="color:black;">${room.message}<div><small>${getHMS(room.created_on)}    ${room.is_read?'&#10004;':''}</small></div></div>`;
                        }
                        
                    }
                    $('#right-chat').prepend(appendRooms);

                    // $('.person').eq(0).addClass('active')

                },
                error: function (edata) {
                   console.log(edata);
                   $('body').prepend('<h3 class="remove-me" style="background:#ff4081;">oops!! Something went wrong!!</h3>');
                   setTimeout(function () { $('.remove-me').fadeOut(function () { $(this).remove();});}, 3000);
                }
            });



        }
    }
    else {
        startInterval();
    }
});


function myFunction() {
  var input, filter, table, tr, td, i;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  tr = document.querySelectorAll('#people .person');

  for (i = 0; i < tr.length; i++) {
    if ($(tr[i]).text().toUpperCase().indexOf(filter)>-1) {
        $(tr[i]).css('display','');
    }
    else{
        $(tr[i]).css('display','none');
    }
    
  }
}


function giveMeDuration (duration) {
    // console.log(duration);

    var date=duration.split('T')[0];
    if (new Date(date).toLocaleDateString() == new Date().toLocaleDateString()) {
        // console.log(Date.parse(new Date(duration).toTimeString().split('(')[0]).toString('hh : mm tt'));
        // return Date.parse(new Date(duration).toTimeString().split('(')[0]).toString('hh : mm tt');
        return getHMS(duration);
    }
    else{
        // console.log(Date.parse(new Date(duration).toTimeString().split('(')[0]).toString('dd / MM / yyyy'));
        return new Date(duration).toDateString()
    }

    // return new Date(duration).toDateString();
    
}



var getHMS=d=>{
    var mydate=new Date(d);
    var addZero=d=>d<=9?`0${d}`:`${d}`;
    return `${mydate.getHours()-12<0?addZero(mydate.getHours()):addZero(mydate.getHours()-12)} : ${addZero(mydate.getMinutes())} ${mydate.getHours()-12<0?'AM':'PM'}`
};

