$(function () {
	// console.log('hello');
	$('#signup-profile-for').select2();
	$('#signup-religion').select2();
	$('#mother-tongue').select2();
	$('.loadericon').hide();
	getDropdown();
});

$('#submit-signup-button').click(function(event) {
	event.preventDefault();
	var valid=true;
	for (var elems of document.querySelectorAll('.signup-field')) {
		if (!elems.validity.valid) {
			elems.reportValidity();
			valid=false;
			// $(elems).focus();
			// $(elems).after('<h5 class="remove-me" style="background:#ff4081;">'+elems.validationMessage+'</h5>');
   //          setTimeout(function () {
   //          	$('.remove-me').remove();
   //          },5000);
			
			break;
		}
	}

	if (valid) {
		$('.loadericon').show();

		var postData={"first_name":$('#signup-fname').val(),"last_name":$('#signup-lname').val(),"username":$('#signup-mobile').val(),"email":$('#signup-email').val(),"password":$('#signup-password').val(),"userdetails":{"relationship":$('#signup-profile-for').val(),"country_code":$('#country').text(),"gender":$('[name="gender-radio"]').val(),"dob":$('#dob').val(),"mother_tongue":$('#mother-tongue').val(),"religion":$('#signup-religion').val()}};

		$.ajax({
	        url: `${domain}auth/registration/`,
	        type: 'post',
	        data: JSON.stringify(postData),
	        headers: {
	            'content-type': 'application/json'
	        },
	        success: function (data) {
	           	console.log(data);
				$('.loadericon').hide();
	           	
	           	// $('body').prepend('<h3 class="remove-me">Message!!</h3>')
	           	// setTimeout(function () {
	           	// 	$('.remove-me').remove();
	           	// }, 5000);

	           	$('input,select').val('').focusout();
	           	localStorage.BMUser=JSON.stringify(data);
	           	window.location.href=""
	        },
	        error: function (edata) {
	           console.log(edata);
	           $('.loadericon').hide();
	           $('body').prepend('<h3 class="remove-me" style="background:#ff4081;">oops!! Something went wrong!!</h3>')
               setTimeout(function () {
                $('.remove-me').animate({
                	top: '100%'},
                	500, function() {
                			$('.remove-me').remove();
                		});
               }, 5000);
	        }
	    });

	}
});

function getDropdown () {
	$.ajax({
        url: `${domain}user/list/drop-down/sign-up/`,
        type: 'get',
        success: function (data) {
           	console.log(data);
           	var profile='<option value="">Profile For *</option>';
           	var religion=`<option selected="" value="">Religion *</option>`;
           	var mt=`<option selected="" value="">Mother Tongue *</option>`;
           	for (var prof of data.relationship) {
           		profile+=`<option value="${prof.id}">${prof.name}</option>`;
           	}
           	for (var rel of data.religion) {
           		religion+=`<option value="${rel.id}">${rel.name}</option>`;
           	}
           	for (var mton of data.mother_tongue) {
           		mt+=`<option value="${mton.id}">${mton.name}</option>`;
           	}
           	$('#signup-profile-for').append(profile);
           	$('#signup-religion').append(religion);
           	$('#mother-tongue').append(mt);
           	// data.country={};
           	// data.country.country_code="AF";
           	var cCode=data.country?($('[data-code="'+data.country.country_code+'"]').text().split('(')[1].split(')')[0]):'+91';
           	$('#country').text(cCode);
           	// console.log(cCode);
        },
        error: function (edata) {
           console.log(edata);
           $('body').prepend('<h3 class="remove-me" style="background:#ff4081;">oops!! Something went wrong!!</h3>');

           setTimeout(function () {
            $('.remove-me').animate({
            	top: '100%'},
            	500, function() {
            			$('.remove-me').remove();
            		});
           }, 5000);
        }
    });
}