// const domain="http://192.168.10.35:8000/";
var domain = "https://matrimony.billioncart.in/";



// var reqStatus={"1": "Pending","2": "Accepted","3": "Rejected","4":"Deleted"};
// $(".showsentrequests").hide();
// $(".shownewrequests").show();
// function clicknew(me) {
//     $('.tablist').removeClass('activeunderline');
//     $(me).closest('.tablist').addClass('activeunderline');
//     $(".showsentrequests").hide();
//     $(".shownewrequests").show();
// }
// function clicksent(me) {
//     $('.tablist').removeClass('activeunderline');
//     $(me).closest('.tablist').addClass('activeunderline');
//     $(".shownewrequests").hide();
//     $(".showsentrequests").show();

// }

// function listRequests () {
// 	$.ajax({
//         url: `${domain}user/list/requests/`,
//         type: 'get',
//         headers: {
//             "Authorization": "Token " + localStorage.wutkn
//         },
//         success: function (data) {
//             var appendNew='';
//             var appendSent='';
            
//             $('.shownewrequests').empty();
//             $('.showsentrequests').empty();

//             if (data.new_requests.length<=0) {
//             	$('.shownewrequests').html('<center><p>There are no new request!!</p></center>');
//             }
//             if (data.sent_request.length<=0) {
//             	$('.showsentrequests').html('<center><p>There are no sent request!!</p></center>');
//             }
//             for (var newMessage of data.new_requests) {
//                 appendNew+=`<div class="row" data-new-req="${newMessage.id}"><div class="col-md-3 col-sm-3 col-xs-3 sidebarimgsection"><img src="${newMessage.user.userprofile.profile_pic?newMessage.user.userprofile.profile_pic:'img/assets/coupleindex.svg'}" class="img-responsive sidebarimg"></div><div class="col-md-9 col-sm-9 col-xs-9"><p class="sidebarusername">${newMessage.user.first_name} ${newMessage.user.last_name}</p><p class="sideusercontent">Lives in <span class="pinkcolor">${newMessage.user.user_address?newMessage.user.user_address.city.name:'- -'}</span></p><a class="btn btn--primary prl20" href="#"><span class="btn__text" onclick="acceptReq(this,${newMessage.id});">Accept</span></a><a class="btn btn--primary prl20 ml3" href="#"><span class="btn__text" onclick="denyReq(this,${newMessage.id});">Deny</span></a></div><hr class="usershr"></div>`;
//             }
//             for (var sentMessage of data.sent_request) {
//                 appendSent+=`<div class="row" style="box-shadow: 1px 10px 1px -10px rgba(0, 0, 0, 0.38);"><div class="col-md-3 col-sm-3 col-xs-3 sidebarimgsection"><img src="${sentMessage.actor.userprofile.profile_pic?sentMessage.actor.userprofile.profile_pic:'img/assets/coupleindex.svg'}" class="img-responsive sidebarimg"></div><div class="col-md-9 col-sm-9 col-xs-9"><p class="sidebarusername">${sentMessage.actor.first_name} ${sentMessage.actor.last_name}</p><p class="sideusercontent">Lives in <span class="pinkcolor">${sentMessage.actor.user_address?sentMessage.actor.user_address.city.name:'- -'}</span></p><p class="sideusercontent">Status : <span class="spanbold">${reqStatus[sentMessage.status]}</span></p></div><hr></div>`;
//             }

//             $('.shownewrequests').append(appendNew);
//             $('.showsentrequests').append(appendSent);



//         },
//         error: function (edata) {
//            // console.log(edata);
//            $('body').prepend($('<h3 class="remove-me" style="background:#ff4081;">oops!! Something went wrong!!</h3>').hide().fadeIn());
//            setTimeout(function () {
//                 $('.remove-me').fadeOut();
//            }, 5000);
//         }
//     });
// }
// $(function () {
//     listRequests();
// });

// function acceptReq(me,id){
//     $.ajax({
//         url: `${domain}user/change-status/${id}/2/`,
//         type: 'get',
//         headers: {
//             "Authorization": "Token " + localStorage.wutkn
//         },
//         success: function (data) {
//             console.log(data);
//             $(me).closest('.row').slideUp(250,function () {
//             	listRequests();
//             });
//         },
//         error: function (edata) {
//            console.log(edata);
//            $('body').prepend('<h3 class="remove-me" style="background:#ff4081;">oops!! Something went wrong!!</h3>');
//            setTimeout(function () { $('.remove-me').fadeOut(function () { $(this).remove();});}, 3000);
           
//         }
//     });
//     console.log(id);
// }

// function denyReq(me,id){
//     console.log(id);
//     $.ajax({
//         url: `${domain}user/change-status/${id}/3/`,
//         type: 'get',
//         headers: {
//             "Authorization": "Token " + localStorage.wutkn
//         },
//         success: function (data) {
//           $(me).closest('.row').slideUp(250,function () {
//           	listRequests();
//           });
          

//         },
//         error: function (edata) {
//            console.log(edata);
//            $('body').prepend('<h3 class="remove-me" style="background:#ff4081;">oops!! Something went wrong!!</h3>');
//            setTimeout(function () { $('.remove-me').fadeOut(function () { $(this).remove();});}, 3000);
//         }
//     });

// }