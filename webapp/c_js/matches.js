var count_data = 0;
var fromLoad = 0;

$(function() {
    changeFilter($('.filter-list')[0], 1);
});

var nextURL = 'asd';
var filterData = { "1": "New Matches", "4": "Yet To be viewed", "2": "Shortlisted matches", "3": "Mutual Matches", "6": "Members looking for me", "5": "Premium members", "7": "Daily Matches" };

function changeFilter(me, id, nu = 0) {
     $("#loaderspinner").show();
    if (nu == 0) {
        $('#profile-heading').html(`${filterData[id]}&nbsp;<span class="pinkcolor" id="profile-filter-count">(0)</span>`);
        $('.filter-list').removeClass('activeunderline');
        $(me).addClass('activeunderline');
        $('#profiles').empty();
    }

    var urlData = nu == 1 ? nextURL : `${domain}user/list-matches/?type=${id}&page=1`;

    $.ajax({
        url: urlData,
        type: 'get',
        headers: {
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            // console.log(data);

            if (!data.results.length > 0) {
                 $('#profiles').empty().append(`<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><center><img src="img/assets/nodata.svg" style="width:40%"></center></div>`);
            }

            var appendHTML = '';
            for (var pro of data.results) {
                appendHTML += `<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6"><div class="card card-2 text-center"><div class="card__top"><a href="javascript:visitProfile(${pro.id})" target="_blank" style="background: url(${pro.userprofile.profile_pic?pro.userprofile.profile_pic:'img/assets/coupleindex.svg'}) top center / cover no-repeat;display: block;height: 140px;"></a></div><div class="card__body p10"><p class="username dynname${pro.id}">${pro.first_name} ${pro.last_name}</p><p class="livesin">Lives in <span class="spanlivesin">${pro.user_address?pro.user_address.city.name:'- -'}</span></p></div><div class="card__bottom text-center ptb3"><div class="card__action"><a href="javascript:void(0)" onclick="likeprofile(this,${pro.id});"><i class="fa fa-heart hoverpink accepticon ${pro.is_liked?'shortlisted':''}" aria-hidden="true"></i></a></div><div class="card__action"><a href="javascript:void(0)" onclick="shortlistProfile(this,${pro.id});"><i class="fa fa-star hoverpink accepticon ${pro.is_shortlisted?'shortlisted':''}" aria-hidden="true"></i></a></div><div class="card__action"><a href="javascript:void(0)" onclick="ignoreProfile(this,${pro.id});"><i class="fa fa-thumbs-down hoverblack rejecticon ${pro.is_ignored?'ignored':''}" aria-hidden="true"></i></a></div></div></div></div>`;
            }

            $('#profiles').append(appendHTML);
            count_data = fromLoad == 1 ? (count_data += data.results.length) : data.results.length;
            fromLoad = 0;
            $('#profile-filter-count').html(`(${count_data})`);
            if (data.next_url) {
                nextURL = data.next_url;
                $('#loadmore').show();
            } else {
                nextURL = '';
                $('#loadmore').hide();
            }
             $("#loaderspinner").hide();
        },
        error: function(edata) {
            $('#profiles').empty().append(`<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6"><center><img src="img/assets/nodata.svg" style="width:40%"></center></div>`);
             $("#loaderspinner").hide();
        }
    });
} //change filter fn ends here

//shortlist profile fn starts here
function shortlistProfile(me, id) {
    var postData = { "actor": id };
    $.ajax({
        url: `${domain}user/shortlist-profile/`,
        type: 'post',
        headers: {
            "content-type": "application/json",
            "Authorization": "Token " + localStorage.wutkn
        },
        data: JSON.stringify(postData),
        success: function(data) {
            $(me).find('i').toggleClass('shortlisted');
            $("#snackbarsuccs").text("Your request has been send to "+ $(".dynname"+id).text());
            showsuccesstoast();
        },
        error: function(edata) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
} //shortlist profile fn ends here

//ignore profile fn starts here
function ignoreProfile(me, id) {
    var postData = { "actor": id };
    $.ajax({
        url: `${domain}user/ignore-profile/`,
        type: 'post',
        headers: {
            "content-type": "application/json",
            "Authorization": "Token " + localStorage.wutkn
        },
        data: JSON.stringify(postData),
        success: function(data) {
            $(me).find('i').toggleClass('ignored');
            $("#snackbarsuccs").text("Your request has been send to "+ $(".dynname"+id).text());
            showsuccesstoast();
        },
        error: function(edata) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
} //ignore profile fn ends here

//like other people profile fn starts here
function likeprofile(me, id) {
    var postData = { "actor": id };
    $.ajax({
        url: `${domain}user/like-profiles/`,
        type: 'post',
        headers: {
            "content-type": "application/json",
            "Authorization": "Token " + localStorage.wutkn
        },
        data: JSON.stringify(postData),
        success: function(data) {
            $(me).find('i').toggleClass('ignored');
            $("#snackbarsuccs").text("Your request has been send to "+ $(".dynname"+id).text());
            showsuccesstoast();
        },
        error: function(edata) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
} //like other people profile fn ends here

function loadMore() {
    fromLoad = 1;
    changeFilter(undefined, undefined, 1);
}

function visitProfile(id) {
    localStorage.userid_toseeprof = id;
    window.open("profile.html", '_blank');
}