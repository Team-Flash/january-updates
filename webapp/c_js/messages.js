$('.chat[data-chat=person3]').addClass('active-chat');
$('.person[data-chat=person3]').addClass('active');

$('.left .person').mousedown(function() {
    if ($(this).hasClass('.active')) {
        return false;
    } else {
        var findChat = $(this).attr('data-chat');
        var personName = $(this).find('.name').text();
        $('.right .top .name').html(personName);
        $('.chat').removeClass('active-chat');
        $('.left .person').removeClass('active');
        $(this).addClass('active');
        $('.chat[data-chat = ' + findChat + ']').addClass('active-chat');
    }
});

$(function() {
    getMessage(1);

    $('.tab ul.tabs').addClass('active').find('> li:eq(0)').addClass('current');

    $('.tab ul.tabs li a').click(function(g) {
        var tab = $(this).closest('.tab'),
            index = $(this).closest('li').index();

        tab.find('ul.tabs > li').removeClass('current');
        $(this).closest('li').addClass('current');

        tab.find('.tab_content').find('div.tabs_item').not('div.tabs_item:eq(' + index + ')').slideUp();
        tab.find('.tab_content').find('div.tabs_item:eq(' + index + ')').slideDown();

        g.preventDefault();
    });
});


$(function() {
    $(".inboxsection").show();
    $(".sentsection").hide();
    $(".chatsection").hide();
    $(".filteredsection").hide();
    $(".trashsection").hide();
    getMID();

});


function clickinbox() {
    $(".inboxsection").show();
    $(".sentsection").hide();
    $(".chatsection").hide();
    $(".filteredsection").hide();
    $(".trashsection").hide();
    $('.msgboxitems').removeClass('active');
    $('.msgboxitems').eq(0).addClass('active');
    getMessage(1);
}

function clicksentitems() {
    $(".sentsection").show();
    $(".inboxsection").hide();
    $(".chatsection").hide();
    $(".filteredsection").hide();
    $(".trashsection").hide();
    $('.msgboxitems').removeClass('active');
    $('.msgboxitems').eq(1).addClass('active');
    getMessage(5);
}

function clickchat() {
    $(".chatsection").show();
    $(".inboxsection").hide();
    $(".sentsection").hide();
    $(".filteredsection").hide();
    $(".trashsection").hide();
    $('.msgboxitems').removeClass('active');
    $('.msgboxitems').eq(2).addClass('active');
    getMessageData(8);
}


function clicktrash() {
    $(".trashsection").show();
    $(".inboxsection").hide();
    $(".sentsection").hide();
    $(".filteredsection").hide();
    $(".chatsection").hide();
    $('.msgboxitems').removeClass('active');
    $('.msgboxitems').eq(3).addClass('active');
    getMessage(4);
}

var nextURL='';
// var messageTypeId=undefined;
function getMessageData(id,nu=0) {
  var urlData='';
  nu==0?(urlData=`${domain}user/request/${id}`):(urlData=nextURL);
  $.ajax({
    url: `${domain}user/request/${id}`,
    type: 'get',
    success: function(data) {
        var appendInboxPending = '';
        var appendInboxAccepted = '';
        var appendInboxDeclined = '';
        var appendSentAll = '';
        var appendSentAwaiting = '';
        var appendChat = '';
        var appendTrash = '';


        if (data.messageTypeId==1) {
          if (data.inbox.pending.length) {
            for (var message of data.inbox.pending) {
              appendInboxPending+=`<div class="row"><div class="col-md-2 col-xs-12"><img src="img/assets/image8.png"></div><div class="col-md-10 col-xs-12"><h4 class="msgpageuserimg">Mr. Ramesh&nbsp;<span>(BM001234)</span><span class="msgpagedate"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>&nbsp;16-May-2017&emsp;<a href="" data-toggle="modal" data-target="#deletemodal"><!--<i class="fa fa-trash-o trashicon" aria-hidden="true"></i>--></a></span></h4><a href="javascript:void(0)" onclick="viewMobile();" class="msgpageanchor"><i class="fa fa-mobile viewnumbericon" aria-hidden="true"></i>&emsp;View Contact Details</a><p class="f13"><span class="">30 years</span>, <span class="">5 ft 2 inches</span> / <span class="">164cms</span>, <span class="">Hindu</span> : <span class="">Nadar</span>, <span class="">Virudhachalam</span>, <span class="">Tamil Nadu</span>, <span class="">India</span>, <span class="">BE</span>, <span class="">Software Engineer</span></p><a class="btn btn--primary prl20 msgreceivedbtn" href="communication-history.html"><span class="btn__text">Interest Received</span></a><p class="msgpagecontent">He is interested in your profile. Would you like to communicate further?</p><a class="btn btn--primary prl20" href="#"><span class="btn__text">Yes</span></a><a class="btn btn--primary prl20 ml3 nobtn" href="#"><span class="btn__text">Not Interested</span></a><a class="btn btn--primary prl20 ml3 nobtn" href="#"><span class="btn__text">Send Mail</span></a></div><hr class="messageshr"></div>`;
            }
          }
          else {
            appendInboxPending+=`<div class="row"><p><center>Currently there is no communication in this folder.</center></p></div>`;
            $('#loadmore-inbox-pending').hide();

          }
          
          if (data.inbox.pending.length) {
            for (var message of data.inbox.accepted) {
              appendInboxAccepted+=`<div class="row"><div class="col-md-2 col-xs-12"><img src="img/assets/image8.png"></div><div class="col-md-10 col-xs-12"><h4 class="msgpageuserimg">Mr. Praveen Kumar&nbsp;<span>(BM001234)</span><!--<span class="premiummem">Premium Member</span>--><span class="msgpagedate"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>&nbsp;16-May-2017&emsp;<a href="" data-toggle="modal" data-target="#deletemodal"><!--<i class="fa fa-trash-o trashicon" aria-hidden="true"></i>--></a></span></h4><a href="javascript:void(0)" onclick="viewMobile();" class="msgpageanchor"><i class="fa fa-mobile viewnumbericon" aria-hidden="true"></i>&emsp;View Contact Details</a><p class="f13"><span class="">30 years</span>, <span class="">5 ft 2 inches</span> / <span class="">164cms</span>, <span class="">Hindu</span> : <span class="">Nadar</span>, <span class="">Virudhachalam</span>, <span class="">Tamil Nadu</span>, <span class="">India</span>, <span class="">BE</span>, <span class="">Software Engineer</span></p><a class="btn btn--primary prl20 msgreceivedbtn" target="_blank;" href="communication-history.html"><span class="btn__text">Interest Accepted</span></a><p class="msgpagecontent">You have accepted his interest.</p><a class="btn btn--primary prl20" href="#" data-toggle="modal" data-target="#myModal"><span class="btn__text">Send Mail</span></a><a class="btn btn--primary prl20 ml3 nobtn" href="#" data-toggle="modal" data-target="#myModal"><span class="btn__text">Call Now</span></a></div><hr class="messageshr"></div>`;
            }
          }
          else {
            appendInboxAccepted+=`<div class="row"><p><center>Currently there is no communication in this folder.</center></p></div>`;
            $('#loadmore-inbox-accepted').hide();
          }

          if (data.inbox.declined.length) {
            for (var message of data.inbox.declined) {
              appendInboxDeclined+=`<div class="row"><div class="col-md-2 col-xs-12"><img src="img/assets/image9.jpg"></div><div class="col-md-10 col-xs-12"><h4 class="msgpageuserimg">Mr. Rohith&nbsp;<span>(BM001234)</span><!--<span class="premiummem">Premium Member</span>--><span class="msgpagedate"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>&nbsp;16-May-2017&emsp;<a href="" data-toggle="modal" data-target="#deletemodal"><!--<i class="fa fa-trash-o trashicon" aria-hidden="true"></i>--></a></span></h4><a href="" data-toggle="modal" data-target="#myModal" class="msgpageanchor"><i class="fa fa-mobile viewnumbericon" aria-hidden="true"></i>&emsp;View Contact Details</a><p class="f13"><span class="">30 years</span>, <span class="">5 ft 2 inches</span> / <span class="">164cms</span>, <span class="">Hindu</span> : <span class="">Nadar</span>, <span class="">Virudhachalam</span>, <span class="">Tamil Nadu</span>, <span class="">India</span>, <span class="">BE</span>, <span class="">Software Engineer</span></p><a class="btn btn--primary prl20 msgreceivedbtn" target="_blank;" href="communication-history.html"><span class="btn__text">Message Declined</span></a><a class="btn btn--primary prl20 msgreceivedbtn ml5" data-toggle="modal" data-target="#blockprofilemodal"><span class="btn__text">Block this Profile</span></a><p class="msgpagecontent">You have declined his message.</p></div><hr class="messageshr"></div>`;
            }
          }
          else {
            appendInboxDeclined+=`<div class="row"><p><center>Currently there is no communication in this folder.</center></p></div>`;
            $('#loadmore-inbox-declined').hide();
          }

          

          $('#inbox-pending').append(appendInboxPending);
          $('#inbox-accepted').append(appendInboxAccepted);
          $('#inbox-declined').append(appendInboxDeclined);

        }
        else if (data.messageTypeId==2) {
          if (data.sent.all.length) {
            for (var message of data.sent.all) {
              appendSentAll+=`<div class="row"><div class="col-md-2 col-xs-12"><img src="img/assets/image8.png"></div><div class="col-md-10 col-xs-12"><h4 class="msgpageuserimg">Mr. Praveen Kumar&nbsp;<span>(BM001234)</span><!--<span class="premiummem">Premium Member</span>--><span class="msgpagedate"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>&nbsp;16-May-2017&emsp;<a href="" data-toggle="modal" data-target="#deletemodal"><!--<i class="fa fa-trash-o trashicon" aria-hidden="true"></i>--></a></span></h4><a href="" data-toggle="modal" data-target="#myModal" class="msgpageanchor"><i class="fa fa-mobile viewnumbericon" aria-hidden="true"></i>&emsp;View Contact Details</a><p class="f13"><span class="">30 years</span>, <span class="">5 ft 2 inches</span> / <span class="">164cms</span>, <span class="">Hindu</span> : <span class="">Nadar</span>, <span class="">Virudhachalam</span>, <span class="">Tamil Nadu</span>, <span class="">India</span>, <span class="">BE</span>, <span class="">Software Engineer</span></p><a class="btn btn--primary prl20 msgreceivedbtn" target="_blank;" href="communication-history.html"><span class="btn__text">Interest Accepted</span></a><p class="msgpagecontent">You have accepted his interest.</p><a class="btn btn--primary prl20" href="#" data-toggle="modal" data-target="#myModal"><span class="btn__text">Send Mail</span></a><a class="btn btn--primary prl20 ml3 nobtn" href="#" data-toggle="modal" data-target="#myModal"><span class="btn__text">Call Now</span></a></div><hr class="messageshr"></div>`;
            }
          }
          else {
            appendSentAll+=`<div class="row"><p><center>Currently there is no communication in this folder.</center></p></div>`;
            $('#loadmore-messages-sent-all').hide();
          }

          if (data.sent.awaiting.length) {
            for (var message of data.sent.awaiting) {
              appendSentAwaiting+=`<div class="row"><div class="col-md-2 col-xs-12"><img src="img/assets/image8.png"></div><div class="col-md-10 col-xs-12"><h4 class="msgpageuserimg">Mr. Praveen Kumar&nbsp;<span>(BM001234)</span><!--<span class="premiummem">Premium Member</span>--><span class="msgpagedate"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>&nbsp;16-May-2017&emsp;<a href="" data-toggle="modal" data-target="#deletemodal"><!--<i class="fa fa-trash-o trashicon" aria-hidden="true"></i>--></a></span></h4><a href="" data-toggle="modal" data-target="#myModal" class="msgpageanchor"><i class="fa fa-mobile viewnumbericon" aria-hidden="true"></i>&emsp;View Contact Details</a><p class="f13"><span class="">30 years</span>, <span class="">5 ft 2 inches</span> / <span class="">164cms</span>, <span class="">Hindu</span> : <span class="">Nadar</span>, <span class="">Virudhachalam</span>, <span class="">Tamil Nadu</span>, <span class="">India</span>, <span class="">BE</span>, <span class="">Software Engineer</span></p><a class="btn btn--primary prl20 msgreceivedbtn" target="_blank;" href="communication-history.html"><span class="btn__text">Interest Accepted</span></a><p class="msgpagecontent">You have accepted his interest.</p><a class="btn btn--primary prl20" href="#" data-toggle="modal" data-target="#myModal"><span class="btn__text">Send Mail</span></a><a class="btn btn--primary prl20 ml3 nobtn" href="#" data-toggle="modal" data-target="#myModal"><span class="btn__text">Call Now</span></a></div><hr class="messageshr"></div>`;
            }
            
          }
          else {
            appendSentAwaiting+=`<div class="row"><p><center>Currently there is no communication in this folder.</center></p></div>`;
            $('#loadmore-messages-sent-awaiting').hide();
          }
          
          $('#messages-sent-all').append(appendSentAll);
          $('#messages-sent-awaiting').append(appendSentAwaiting);
        }
        else if (data.messageTypeId==3) {
          if (data.chat.length) {
            for (var message of data.chat) {
              appendChat+=`<div class="row"><div class="col-md-2 col-xs-12"><img src="img/assets/image7.jpg"></div><div class="col-md-10 col-xs-12"><h4 class="msgpageuserimg">Mr. Rohith&nbsp;<span>(BM001234)</span><!--<span class="premiummem">Premium Member</span>--><span class="msgpagedate"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>&nbsp;16-May-2017&emsp;<a href="" data-toggle="modal" data-target="#deletemodal"><!--<i class="fa fa-trash-o trashicon" aria-hidden="true"></i>--></a></span></h4><a href="" data-toggle="modal" data-target="#myModal" class="msgpageanchor"><i class="fa fa-mobile viewnumbericon" aria-hidden="true"></i>&emsp;View Contact Details</a><p class="f13"><span class="">30 years</span>, <span class="">5 ft 2 inches</span> / <span class="">164cms</span>, <span class="">Hindu</span> : <span class="">Nadar</span>, <span class="">Virudhachalam</span>, <span class="">Tamil Nadu</span>, <span class="">India</span>, <span class="">BE</span>, <span class="">Software Engineer</span></p><a class="btn btn--primary prl20 msgreceivedbtn" target="_blank;" href="communication-history.html"><span class="btn__text">Message Received</span></a><p class="msgpagecontent">He has sent you a message.<br>"Iam interested on you... This is my Whatsapp Number 8939142452"<br>He is interested in your profile. Would you like to communicate further?</p></div><hr class="messageshr"></div>`;
            }
          }
          else {
            appendChat+=`<div class="row"><p><center>Currently there is no communication in this folder.</center></p></div>`;
            $('#loadmore-chats').hide();
          }
          $('#messages-chats').append(appendChat);


        }
        else if (data.messageTypeId==4) {
          if (data.trash.length) {
            for (var message of data.trash) {
              appendTrash+=`<div class="row"><div class="col-md-2 col-xs-12"><img src="img/assets/image7.jpg"></div><div class="col-md-10 col-xs-12"><h4 class="msgpageuserimg">Mr. Rohith&nbsp;<span>(BM001234)</span><!--<span class="premiummem">Premium Member</span>--><span class="msgpagedate"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>&nbsp;16-May-2017&emsp;<a href="" data-toggle="modal" data-target="#deletemodal"><!--<i class="fa fa-trash-o trashicon" aria-hidden="true"></i>--></a></span></h4><a href="" data-toggle="modal" data-target="#myModal" class="msgpageanchor"><i class="fa fa-mobile viewnumbericon" aria-hidden="true"></i>&emsp;View Contact Details</a><p class="f13"><span class="">30 years</span>, <span class="">5 ft 2 inches</span> / <span class="">164cms</span>, <span class="">Hindu</span> : <span class="">Nadar</span>, <span class="">Virudhachalam</span>, <span class="">Tamil Nadu</span>, <span class="">India</span>, <span class="">BE</span>, <span class="">Software Engineer</span></p><a class="btn btn--primary prl20 msgreceivedbtn" target="_blank;" href="communication-history.html"><span class="btn__text">Message Received</span></a><p class="msgpagecontent">He has sent you a message.<br>"Iam interested on you... This is my Whatsapp Number 8939142452"<br>He is interested in your profile. Would you like to communicate further?</p></div><hr class="messageshr"></div>`;
            }
          }
          else {
            appendTrash+=`<div class="row"><p><center>Currently there is no communication in this folder.</center></p></div>`;
            $('#loadmore-trash').hide();
          }

          $('#messages-trash').append(appendTrash);

        }
        
        data.nextURL?(nextURL=data.nextURL):(nextURL='');

    },
    error: function(edata) {
        // console.log(edata);
        $('body').prepend($('<h3 class="remove-me" style="background:#ff4081;">oops!! Something went wrong!!</h3>').hide().fadeIn());
        setTimeout(function() {
            $('.remove-me').fadeOut();
        }, 5000);
    }
  });
}

function loadMore () {
  getMessageData(undefined,1)
}



var nextURL='';
var currentViewId=undefined;

var sections={"1":"inbox-pending","2":"inbox-accepted","3":"inbox-declined","4":"messages-trash","5":"messages-sent-pending","6":"messages-sent-accepted","7":"messages-sent-declined","8":"messages-chats"};

function getMessage(id,nu=0) {
  currentViewId=id;
  
  var urlData='';
  nu==0?(urlData=`${domain}user/list/messages/?type=${id}`):(urlData=nextURL);
  
  $.ajax({
    url: urlData,
    type: 'get',
    headers: {
        "Authorization": "Token " + localStorage.wutkn
    },
    success: function (data) {
        console.log(data);
        var appendHTML=``;
        if (id<=4) {
          
            if (data.results.length<=0) {
                appendHTML+=`<div class="row"><p><center>Currently there is no communication in this folder.</center></p></div>`;
            }
            for (var req of data.results) {
                var dob=req.user.userdetails.dob;
                var buttons=id==1?`<a class="btn btn--primary prl20" href="javascript:void(0)" onclick="acceptReqSS(this,${req.id})"><span class="btn__text">Yes</span></a><a class="btn btn--primary prl20" href="javascript:void(0)" onclick="denyReqSS(this,${req.id})"><span class="btn__text">Not Interested</span></a>`:``;

                appendHTML+=`<div class="row" data-req-id="${req.id}"><div class="col-md-2 col-xs-12" imgsrc="${req.user.userprofile.profile_pic?req.user.userprofile.profile_pic:'img/assets/coupleindex.svg'}"><a class="pointer" style="background: url(${req.user.userprofile.profile_pic?req.user.userprofile.profile_pic:'img/assets/coupleindex.svg'}) top center / cover no-repeat;display: block;height: 105px;"></a></div><div class="col-md-10 col-xs-12"><h4 class="msgpageuserimg"><a href="javascript:visitProfile(${req.user.id});">${req.user.first_name} ${req.user.last_name}&nbsp;<span>(${req.user.userprofile.uid})</span></a><!--<span class="premiummem">Premium Member</span>--><span class="msgpagedate"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>&nbsp;${new Date(req.created_on).toLocaleDateString()}&emsp;</span></h4><a href="javascript:void(0)" onclick="viewMobile('${req.user.userprofile.profile_pic?req.user.userprofile.profile_pic:'img/assets/coupleindex.svg'}','${req.user.userdetails.mobile_number}','${req.user.userprofile.uid}','${req.user.first_name} ${req.user.last_name}');" class="msgpageanchor"><i class="fa fa-mobile viewnumbericon" aria-hidden="true"></i>&emsp;View Contact Details</a><p class="f13"><span class="">${new Date().getFullYear()-new Date(dob.split('-')[0],dob.split('-')[1]-1,dob.split('-')[2]).getFullYear()} years old </span>, <span class=""> ${req.user.userdetails.religion.name} </span>, <span class=""> ${req.user.userdetails.mother_tongue.name} </span></p><p><a class="btn btn--primary prl20 ml3 nobtn" href="javascript:void(0);" onclick="blockMessage(this,${req.user.id});"><span class="btn__text">Block this profile</span></a>${buttons}</p></div><hr class="messageshr"></div><hr>`;
            }   
        }
        else if (id>=5 && id<=7) {
          
          if (data.results.length<=0) {
              appendHTML+=`<div class="row"><p><center>Currently there is no communication in this folder.</center></p></div>`;
          }
          for (var req of data.results) {
                var dob=req.actor.userdetails.dob;
                appendHTML+=`<div class="row" data-request-id="${req.id}"><div class="col-md-2 col-xs-12" imgsrc="${req.user.userprofile.profile_pic?req.user.userprofile.profile_pic:'img/assets/coupleindex.svg'}"><a class="pointer" style="background: url(${req.user.userprofile.profile_pic?req.user.userprofile.profile_pic:'img/assets/coupleindex.svg'}) top center / cover no-repeat;display: block;height: 105px;"></a></div><div class="col-md-10 col-xs-12"><h4 class="msgpageuserimg"><a href="javascript:visitProfile(${req.actor.id});">${req.actor.first_name} ${req.actor.last_name}&nbsp;<span>(${req.actor.userprofile.uid})</span></a><!--<span class="premiummem">Premium Member</span>--><span class="msgpagedate"><i class="fa fa-calendar-check-o" aria-hidden="true"></i>&nbsp;${new Date(req.created_on).toLocaleDateString()}&emsp;</span></h4><a href="javascript:void(0)" href="javascript:void(0)" onclick="viewMobile('${req.actor.userprofile.profile_pic?req.actor.userprofile.profile_pic:'img/assets/coupleindex.svg'}','${req.actor.userdetails.mobile_number}','${req.actor.userprofile.uid}','${req.actor.first_name} ${req.actor.last_name}');"  class="msgpageanchor"><i class="fa fa-mobile viewnumbericon" aria-hidden="true"></i>&emsp;View Mobile Number / Send SMS</a><p class="f13"><span class="">${new Date().getFullYear()-new Date(dob.split('-')[0],dob.split('-')[1]-1,dob.split('-')[2]).getFullYear()} years old </span>, <span class=""> ${req.actor.userdetails.religion.name} </span>, <span class=""> ${req.actor.userdetails.mother_tongue.name} </span></p></div><hr class="messageshr"></div><hr>`;
          }

        }

        if (data.next_url) {
          nextURL=data.next_url;
          $('#loadmore-box').show();
        }
        else {
          nextURL='';
          $('#loadmore-box').hide();
          $(`#${sections[id]}`).empty();
        }

        $(`#${sections[id]}`).append(appendHTML);

    },
    error: function (edata) {
       // console.log(edata);
       $('body').prepend($('<h3 class="remove-me" style="background:#ff4081;">oops!! Something went wrong!!</h3>').hide().fadeIn());
       setTimeout(function () {
            $('.remove-me').fadeOut();
       }, 5000);
    }
  });

}



function loadMore () {
  getMessage(currentViewId,1);
}



function acceptReqSS(me,id){
    $.ajax({
        url: `${domain}user/change-status/${id}/2/`,
        type: 'get',
        headers: {
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function (data) {
            console.log(data);
            $(me).closest('.row').slideUp(250,function () {
              listRequests();
            });
            getMessage(1);

        },
        error: function (edata) {
           console.log(edata);
           $('body').prepend('<h3 class="remove-me" style="background:#ff4081;">oops!! Something went wrong!!</h3>');
           setTimeout(function () { $('.remove-me').fadeOut(function () { $(this).remove();});}, 3000);
           
        }
    });
    console.log(id);
}

function denyReqSS(me,id){
    console.log(id);
    $.ajax({
        url: `${domain}user/change-status/${id}/3/`,
        type: 'get',
        headers: {
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function (data) {
          $(me).closest('.row').slideUp(250,function () {
            listRequests();
          });
          getMessage(1);
          

        },
        error: function (edata) {
           console.log(edata);
           $('body').prepend('<h3 class="remove-me" style="background:#ff4081;">oops!! Something went wrong!!</h3>');
           setTimeout(function () { $('.remove-me').fadeOut(function () { $(this).remove();});}, 3000);
        }
    });

}

var trashid=undefined;
var trashref=undefined;

function trashMessage(me,id){
    trashid=id;
    trashref=me;
    $('#deletemodal').modal('show');

    $('#deletemodal h4.msgpageuserimg').html($(me).closest('.row').find('.msgpageuserimg>a').html());
    $('#deletemodal p.f13').html(`${$(me).closest('.row').find('.f13').text()}`);
    $('#deletemodal img.modalimg').attr('src', $(me).closest('.row').find('.col-md-2').attr('imgsrc'));
    // var imagesrc=;
    console.log($(me).closest('.row').find('.f13').text());
}

function blockMessage(me,id){
    trashid=id;
    trashref=me;
    $('#blockprofilemodal').modal('show');

    $('#blockprofilemodal h4.msgpageuserimg').html($(me).closest('.row').find('.msgpageuserimg>a').html());
    $('#blockprofilemodal p.f13').html(`${$(me).closest('.row').find('.f13').text()}`);
    $('#blockprofilemodal img.modalimg').attr('src', $(me).closest('.row').find('.col-md-2').attr('imgsrc'));
    // var imagesrc=;
    console.log($(me).closest('.row').find('.f13').text());
}


function trash () {
  var me=trashref;
  $.ajax({
        url: `${domain}user/change-status/${trashid}/4/`,
        type: 'get',
        headers: {
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function (data) {
          
          $('#deletemodal').modal('hide');

          
          $('#snackbarsuccs').text('A profile is blocked');
          showsuccesstoast();

          $(me).closest('.row').slideUp(250,function () {
            listRequests();
          });
          
          // getMessage(1);
        },
        error: function (edata) {
           console.log(edata);
           $('body').prepend('<h3 class="remove-me" style="background:#ff4081;">oops!! Something went wrong!!</h3>');
           setTimeout(function () { $('.remove-me').fadeOut(function () { $(this).remove();});}, 3000);
        }
    });
}

function blockIT () {
  var me=trashref;
  $.ajax({
        url: `${domain}user/block-profiles/`,
        type: 'post',
        data: JSON.stringify({"actor":trashid}),
        headers: {
            "Authorization": "Token " + localStorage.wutkn,
            "content-type": "application/json"
        },
        success: function (data) {
          
          $('#blockprofilemodal').modal('hide');

          
          $('#snackbarsuccs').text('A profile is blocked');
          showsuccesstoast();

          $(me).closest('.row').slideUp(250,function () {
            listRequests();
          });
          
          // getMessage(1);
        },
        error: function (edata) {
           console.log(edata);
        }
    });
}


function viewMobile(image,mobile,uid,uname){
  $('#modal-body').html(`<div class="row"><div class="col-md-12"><div class="row"><div class="col-md-2"><img src="${image}" class="img-responsive modalimg" style="height: 70px;width: 100%;"></div><div class="col-md-10"><h4 class="msgpageuserimg">${uname}&nbsp;<span>(${uid})</span><span class="premiummem">${mobile}</span></h4></div></div></div></div>`);
  $('#myModal').modal('show');
}


function visitProfile (id) {
  localStorage.userid_toseeprof=id;
  // window.location.href="profile.html";
   window.open("profile.html", '_blank');
}
function getMID() {

    $.ajax({
        url: `${domain}user/retrieve/keywords/`,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: (data)=>{
          var options=``;
          $('#mid-value').empty();
          options+=`<option ></option>`;

          for (var d of data) {
            options+=`<option value='${d.id}'>${d.userprofile__uid}</option>`;
          }
          $('#mid-value').append(options);
          $('#mid-value').select2({placeholder: "Enter Matrimony ID..."});
        },
        error: function(data) {
        }
    });

}

function getData(me) {
  // console.log($(me).val());
  visitProfile($(me).val())
}