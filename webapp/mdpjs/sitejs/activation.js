$(function() {

    $(".loadericon").hide();

    function getQueryStrings() {
        var assoc = {};
        var decode = function(s) {
            return decodeURIComponent(s.replace(/\+/g, " "));
        };
        var queryString = location.search.substring(1);
        var keyValues = queryString.split('&');
        for (var i in keyValues) {
            var key = keyValues[i].split('=');
            if (key.length > 1) {
                assoc[decode(key[0])] = decode(key[1]);
            }
        }
        return assoc;
    }

    var qs = getQueryStrings();
    var uid = qs["id"];
    var token = qs["token"];
    sessionStorage.tokenactivate = token;
    sessionStorage.useridactivate = uid;

});

//activatenow fn starts here
function activatenow() {

    $(".loadericon").show();
    $(".activationBtn").attr("disabled", true);

    var uniqueclient = Math.random() * 10000000000000000;

    var postData = JSON.stringify({
        "uid": sessionStorage.useridactivate,
        "token": sessionStorage.tokenactivate,
        "client": uniqueclient
    });

    $.ajax({
        url: accountactivation_api,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {

            $(".loadericon").hide();
            $(".activationBtn").attr("disabled", false);
        },
        error: function(data) {

            $(".loadericon").hide();
            $(".activationBtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {
        
        localStorage.useridactivate = sessionStorage.useridactivate;
        localStorage.useracccred = JSON.stringify(dataJson);
        localStorage.wufname = dataJson.first_name;
        localStorage.wulname = dataJson.last_name;
        localStorage.wutkn = dataJson.token;
        localStorage.userregphoneno = dataJson.username;
        localStorage.usermailid = dataJson.email;
        localStorage.matrimonyid = dataJson.userprofile.uid;
        localStorage.wuupldedpropic = dataJson.userprofile.profile_pic;

        localStorage.gender = dataJson.userdetails.gender;
        localStorage.profilecratedforname = dataJson.userdetails.relationship.name;
        localStorage.profilecratedforid = dataJson.userdetails.relationship.id;
        // localStorage.wufbpropic = dataJson.user_info.userprofile.picture;

        $("#snackbarsuccs").text("Your Account Has Been Activated Successfully!Please wait...");
        showsuccesstoast();

        setTimeout(function() {
            window.location.replace("register-phase1.html");
        }, 3000);
    });

} //activatenow fn ends here
