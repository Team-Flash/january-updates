var deleteduserdynimg = [];
var aboutthem = [];
var aboutthierhobbies = [];
$(function() {

    $(".loadericon,.emptyphotos,.loadericonconfirm,.enterotpsection,.loadericonphnochange,.loadericonverifyotp,.loadericonresendcode,.resendtimeshow,.helpmewritethis").hide();

    // geteditp1datafromlocal(1); //this fn can happen 

    if (localStorage.editphase1data == undefined) {
        geteditphase1data();
    } else {
        var currentdate = new Date().getDate();
        if (currentdate != localStorage.datastoreddate_ep1) {
            localStorage.removeItem("editphase1data");
            geteditphase1data();
        } else {
            geteditp1datafromlocal(1); //this fn can happen 
        }
    }

    usergivendata(); //user given data call fn 

});

//get edit phase 1 data from server
function geteditphase1data() {

    $.ajax({
        url: editprofilep1_dd_api,
        type: 'get',
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {

            localStorage.editphase1data = JSON.stringify(data);
            localStorage.datastoreddate_ep1 = new Date().getDate();

            geteditp1datafromlocal(0); //this fn is common for all
        },
        error: function(edata) {
            console.log("error occured in get edit phase 1 dd and others data");
        }
    });

} //get register phase 1 data from server

//get edit phaseone details from local
function geteditp1datafromlocal(type) {

    var data = JSON.parse(localStorage.editphase1data);

    //profile for dyn append
    $('#editp1_proffor').empty();
    for (var i = 0; i < data.relationship.length; i++) {
        $('#editp1_proffor').append("<option value='" + data.relationship[i].id + "'>" + data.relationship[i].name + "</option>");
    }
}

$(".editp1_useraboutme").keyup(function() {
    $(".totalcharacters").text($(".editp1_useraboutme").val().length);
});

//Help me write this phase starts here

$(".helpmemodal").click(function() {
    aboutthem = [];
    aboutthierhobbies = [];
    aboutthem.push($(".aboutthemlist .active").text());
    aboutthierhobbies.push($(".aboutthierhobbieslist .active").text());
});

$(".aboutthem").click(function() {

    $(this).find(".descr").toggleClass("active");
    if ($(".aboutthemlist .active").length < 1) {
        $(this).find(".descr").toggleClass("active");
    }
    if ($(".aboutthemlist .active").length > 5) {
        $(this).find(".descr").removeClass("active");
    }

    aboutthem = [];
    for (var i = 0; i < $(".aboutthemlist .active").length; i++) {
        aboutthem.push($(".aboutthemlist .active").eq(i).text());
    }

    // aboutthierhobbies
    if (localStorage.profilecratedforname != "Myself") {
        var genderctgry = (localStorage.gender == "Male") ? "His" : "Her";
        var texttoshow = "My " + localStorage.profilecratedforname + " is very " + aboutthem + " and " + genderctgry + " Hobbies and Interests are " + aboutthierhobbies + " . ";
    } else {
        var texttoshow = "People around me used to tell that am very " + aboutthem + " and my Hobbies and Interests are " + aboutthierhobbies + " . ";
    }

    $("#aboutuscreation").val(texttoshow);
    $(".editp1_useraboutme").val(texttoshow);
    $(".totalcharacters").text(texttoshow.length);

});

$(".aboutthierhobbies").click(function() {

    $(this).find(".descr").toggleClass("active");
    if ($(".aboutthierhobbieslist .active").length < 1) {
        $(this).find(".descr").toggleClass("active");
    }
    if ($(".aboutthierhobbieslist .active").length > 5) {
        $(this).find(".descr").removeClass("active");
    }
    aboutthierhobbies = [];
    for (var i = 0; i < $(".aboutthierhobbieslist .active").length; i++) {
        aboutthierhobbies.push($(".aboutthierhobbieslist .active").eq(i).text());
    }

    // aboutthierhobbies
    if (localStorage.profilecratedforname != "Myself") {
        var genderctgry = (localStorage.gender == "Male") ? "His" : "Her";
        var texttoshow = "My " + localStorage.profilecratedforname + " is very " + aboutthem + " and " + genderctgry + " hobbies and interests are " + aboutthierhobbies + " . ";
    } else {
        var texttoshow = "People around me used to tell that am very " + aboutthem + " and my hobbies and interests are " + aboutthierhobbies + " . ";
    }

    $("#aboutuscreation").val(texttoshow);
    $(".editp1_useraboutme").val(texttoshow);
    $(".totalcharacters").text(texttoshow.length);

});


//get usergiven data details from local
function usergivendata() {



    var data = JSON.parse(localStorage.myprofiledata);

    if (data.user_photos.length == 0) {
        $(".emptyphotos").show();
        $(".dynphotoscoloumn").hide();
    } else {
        $(".emptyphotos").hide();
        $(".dynphotoscoloumn").show();
        $(".dynamicuploadedphotos").empty();

        for (var i = 0; i < data.user_photos.length; i++) {
            $(".dynamicuploadedphotos").append("<div class='col-xl-2 col-lg-2 col-md-2 col-sm-3 col-xs-6 mt10 paddlr3 dynimg" + data.user_photos[i].id + " dynuserimages'> <div class='hovereffectclose'> <div class='product-cat'> <a class='pointer' onclick='deletethisimg(" + data.user_photos[i].id + ")'> <span class='roll'></span> <img class='img-responsive responsiveimg extraa' src='" + data.user_photos[i].image + "' alt='' style='height:150px;'></a> </div></div></div>");

        }

    } //else cond ends here

    $(".editp1_fname").val(data.first_name);
    $(".editp1_lname").val(data.last_name);
    $(".editp1_useremailid").val(data.email);
    $(".editp1_userphoeno").val(localStorage.userregphoneno);

    if (data.userdetails.gender == "Male") {
        $('[name="editp1_usergender"]').eq(0).prop("checked", true);
    } else {
        $('[name="editp1_usergender"]').eq(1).prop("checked", true);
    }
    $("#dob").val(data.userdetails.dob);
    $('#editp1_proffor option[value="' + data.userdetails.relationship.id + '"]').attr("selected", true);

    if (data.about_me == null) {
        $(".helpmewritethis").show();
        var aboutme = "";
    } else {
        $(".helpmewritethis").hide();
        var aboutme = data.about_me.about;
    }

    $(".editp1_useraboutme").val(aboutme);

    $(".totalcharacters").text(aboutme.length);

    if (data.photo_privacy != null) {
        if (data.photo_privacy.privacy_status == 1) {
            $('input:radio[name="ep1_photoprivacy"]').eq(0).prop("checked", true);
        } else {
            $('input:radio[name="ep1_photoprivacy"]').eq(1).prop("checked", true);
        }
    }

} //get phaseone details from local

//userprofile pics fn starts here
function userprofilepicsfn(e) {

    var files = e.files;
    $(".dynamicprofimages").empty();
    if (files.length != 0) {
        for (var i = 0; i <= files.length; i++) {
            var file = files[i];
            var reader = new FileReader();

            reader.onload = (function(file) {
                return function(event) {
                    $(".dynamicprofimages").append("<div class='col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12 mt10 paddlr3'> <div class='hovereffectclose'> <img class='img-responsive responsiveimg profileselectedimage profilepicupimg' src='" + event.target.result + "' alt='' style='height:150px;'> <div class='overlay'> <a class='info' href='#'><i class='font-icon-close-2'></i></a> </div></div></div>")
                };
            })(file);

            reader.readAsDataURL(file);
        } // end for;
    } else {
        $(".dynamicprofimages").append("<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mt10 paddlr3'> <div class='hovereffectclose'> <img class='img-responsive responsiveimg profileselectedimage profilepicupimg' src='img/assets/noimgbg.jpg' alt='' style='height:150px;'> <div class='overlay'> <a class='info' href='#'><i class='font-icon-close-2'></i></a> </div></div></div>")
    }

};

//border color changing
$(".editphase1form").click(function() {
    $(".editphase1form input,.editphase1form select,.editphase1form textarea").removeClass("iserror");
});

//update fn phase one starts here
function editp1_fn() {

    var postData = new FormData();
    var userprofimage = $("#userprofilepics")[0].files;

    if ($(".dynuserimages").length == 0 && userprofimage.length == 0) {
        $("#snackbarerror").text("Profile Photo is required");
        // $(".editp1_fname").addClass("iserror");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('[name="ep1_photoprivacy"]:checked').val() == undefined) {
        $("#snackbarerror").text("Photo Privacy is required");
        $("html, body").animate({ scrollTop: 350 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($(".editp1_fname").val() == '') {
        $("#snackbarerror").text("First Name is required");
        $(".editp1_fname").addClass("iserror");
        // $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($(".editp1_lname").val() == '') {
        $("#snackbarerror").text("Last Name is required");
        $(".editp1_lname").addClass("iserror");
        // $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#dob").val() == '') {
        $("#snackbarerror").text("DOB is required");
        $("#dob").addClass("iserror");
        // $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($(".editp1_useraboutme").val() == '') {
        $("#snackbarerror").text("About Me is required");
        $(".editp1_useraboutme").addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $('.loadericon').show();
    $(".updateBtn").attr("disabled", true);

    postData.append("first_name", $(".editp1_fname").val());
    postData.append("last_name", $(".editp1_lname").val());
    postData.append("dob", $("#dob").val());
    postData.append("gender", $('[name="editp1_usergender"]:checked').val());
    postData.append("relationship", $("#editp1_proffor").val());
    postData.append("about", $(".editp1_useraboutme").val());
    postData.append("privacy_status", $('[name="ep1_photoprivacy"]:checked').val());

    if (userprofimage.length != 0) {
        for (var i = 0; i < userprofimage.length; i++) {
            postData.append("photos", userprofimage[i]);
        }
    }

    $.ajax({
        url: sendeditp1data_api,
        type: 'PUT',
        data: postData,
        crossDomain: true,
        contentType: false,
        processData: false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        success: function(data) {
            $('.loadericon').hide();
            $(".updateBtn").attr("disabled", false);
        },
        error: function(data) {

            $('.loadericon').hide();
            $(".updateBtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {
        localStorage.wuupldedpropic = dataJson.userprofile.profile_pic;
        window.location.href = "myprofile.html";
    });

} //update fn phase one starts here

//delete this image fn starts here
function deletethisimg(imageid) {
    deleteduserdynimg.push(imageid);
    sessionStorage.delete_dynimg_id = imageid;
    if($(".dynuserimages").length > 1){
        $("#blockprofilemodal").modal("toggle");
    }else{
         $("#snackbarerror").text("you can't delete this image.Because atleast one image should be required");
        showerrtoast();
    }
} //delete this image fn ends here

//delete photo fn starts here
function deletephoto() {

    $('.loadericonconfirm').show();
    $(".photodeleteBtn").attr("disabled", true);

    var postData = JSON.stringify({
        "photos": deleteduserdynimg
    });

    $.ajax({
        url: deleteprofpic_api,
        type: 'DELETE',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            deleteduserdynimg = [];
            $('.loadericonconfirm').hide();
            $(".photodeleteBtn").attr("disabled", false);

        },
        error: function(data) {

            $('.loadericonconfirm').hide();
            $(".photodeleteBtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {

        $(".dynimg" + sessionStorage.delete_dynimg_id).remove();
        $("#snackbarsuccs").text("Photo Deleted Successfully!");
        showsuccesstoast();

        if ($(".dynuserimages").length == 0) {
            $(".emptyphotos").show();
            $(".dynphotoscoloumn").hide();
        }
        $(".picmodal").click();

        getuserprofiledata();

    }); //done fn ends here
} //delete photo fn ends here

//getuserprofiledata fn starts heree
function getuserprofiledata() {

    $.ajax({
        url: myprofile_api + localStorage.useridactivate + '/',
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": 'Token ' + localStorage.wutkn + ''
        },
        success: function(data) {

            localStorage.myprofiledata = JSON.stringify(data);

        },
        error: function(edata) {
            console.log("error occured in my profile data");
        }
    });

}

// =====================================================================================================

//change my mobile number fn starts here
function changemymobileno() {

    if ($('#editedmobileno').val().trim() == '') {
        $("#snackbarerror").text("Mobile No is required");
        $('#editedmobileno').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $(".updateBtn").attr("disabled", true);
    $('.loadericonphnochange').show();

    var postData = JSON.stringify({
        "username": $("#editedmobileno").val()
    });

    $.ajax({
        url: editmobileno_api,
        type: 'PUT',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": 'Token ' + localStorage.wutkn + ''
        },
        success: function(data) {
            $(".updateBtn").attr("disabled", false);
            $('.loadericonphnochange').hide();
        },
        error: function(data) {

            $(".updateBtn").attr("disabled", false);
            $('.loadericonphnochange').hide();

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {

        $("#snackbarsuccs").text("Your mobile number has been changed successfully!");
        showsuccesstoast();
        localStorage.userregphoneno = $("#editedmobileno").val();
        $(".userchangedno").text(localStorage.userregphoneno);

        $(".updatenumbersection").slideToggle();
        $(".enterotpsection").slideToggle();
        $("#editedmobileno").val("");

        sendOTP();

    });

} //change my mobile number fn starts here

$("#otpcode,#editedmobileno").click(function() {
    $("input").removeClass("iserror");
});

//send otp fn starts here
function sendOTP() {

    $.ajax({
        url: sendotptouser_api,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": 'Token ' + localStorage.wutkn + ''
        },
        success: function(data) {

            $("#snackbarsuccs").text("OTP has been sent to your register phone no " + localStorage.userregphoneno);
            showsuccesstoast();

        },
        error: function(data) {
            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    });
} //send otp fn starts here

//verify otp fn starts here
function verifyotp() {

    if ($('#otpcode').val().trim() == '') {
        $("#snackbarerror").text("OTP code is required");
        $('#otpcode').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $(".otpverifyBtn").attr("disabled", true);
    $('.loadericonverifyotp').show();

    var postData = JSON.stringify({
        "otp": $("#otpcode").val()
    });

    $.ajax({
        url: otpverification_api,
        type: 'POST',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": 'Token ' + localStorage.wutkn + ''
        },
        success: function(data) {
            $(".otpverifyBtn").attr("disabled", false);
            $('.loadericonverifyotp').hide();
        },
        error: function(data) {

            $(".otpverifyBtn").attr("disabled", false);
            $('.loadericonverifyotp').hide();

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {
        $(".editp1_userphoeno").val(localStorage.userregphoneno);
        $(".clsephnocheckmodal").click();
        $(".updatenumbersection").slideToggle();
        $(".enterotpsection").slideToggle();
        $("#otpcode").val("");
        $("#snackbarsuccs").text("Your phone no has been verified successfully");
        showsuccesstoast();
    });

} //verify otp fn starts here

//resend opt fn starts here
function resendotp() {

    $('.loadericonresendcode').show();

    $.ajax({
        url: sendotptouser_api,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": 'Token ' + localStorage.wutkn + ''
        },
        success: function(data) {
            $('.loadericonresendcode').hide();
            $("#snackbarsuccs").text("OTP has been sent to your register phone no " + localStorage.userregphoneno);
            showsuccesstoast();
            //timer fn starts here
            $(".resentotpatag").hide();
            $(".resendtimeshow").show();
            $(".timershow").text("05:00");
            var interval = setInterval(function() {
                var timer = $('.timershow').html();
                timer = timer.split(':');
                var minutes = parseInt(timer[0], 10);
                var seconds = parseInt(timer[1], 10);
                seconds -= 1;
                if (minutes < 0) return clearInterval(interval);
                if (minutes < 10 && minutes.length != 2) minutes = '0' + minutes;
                if (seconds < 0 && minutes != 0) {
                    minutes -= 1;
                    seconds = 59;
                } else if (seconds < 10 && length.seconds != 2) seconds = '0' + seconds;
                $('.timershow').html(minutes + ':' + seconds);

                if (minutes == 0 && seconds == 0) {
                    clearInterval(interval);
                    gotochecktime();
                }

            }, 1000);

        },
        error: function(data) {
            $('.loadericonresendcode').hide();
            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    });

} //resend opt fn ends here

//gotochecktime fn starts here
function gotocheck() {
    $(".resendtimeshow").hide();
    $(".resentotpatag").show();
}