$(function() {

    if (localStorage.interestedProfileRedirect == 1) {
        localStorage.interestedProfileRedirect = 0;
        changeFilter($('.filter-list').eq(4), 5, 0);
    } else {
        changeFilter($('.filter-list')[0], 1, 0);
    }

});

var filterData = { "1": "People Who viewed my Profile", "2": "People Who Shortlisted me", "7": "People who viewed my Number", "6": "Mobile Numbers viewed by me", "3": "My Ignored Profiles", "4": "My Blocked Profiles", "5": "My Shortlisted Profiles", "8": "My Liked Profiles", "9": "People Who liked my profile" };

function changeFilter(me, id, type) {

    $('#profiles').empty();
    $("#loaderspinner").show();
    sessionStorage.id = id;

    if (type == 0) {
        $('#profile-heading').html(`${filterData[id]}&nbsp;<span class="pinkcolor" id="profile-filter-count"></span>`);
        $('.filter-list').removeClass('activeunderline');
        $(me).addClass('activeunderline');
        var url = `${domain}user/list/matrimony/?type=${id}`;
    } else {
        var url = `${domain}user/list/matrimony/?type=${id}&page=${sessionStorage.gotopage_no}`;
    }

    $.ajax({
        url: url,
        type: 'get',
        headers: {
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            if (type == 0) {
                pagination(data.count);
            }
            if (data.results.length == 0) {
                $('#profiles').empty().append(`<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><center><img src="img/assets/nodata.svg" style="width:40%"></center></div>`);
            }
            // console.log(data);
            var appendHTML = '';
            for (var pro of data.results) {
                appendHTML += `<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6"><div class="card card-2 text-center"><div class="card__top"><a href="javascript:visitProfile(${pro.id})" target="_blank" style="background: url(${pro.userprofile.profile_pic?pro.userprofile.profile_pic:'img/assets/coupleindex.svg'}) top center / cover no-repeat;display: block;height: 140px;"></a></div><div class="card__body p10"><p class="username dynname${pro.id}">${pro.first_name} ${pro.last_name}</p><p class="livesin">Lives in <span class="spanlivesin">${pro.user_address?pro.user_address.city.name:'- -'}</span></p></div><div class="card__bottom text-center ptb3"><div class="card__action"><a href="javascript:void(0)" onclick="likeprofile(this,${pro.id});"><i class="fa fa-heart hoverpink accepticon ${pro.is_liked?'shortlisted':''}" aria-hidden="true"></i></a></div><div class="card__action"><a href="javascript:void(0)" onclick="shortlistProfile(this,${pro.id});"><i class="fa fa-star hoverpink accepticon ${pro.is_shortlisted?'shortlisted':''}" aria-hidden="true"></i></a></div><div class="card__action"><a href="javascript:void(0)" onclick="ignoreProfile(this,${pro.id});"><i class="fa fa-thumbs-down hoverblack rejecticon ${pro.is_ignored?'ignored':''}" aria-hidden="true"></i></a></div></div></div></div>`;
            }
            $('#profiles').append(appendHTML);
            $('#profile-filter-count').html(`(${data.count})`);
            $("#loaderspinner").hide();
            $(".matrimonyheading").click();
        },
        error: function(edata) {
            $('#profiles').empty().append(`<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><center><img src="img/assets/nodata.svg" style="width:40%"></center></div>`);
            $("#loaderspinner").hide();
            $(".matrimonyheading").click();
             $(".dynpagination").empty();
        }
    });
}

//shortlist profile fn starts here
function shortlistProfile(me, id) {
    var postData = { "actor": id };
    $.ajax({
        url: `${domain}user/shortlist-profile/`,
        type: 'post',
        headers: {
            "content-type": "application/json",
            "Authorization": "Token " + localStorage.wutkn
        },
        data: JSON.stringify(postData),
        success: function(data) {
            $(me).find('i').toggleClass('shortlisted');
            $("#snackbarsuccs").text("Your request has been send to "+ $(".dynname"+id).text());
            showsuccesstoast();
        },
        error: function(edata) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
} //shortlist profile fn ends here

//ignore profile fn starts here
function ignoreProfile(me, id) {
    var postData = { "actor": id };
    $.ajax({
        url: `${domain}user/ignore-profile/`,
        type: 'post',
        headers: {
            "content-type": "application/json",
            "Authorization": "Token " + localStorage.wutkn
        },
        data: JSON.stringify(postData),
        success: function(data) {
            $(me).find('i').toggleClass('ignored');
            $("#snackbarsuccs").text("Your request has been send to "+ $(".dynname"+id).text());
            showsuccesstoast();
        },
        error: function(edata) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
} //ignore profile fn ends here

//like other people profile fn starts here
function likeprofile(me, id) {
    var postData = { "actor": id };
    $.ajax({
        url: `${domain}user/like-profiles/`,
        type: 'post',
        headers: {
            "content-type": "application/json",
            "Authorization": "Token " + localStorage.wutkn
        },
        data: JSON.stringify(postData),
        success: function(data) {
            $(me).find('i').toggleClass('ignored');
            $("#snackbarsuccs").text("Your request has been send to "+ $(".dynname"+id).text());
            showsuccesstoast();
        },
        error: function(edata) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
} //like other people profile fn ends here

//pagination fn starts here
function pagination(count) {
    $(".dynpagination").empty().show().append(`<li class="mright10"><a onclick="previouspage()" class="cptr font12 paginationtext">« Previous</a></li>`);
    // var totalpagescount = Math.round(count / 3);

    if ((count / 12) % 1 == 0) { //should change here only for how many box should come // count
        var totalpagescount = (count / 12);
    } else {
        var totalpagescount = parseInt((count / 12), 10) + 1;
    }

    sessionStorage.totalpagescount = totalpagescount;
    for (var i = 0; i < totalpagescount; i++) {
        $(".dynpagination").append(`<li class="pageli mright10 pageli${i+1} ${(i == 0 ? "active" : "")}" pageno="${i+1}"> <a class="cptr font12 paginationtext ${(i == 0 ? "pageactive" : "")}" onclick="gotothispage(${i+1})">${i+1}</a> </li>`);
        if (i == (totalpagescount - 1)) {
            $(".dynpagination").append(`<li class="mright10"><a onclick="nextpage()" class="cptr font12 paginationtext"> Next » </a></li>`);
        }
    }
    $(".pageli").hide();
    sessionStorage.showcount = 5;
    for (var i = 1; i <= 5; i++) {
        $(".pageli" + i).show();
    }
}

function gotothispage(pageno) {
    sessionStorage.whichclick = 2;
    $(".pageli").removeClass("active");
    $(".pageli" + pageno).addClass("active");

    $(".pageli a").removeClass("pageactive");
    $(".pageli" + pageno + " a").addClass("pageactive");
    sessionStorage.gotopage_no = pageno;
    changeFilter(undefined, sessionStorage.id, 1);
}

function nextpage() {
    sessionStorage.whichclick = 2;
    var currentpageno = parseInt($(".pageli.active").attr("pageno"));
    console.log(currentpageno);

     if ((currentpageno % 6) == 0 && (currentpageno < sessionStorage.totalpagescount)) {
            $(".pageli").hide();
            for (var i = currentpageno; i <= currentpageno + 5; i++) {
                $(".pageli" + i).show();
            }
        }

    currentpageno++;
    console.log(currentpageno);
    if (parseInt(sessionStorage.totalpagescount) >= currentpageno) {
        $(".pageli").removeClass("active");
        $(".pageli" + currentpageno).addClass("active");

        $(".pageli a").removeClass("pageactive");
        $(".pageli" + currentpageno + " a").addClass("pageactive");
        sessionStorage.gotopage_no = currentpageno;
        changeFilter(undefined, sessionStorage.id, 1);

        if ((currentpageno % 6) == 0 && (currentpageno < sessionStorage.totalpagescount)) {
            $(".pageli").hide();
            for (var i = currentpageno; i <= currentpageno + 5; i++) {
                $(".pageli" + i).show();
            }
        }
    }
}

function previouspage() {
    sessionStorage.whichclick = 2;
    var currentpageno = parseInt($(".pageli.active").attr("pageno"));
    console.log(currentpageno);

     if ((currentpageno % 6) == 0) {
            $(".pageli").hide();
            for (var i = currentpageno; i >= currentpageno - 5; i--) {
                $(".pageli" + i).show();
            }
        }

    currentpageno--;
    console.log(currentpageno);
    if (currentpageno != 0) {
        $(".pageli").removeClass("active");
        $(".pageli" + currentpageno).addClass("active");

        $(".pageli a").removeClass("pageactive");
        $(".pageli" + currentpageno + " a").addClass("pageactive");
        sessionStorage.gotopage_no = currentpageno;
        changeFilter(undefined, sessionStorage.id, 1);
       
    }
}


//visit other people profiles fn starts here
function visitProfile(id) {
    localStorage.userid_toseeprof = id;
    window.open("profile.html", '_blank');
}
