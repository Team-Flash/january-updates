var hobbiesandinterest = [];
var favouritemusic = [];
var sportsandfitness = [];
var spokenlanguages = [];
var hobbiesothersarr = [];
var favmusicothersarr = [];
var sportsothersarr = [];
var langothersarr = [];

$(function() {

    $(".loadericon").hide();

    // if (localStorage.editphase4data == undefined) {
    //     geteditphase4data();
    // } else {
    //     var currentdate = new Date().getDate();
    //     if (currentdate != localStorage.datastoreddate_ep4) {
    //         localStorage.removeItem("editphase4data");
    //         geteditphase4data();
    //     } else {
    //         geteditp4datafromlocal(1); //this fn can happen 
    //     }
    // }
    geteditphase4data();
}); //initial fn ends here for editphase4

// geteditphase4data fn starts here
function geteditphase4data() {

    $.ajax({
        url: editprofilep4_dd_api,
        type: 'get',
       
        headers: {
            "content-type": 'application/json',
            'Authorization': 'Token ' + localStorage.wutkn
        },
        success: function(data) {

            localStorage.editphase4data = JSON.stringify(data);
            localStorage.datastoreddate_ep4 = new Date().getDate();

            geteditp4datafromlocal(0); //this fn is common for all
        },
        error: function(edata) {
            console.log("error occured in get edit phase 4 dd and others data");
        }
    });

} // geteditphase4data fn ends here

// localstorage data processing for editphase4
function geteditp4datafromlocal(type) {

    var data = JSON.parse(localStorage.editphase4data);

    //hobbies dyn append
    $('.ep4_hobbies').empty();
    for (var i = 0; i < data.hobbies.length; i++) {
        $('.ep4_hobbies').append(" <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'> <label class='control control--checkbox displayinline doshacheckbox'>" + data.hobbies[i].name + "<input type='checkbox' name='ep4_hobbiesninterest' value='" + data.hobbies[i].id + "'/> <div class='control__indicator'></div></label> </div>");
    }

    //music dyn append
    $('.ep4_favmusics').empty();
    for (var i = 0; i < data.music.length; i++) {
        $('.ep4_favmusics').append("<div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'> <label class='control control--checkbox displayinline doshacheckbox'>" + data.music[i].name + "<input type='checkbox' name='ep4_favoritemusics' value='" + data.music[i].id + "'/> <div class='control__indicator'></div></label> </div>");
    }


    //sports dyn append
    $('.ep4_favsports').empty();
    for (var i = 0; i < data.sports.length; i++) {
        $('.ep4_favsports').append(" <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'> <label class='control control--checkbox displayinline doshacheckbox'>" + data.sports[i].name + "<input type='checkbox' name='ep4_sportsrfitness' value='" + data.sports[i].id + "'/> <div class='control__indicator'></div></label> </div>");
    }

    //spokelang dyn append
    $('.ep4_spokenlangs').empty();
    for (var i = 0; i < data.languages.length; i++) {
        $('.ep4_spokenlangs').append("<div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'> <label class='control control--checkbox displayinline doshacheckbox'>" + data.languages[i].name + "<input type='checkbox' name='ep4_spokenlang' value='" + data.languages[i].id + "'/> <div class='control__indicator'></div></label> </div>");
    }

    usergivendata();
}

//================ ================================================================================
//verify user data 

//user given data fn starts here
function usergivendata() {

    var data = JSON.parse(localStorage.myprofiledata);

    //hobbies and interests
    if (data.hobbies.length != 0 || data.hobbies.length != null) {
        for (var i = 0; i < data.hobbies.length; i++) {
            $('input:checkbox[name="ep4_hobbiesninterest"]').filter('[value="' + data.hobbies[i].hobby.id + '"]').attr('checked', true);
        }
    }

    //favorite music
    if (data.favourite_music.length != 0 || data.favourite_music.length != null) {
        for (var i = 0; i < data.favourite_music.length; i++) {
            $('input:checkbox[name="ep4_favoritemusics"]').filter('[value="' + data.favourite_music[i].music.id + '"]').attr('checked', true);
        }
    }

    //sports/fitnessfavorite music
    if (data.sports.length != 0 || data.sports.length != null) {
        for (var i = 0; i < data.sports.length; i++) {
            $('input:checkbox[name="ep4_sportsrfitness"]').filter('[value="' + data.sports[i].sport.id + '"]').attr('checked', true);
        }
    }

    //spokenlanguages
    if (data.spoken_languages.length != 0 || data.spoken_languages.length != null) {
        for (var i = 0; i < data.spoken_languages.length; i++) {
            $('input:checkbox[name="ep4_spokenlang"]').filter('[value="' + data.spoken_languages[i].language.id + '"]').attr('checked', true);
        }
    }

    // if (data.other_hobbies.length != 0 || data.other_hobbies.length != null) {
    //     for (var i = 0; i < data.other_hobbies.length; i++) {

    //         $(".tags0 .tagAdd0").before('<li class="addedTag0">' + data.other_hobbies[i].hobby__name + '<span class="tagRemove" onclick="$(this).parent().remove();">x</span><input type="hidden" value="' + data.other_hobbies[i].hobby__name + '" name="tags[]"></li>');

    //     }
    // }

    // if (data.other_music.length != 0 || data.other_music.length != null) {
    //     for (var i = 0; i < data.other_music.length; i++) {

    //         $(".tags0 .tagAdd0").before('<li class="addedTag0">' + data.other_music[i].music__name + '<span class="tagRemove" onclick="$(this).parent().remove();">x</span><input type="hidden" value="' + data.other_music[i].music__name + '" name="tags[]"></li>');

    //     }
    // }

    //  if (data.other_sports.length != 0 || data.other_sports.length != null) {
    //     for (var i = 0; i < data.other_sports.length; i++) {

    //         $(".tags0 .tagAdd0").before('<li class="addedTag0">' + data.other_sports[i].music__name + '<span class="tagRemove" onclick="$(this).parent().remove();">x</span><input type="hidden" value="' + data.other_sports[i].music__name + '" name="tags[]"></li>');

    //     }
    // }



}
//hobbies and interest,favorite music,sports n fitness,spoken lang

function editp4_fn() {

    if ($('input[name="ep4_hobbiesninterest"]:checked').length == 0) {
        $("#snackbarerror").text("Hobbies and interests is required");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('input[name="ep4_favoritemusics"]:checked').length == 0) {
        $("#snackbarerror").text("Favorite musics list is required");
        $("html, body").animate({ scrollTop: 1000 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('input[name="ep4_sportsrfitness"]:checked').length == 0) {
        $("#snackbarerror").text("Sports and Fitness  is required");
        $("html, body").animate({ scrollTop: 1300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('input[name="ep4_spokenlang"]:checked').length == 0) {
        $("#snackbarerror").text("Spoken languages is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $('.loadericon').show();
    $(".updateBtn").attr("disabled", true);


    for (var i = 0; i < $('input[name="ep4_hobbiesninterest"]:checked').length; i++) {
        hobbiesandinterest.push($('[name="ep4_hobbiesninterest"]:checked').eq(i).val());
    }
    for (var i = 0; i < $('input[name="ep4_favoritemusics"]:checked').length; i++) {
        favouritemusic.push($('[name="ep4_favoritemusics"]:checked').eq(i).val());
    }
    for (var i = 0; i < $('input[name="ep4_sportsrfitness"]:checked').length; i++) {
        sportsandfitness.push($('[name="ep4_sportsrfitness"]:checked').eq(i).val());
    }
    for (var i = 0; i < $('input[name="ep4_spokenlang"]:checked').length; i++) {
        spokenlanguages.push($('[name="ep4_spokenlang"]:checked').eq(i).val());
    }

    // if ($(".addedTag0").length != 0) {
    //     for (var i = 0; i < $(".addedTag0").length; i++) {
    //         hobbiesothersarr.push({ "hobby": $("#hobbiesothers li input").eq(i).val() });
    //     }
    // }
    // if ($(".addedTag1").length != 0) {
    //     for (var i = 0; i < $(".addedTag1").length; i++) {
    //         favmusicothersarr.push({ "music": $("#favmusicsothers li input").eq(i).val() });
    //     }
    // }
    // if ($(".addedTag2").length != 0) {
    //     for (var i = 0; i < $(".addedTag2").length; i++) {
    //         sportsothersarr.push({ "sport": $("#sportsandfitnessothers li input").eq(i).val() });
    //     }
    // }
    // if ($(".addedTag3").length != 0) {
    //     for (var i = 0; i < $(".addedTag3").length; i++) {
    //         langothersarr.push({ "language": $("#spkoenlangothers li input").eq(i).val() });
    //     }
    // }

     var Datatosend = {
        "hobbies": hobbiesandinterest,
        "favourite_music": favouritemusic,
        "sports": sportsandfitness,
        "spoken_languages": spokenlanguages
        // "other_hobbies": hobbiesothersarr,
        // "other_music": favmusicothersarr,
        // "other_sport": sportsothersarr,
        // "other_language": langothersarr
    };


    // if (hobbiesothersarr.length == 0) {
    //     delete Datatosend.hobbiespthers;
    // }
    // if (favmusicothersarr.length == 0) {
    //     delete Datatosend.favmusicspthers;
    // }
    // if (sportsothersarr.length == 0) {
    //     delete Datatosend.favsportspthers;
    // }
    // if (langothersarr.length == 0) {
    //     delete Datatosend.spokenlangpthers;
    // }

    var postData = JSON.stringify(Datatosend);

    $.ajax({
        url: sendeditp4data_api,
        type: 'PUT',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {

            $('.loadericon').hide();
            $(".updateBtn").attr("disabled", false);

        },
        error: function(data) {

            $('.loadericon').hide();
            $(".updateBtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {
        window.location.href = "myprofile.html";
    });

}
//func ends here
