$(function() {

    $(".loadericon").hide();

});

$(".contactusform").click(function() {
    $("input,textarea,select").removeClass("iserror");
});

//contact us fn starts here
function contactus() {

    if ($('#username').val().trim() == '') {
        $("#snackbarerror").text("First Name is required");
        $('#username').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#useremailid').val().trim() == '') {
        $("#snackbarerror").text("E-Mail ID is required");
        $('#useremailid').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    var email = $('#useremailid').val();
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= email.length) {
        $('#useremailid').addClass("iserror");
        $("#snackbarerror").text("Valid E-Mail ID is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#usermessage').val().trim() == '') {
        $("#snackbarerror").text("First Name is required");
        $('#usermessage').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $(".loadericon").show();
    $(".sendBtn").attr("disabled", true);

    var postData = JSON.stringify({
        "name": $("#username").val(),
        "email": $("#useremailid").val(),
        "message": $("#usermessage").val()
    });

    $.ajax({
        url: contactus_api,
        type: 'POST',
        data: postData,
        headers: {
            "content-type": 'application/json'
        },
        success: function(data) {

            $(".sendBtn").attr("disabled", false);
            $('.loadericon').hide();
        },
        error: function(data) {

            $(".sendBtn").attr("disabled", false);
            $('.loadericon').hide();

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {

        $("#snackbarsuccs").text("Your email Has been received.We will get back you soon!");
        showsuccesstoast();
        $("#username,#useremailid,#usermessage").val("");

    });

} //contact us fn ends here
