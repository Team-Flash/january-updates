// add advmate initial function stats here

// $(function() {

//     var showprofile = [];
//     var martialstatus = [];
//     var dontshowprofile = [];
//     $.ajax({
//         url: advmatesearch_dd_api,
//         type: 'get',
//         headers: {
//             "content-type": 'application/json',
//             "Authrization": 'Token ' + localStorage.token + ''
//         },
//         success: function(data) {

//             localStorage.regular_search_data = JSON.stringify(data);
//             localStorage.datastoredsearch_regular = new Date().getDate();

//             getadvmatesearchfromlocal(0); //this fn is common for all
//         },
//         error: function(edata) {
//             console.log("error occured in get edit phase 7 dd and others data");
//         }
//     });
//     $('#sradv_Religion').empty();
//     for (i = 0; i < data.Religion.length; i++) {
//         $("#sradv_religion").append("<option value=" + (i + 1) + ">" + data.Religion[i] + "</option>");
//     }
//     $('#sradv_mothertongue').empty();
//     for (i = 0; i < data.Language.length; i++) {
//         $("#sradv_mothertongue").append("<option value=" + (i + 1) + ">" + data.Language[i] + "</option>");
//     }
//     $('#sradv_caste').empty();
//     for (i = 0; i < data.Caste.length; i++) {
//         $("#sradv_caste").append("<option value=" + (i + 1) + ">" + data.Caste[i] + "</option>");
//     }


//     //append country,states and city
//     //country dyn append
//     for (var i = 0; i < data.country.length; i++) {
//         if (data.country[i].name == "India") {
//             countryone += "<option selected='' value='" + data.country[i].id + "'>" + data.country[i].name + "</option>";
//             localStorage.selectedcountrycode = data.country[i].id;
//         } else {
//             countryone += "<option value='" + data.country[i].id + "'>" + data.country[i].name + "</option>";
//         }
//     }
//     $('#sradv_country').empty().append(countryone);
//     getstateslist();


//     $("#sradv_country").change(function() {
//         localStorage.selectedcountrycode = $("#sradv_country").val();
//         getstateslist();
//     });

//     $("#sradv_state").change(function() {
//         localStorage.selectedstatecode = $("#sradv_state").val();
//         getcitieslist();
//     });

//     //get states list fn starts here
//     function getstateslist() {
//         $.ajax({
//             url: getregp1statesdata_api + localStorage.selectedcountrycode + '/',
//             type: 'get',
//             success: function(data) {
//                 //states dyn append
//                 for (var i = 0; i < data.states.length; i++) {
//                     if (i == 0) {
//                         statesone += "<option selected="
//                         " value='" + data.states[i].id + "'>" + data.states[i].name + "</option>";
//                         localStorage.selectedstatecode = data.states[i].id;
//                     } else {
//                         statesone += "<option value='" + data.states[i].id + "'>" + data.states[i].name + "</option>";
//                     }
//                 }
//                 $('#sradv_state').empty().append(statesone);
//                 getcitieslist();
//             },
//             error: function(edata) {
//                 console.log("error occured in get register phase 1 states dd data");
//             }
//         });
//     } //get states list fn ends here

//     //get cities list fn starts here
// function getcitieslist() {
//     $.ajax({
//         url: getregp1citiesdata_api + localStorage.selectedstatecode + '/',
//         type: 'get',
//         success: function(data) {
//             //states dyn append
//             for (var i = 0; i < data.cities.length; i++) {
//                 if (i == 0) {
//                     cityone += "<option selected="
//                     " value='" + data.cities[i].id + "'>" + data.cities[i].name + "</option>";
//                 } else {
//                     cityone += "<option value='" + data.cities[i].id + "'>" + data.cities[i].name + "</option>";
//                 }
//             }
//             $('#sradv_city').empty().append(cityone);
//             getcitieslist();
//         },
//         error: function(edata) {
//             console.log("error occured in get register phase 1 cities dd data");
//         }
//     });
// } //get cities list fn ends here




//     //education dyn
//     $('#sradv_education').empty();
//     for (var i = 0; i < data.education.length; i++) {
//         $('#sradv_education').append("<optgroup class='a sradv_educationsubmenu" + data.education[i].id + "' label='" + data.education[i].name + "'></optgroup>");
//         for (var j = 0; j < data.education[i].branch.length; j++) {
//             $(".sradv_educationsubmenu" + data.education[i].id).append("<option value='" + data.education[i].branch[j].id + "'>" + data.education[i].branch[j].name + "</option>");
//         }
//     }
//     for (i = 0; i < data.occupation.length; i++) {
//         $("#sradv_occupation").append("<option value=" + (i + 1) + ">" + data.occupation[i] + "</option>");
//     }

//     for (i = 0; i < data.intrest.length; i++) {
//         $("#sradv_bpi").append('<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"><label class="control control--checkbox displayinline doshacheckbox">' + data.intrest[i] + '<input type="checkbox" ><div class="control__indicator"></div></label></div>');
//     }


//===========================================================================================================================================
$(".age").on("click", function() {
    $("#select2-sradv_endage-container").removeClass("iserror");
});
$(".height").on("click", function() {
    $("#select2-sradv_endheight-container").removeClass("iserror");
});

$(".search_smt").on("click", function() {
    $("#select2-sradv_endage-container").removeClass("iserror");
    $("#select2-sradv_endheight-container").removeClass("iserror");
    if ($("#sradv_startage").val() == $("#sradv_endage").val()) {
        $("#snackbarerror").text("Start age and End age should not be same");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($("#sradv_endage").val() < $("#sradv_startage").val()) {
        $("#snackbarerror").text("End Age Should be higher than Start Age");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        $('#select2-sradv_endage-container').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($("#sradv_startheight").val() == $("#sradv_endheight").val()) {
        $("#snackbarerror").text("Start height and End height should not be same");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#sradv_endheight").val() < $("#sradv_startheight").val()) {
        $("#snackbarerror").text("End height Should be higher than Start height");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        $('#select2-sradv_endheight-container').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    for (var i = 0; i < $('input[name="sradv_maritalstatus"]:checked').length; i++) {
        maritalstatus.push($('[name="ep7_pmaritalstatus"]:checked').eq(i).val());
    }
    for (var i = 0; i < $('input[name="sradv_showprofile"]:checked').length; i++) {
        showprofile.push($('[name="sradv_showprofile"]:checked').eq(i).val());
    }
    for (var i = 0; i < $('input[name="sradv_dontshowprofile"]:checked').length; i++) {
        dontshowprof.push($('[name="sradv_dontshowprofile"]:checked').eq(i).val());
    }


    var Datatosend = {
        "startsage": $("#sradv_startage").val(),
        "endage": $("#sradv_endage").val(),
        "startheight": $("#sradv_startheight").val(),
        "endheight": $("#sradv_endheight").val(),
        "maritalstatus": maritalstatus,
        "religion": $("#sradv_religion").val(),
        "mothertongue": $("#sradv_mothertongue").val(),
        "caste": $("#sradv_caste").val(),
        "country": $("#sradv_country").val(),
        "state": $("#sradv_state").val(),
        "city": $("#sradv_city").val(),
        "education": $("#sradv_education").val(),
        "occupation": $("#sradv_occupation").val(),
        "showpro": showprofile,
        "dontshowpro": dontshowprofile

    }

    if (maritalstatus.length == 0) {
        delete Datatosend.maritalstatus;
    }
    if (showprofile.length == 0) {
        delete Datatosend.showprofile;
    }
    if (dontshowprofile.length == 0) {
        delete Datatosend.dontshowpro;
    }

    var postData = JSON.stringify({ Datatosend });

    $.ajax({
        url: searchadvmate_api,
        type: 'put',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authrization": 'Token ' + localStorage.token + ''
        },
        success: function(data) {

            $('.loadericon').hide();
            $(".updateBtn").attr("disabled", false);
        },
        error: function(edata) {
            console.log("error occured in loading data for the search");
        }
    }).done(function(dataJson) {
        sessionStorage.searchresults_regdata = JSON.stringify(dataJson);
        window.location.href = "search-results.html";
    });
});
// });
