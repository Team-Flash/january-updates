//fb login starts here

(function(thisdocument, scriptelement, id) {
    var js, fjs = thisdocument.getElementsByTagName(scriptelement)[0];
    if (thisdocument.getElementById(id)) return;

    js = thisdocument.createElement(scriptelement);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js"; //you can use 
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

window.fbAsyncInit = function() {
    FB.init({
        appId: '1233023933493853', //Your APP ID
        cookie: true, // enable cookies to allow the server to access 
        xfbml: true, // parse social plugins on this page
        version: 'v2.8' // use version 2.1
    });

    // These three cases are handled in the callback function.
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });

};

// This is called with the results from from FB.getLoginStatus().
function statusChangeCallback(response) {
    if (response.status === 'connected') {
        // Logged into your app and Facebook.
        console.log(response);
        // _dologinfb();
    } else if (response.status === 'not_authorized') {
        // The person is logged into Facebook, but not your app.
        document.getElementById('status').innerHTML = 'Please log ' +
            'into this app.';
    }
}

function _login() {
    FB.login(function(response) {
        // handle the response
        if (response.status === 'connected') {
            localStorage.fbat = response.authResponse.accessToken;
            localStorage.fbuid = response.authResponse.userID;
            _dologinfb();
        }
    }, {
        scope: 'public_profile,email,user_birthday'
    });
}

function _signup() {
    FB.login(function(response) {
        // handle the response
        if (response.status === 'connected') {
            localStorage.fbat = response.authResponse.accessToken;
            localStorage.fbuid = response.authResponse.userID;
            _dosignupfb();
        }
    }, {
        scope: 'public_profile,email,user_birthday'
    });
}

//fb login ends here


// ================================================================================================================

//google login starts here
function googleauthfn(type) {
    localStorage.googleauthtype = type;
    var myParams = {
        'clientid': '649166938188-mcfigbbgd0j0clvjlk3cn9e8rbmstjgm.apps.googleusercontent.com',
        'cookiepolicy': 'single_host_origin',
        'callback': 'loginCallback',
        'approvalprompt': 'force',
        'scope': 'https://www.googleapis.com/auth/plus.login https://www.googleapis.com/auth/plus.profile.emails.read https://www.googleapis.com/auth/userinfo.profile https://www.googleapis.com/auth/plus.me'
    };
    gapi.auth.signIn(myParams);
}

function loginCallback(result) {
    localStorage.googleat = result.id_token;
    // onSignIn();
    if (localStorage.googleauthtype == 0 && result.id_token != undefined) {
        _dologingoogle();
    } else {
        _dosignupgoogle();
    }

}


function onLoadCallback() {
    gapi.client.setApiKey('');
    gapi.client.load('plus', 'v1', function() {});
}
(function() {
    var po = document.createElement('script');
    po.type = 'text/javascript';
    po.async = true;
    po.src = 'https://apis.google.com/js/client.js?onload=onLoadCallback';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(po, s);
})();

//google login ends here
