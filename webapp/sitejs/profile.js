$(function() {

    $(".secondsetimages,.imgprevicon,.imgnexticon,.showemailid,.showphoneno").hide();

    // gotoshowprofile_fn();

    var postData = JSON.stringify({
        "actor": localStorage.userid_toseeprof
    });

    $.ajax({
        url: viewdprofile_api,
        type: 'POST',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": 'Token ' + localStorage.wutkn + ''
        },
        success: function(data) {
            // alert("profile view success");
        },
        error: function(edata) {
            console.log("error occured in my profile data view api hit");
        }
    });

    $.ajax({
        url: myprofile_api + localStorage.userid_toseeprof + '/',
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": 'Token ' + localStorage.wutkn + ''
        },
        success: function(data) {

            localStorage.myprofiledata1 = JSON.stringify(data);
            gotoshowprofile_fn();

        },
        error: function(edata) {
            console.log("error occured in my profile data");
        }
    });

});

$(".hidemailid").click(function() {
    $(".hidemailid").hide();
    $(".showemailid").show();
});

// $(".hidephoneno").click(function() {
//     $(".hidephoneno").hide();
//     $(".showphoneno").show();
// });

$(".imgnexticon").click(function() {
    $(".firstsetimages").hide();
    $(".secondsetimages").slideToggle();
    $(".imgnexticon").hide();
    $(".imgprevicon").show();
});
$(".imgprevicon").click(function() {
    $(".secondsetimages").hide();
    $(".firstsetimages").slideToggle();
    $(".imgnexticon").show();
    $(".imgprevicon").hide();
});


// gotoshowprofile_fn fn starts headers
function gotoshowprofile_fn() {

    var data = JSON.parse(localStorage.myprofiledata1);

    // BASIC INFO STARTS HERE

    // 18/11 -v request number and photo
    data.username === null ? $(".req_number_btn").show() : $(".req_number_btn").remove();
    data.user_photos.length === 0 ? $(".req_photo_btn").show() : $(".req_photo_btn").remove();
    //18/11 -v request number and photo

    //profile picture section
    if (data.user_photos.length >= 1) {
        $(".mainimage").attr("src", data.user_photos[0].image);
        $(".mainimage").attr("href", data.user_photos[0].image);

        $(".mainimageupdatedone").empty().append(`<a href="${data.user_photos[0].image}" data-lightbox="Gallery 1" class="mainimage" src="${data.user_photos[0].image}" style="background-image: url('${data.user_photos[0].image}');height: 350px;display: block;background-position: top center;background-size:cover;"></a>`);

        for (var i = 1; i < data.user_photos.length; i++) {
            if (i >= 1 && i < 5) {
                $(".firstsetimages img").eq(i - 1).attr("src", data.user_photos[i].image);
                $(".firstsetimages a").eq(i - 1).attr("href", data.user_photos[i].image);
            } else {
                $(".secondsetimages img").eq(i - 4).attr("src", data.user_photos[i].image);
                $(".secondsetimages a").eq(i - 4).attr("href", data.user_photos[i].image);
            }
        }
    }

    if (data.user_photos.length > 5) {
        $(".imgnexticon").show();
    } else {
        $(".imgprevicon,.imgnexticon").hide();
    }

    if (data.user_photos.length == 0 && data.userprofile.profile_pic != null) {
        $(".mainimage").attr("src", data.userprofile.profile_pic);
        $(".mainimage").attr("href", data.userprofile.profile_pic);
        $(".mainimageupdatedone").empty().append(`<a href="${data.userprofile.profile_pic}" data-lightbox="Gallery 1" class="mainimage" src="${data.userprofile.profile_pic}" style="background-image: url('${data.userprofile.profile_pic}');height: 350px;display: block;background-position: top center;background-size:cover;"></a>`);
    }

    //name , age ,gender , about them ,mailid , phoneno
    var gentereq = (data.userdetails.gender == "Male") ? "Mr. " : "Ms. ";
    $(".dynusername").text(gentereq + data.first_name + " " + data.last_name + " (" + data.userprofile.uid + ")");

    var currentyear = parseInt(new Date().getFullYear());
    var userage = currentyear - parseInt(data.userdetails.dob.substring(0, 4));
    $(".dynuserage").text(userage);
    $(".dynusergender").text(data.userdetails.gender);

    (data.about_me != null) ? $(".dynuseraboutthem").text(data.about_me.about): $(".dynuseraboutthem").text("Not Mentioned");

    data.is_shortlisted ? $(".likethismember").hide() : $(".likethismember").show();

    // $(".dynuserphoneno").text(data.username).attr("href", "tel:" + data.username + "");
    $(".dynuseremailid").text(data.email).attr("mailto", "tel:" + data.email + "");
    // $(".dynuserphoneno").text(data.username).attr("href", "tel:" + data.username + "");
    if (data.username != null) {
        $(".dynuserphoneno").text(data.username).attr("href", "tel:" + data.username + "");
    } else {
        $(".dynuserphoneno").text("Hidden").attr("href", "tel:hidden");
    }
    // ========================================================================================================================

    //PERSONEL DETAILS SECTION

    $(".dyndob").text(data.userdetails.dob);

    if (data.user_personal_details != null) {
        //marital status fn
        $(".dynmaritalstatus").text(data.user_personal_details.marital_status.name);

        //user height fn
        var userheight = data.user_personal_details.height.toString().split(".");
        if (userheight.length == 1) {
            var showuserheight = userheight[0] + " ft";
        } else {
            var showuserheight = userheight[0] + " ft " + userheight[1] + " in";
        }
        $(".dynuserheight").text(showuserheight);

        //family status , family values , family type fn
        $(".dynuserfam_status").text(data.user_personal_details.family_status.name);
        $(".dynuserfam_value").text(data.user_personal_details.family_values.name);
        $(".dynuserfam_type").text(data.user_personal_details.family_type);
        (data.user_personal_details.is_physically_challenged == true) ? $(".dynuser_disability").text("Yes"): $(".dynuser_disability").text("No");

    } else {
        $(".dynmaritalstatus").text("Not mentioned");
        $(".dynuserheight").text("Not mentioned");
        $(".dynuserfam_status").text("Not mentioned");
        $(".dynuserfam_value").text("Not mentioned");
        $(".dynuserfam_type").text("Not mentioned");
        $(".dynuser_disability").text("Not mentioned");
    }

    //Professional Details
    if (data.user_professional_details != null) {
        $(".dynuserhighstedu").text(data.user_professional_details.highest_education.name);
        $(".dynuseremployedin").text(data.user_professional_details.employed_in.name);
        $(".dynuseroccupation").text(data.user_professional_details.occupation.name);
        if (data.user_professional_details.annual_income == 1) {
            var userincome = "1 to 3 lakhs ";
        } else if (data.user_professional_details.annual_income == 2) {
            var userincome = "3 to 5 lakhs ";
        } else if (data.user_professional_details.annual_income == 3) {
            var userincome = "5 to 10 lakhs ";
        } else if(data.user_professional_details.annual_income == 4) {
            var userincome = "10 lakhs and more";
        } else if(data.user_professional_details.annual_income == 5) {
            var userincome = "Below 1 lakh";
        }
        $(".dynuserincome").text(data.user_professional_details.currency.name + " " + userincome + " per annum");
    } else {
        $(".dynuserhighstedu").text("Not mentioned");
        $(".dynuseremployedin").text("Not mentioned");
        $(".dynuseroccupation").text("Not mentioned");
        $(".dynuserincome").text("Not mentioned");
    }

    //Hobbies & Interests details
    if (data.hobbies.length != 0) {
        var userhobbies = "";
        for (var i = 0; i < data.hobbies.length; i++) {
            userhobbies += data.hobbies[i].hobby.name + " , ";
        }
        $(".dynuserhobbies").text(userhobbies.slice(0, -2) + ".");
    } else {
        $(".dynuserhobbies").text("Not mentioned")
    }

    if (data.favourite_music.length != 0) {
        var favmusics = "";
        for (var i = 0; i < data.favourite_music.length; i++) {
            favmusics += data.favourite_music[i].music.name + " , ";
        }
        $(".dynuserfavmusic").text(favmusics.slice(0, -2) + ".");
    } else {
        $(".dynuserfavmusic").text("Not mentioned")
    }

    if (data.sports.length != 0) {
        var favsports = "";
        for (var i = 0; i < data.sports.length; i++) {
            favsports += data.sports[i].sport.name + " , ";
        }
        $(".dynuserfavsports").text(favsports.slice(0, -2) + ".");
    } else {
        $(".dynuserfavsports").text("Not mentioned")
    }

    if (data.spoken_languages.length != 0) {
        var spokenlang = "";
        for (var i = 0; i < data.spoken_languages.length; i++) {
            spokenlang += data.spoken_languages[i].language.name + " , ";
        }
        $(".dynuserspokenlang").text(spokenlang.slice(0, -2) + ".");
    } else {
        $(".dynuserspokenlang").text("Not mentioned")
    }

    // Basic Information Details
    if (data.basic_information != null) {
        $(".dynuserbodytype").text(data.basic_information.body_type);
        $(".dynuserskintone").text(data.basic_information.skin_tone);
        $(".dynuserweight").text(data.basic_information.weight + " Kgs");
    } else {
        $(".dynuserbodytype").text("Not mentioned");
        $(".dynuserskintone").text("Not mentioned");
        $(".dynuserweight").text("Not mentioned");
    }

    if (data.lifestyle != null) {
        $(".dynusereatinghabits").text(data.lifestyle.eating_habit);
        $(".dynuserdrinkinghabits").text(data.lifestyle.drinking_habit);
        $(".dynusersmokinghabits").text(data.lifestyle.smoking_habit);
    } else {
        $(".dynusereatinghabits").text("Not mentioned");
        $(".dynuserdrinkinghabits").text("Not mentioned");
        $(".dynusersmokinghabits").text("Not mentioned");
    }

    if (data.family_info != null) {
        $(".dynuserfatherstatus").text(data.family_info.father_status.name);
        $(".dynusermotherstatus").text(data.family_info.mother_status.name);
        $(".dynuserparentphnno").text(data.family_info.contact_number);
    } else {
        $(".dynuserfatherstatus").text("Not mentioned");
        $(".dynusermotherstatus").text("Not mentioned");
        $(".dynuserparentphnno").text("Not mentioned");
    }

    // Religious Details
    $(".dynuserreligion").text(data.userdetails.religion.name);
    $(".dynusermothertongue").text(data.userdetails.mother_tongue.name);

    if (data.user_religion_details != null) {
        //user caste , subcaste , gothra , Dosham
        $(".dynusercaste").text(data.user_religion_details.caste.name);

        if (data.user_religion_details.subcaste != null) {
            $(".dynusersubcaste").text(data.user_religion_details.subcaste.name);
        } else {
            $(".dynusersubcaste").text("Not mentioned");
        }

        (data.user_religion_details.gothram != null) ? $(".dynusergothra").text(data.user_religion_details.gothram): $(".dynusergothra").text("Not mentioned");

        if (data.user_dosham.length != 0) {
            var userdosham = "";
            for (var i = 0; i < data.user_dosham.length; i++) {
                userdosham += data.user_dosham[i].dosham.name + " , ";
            }
            $(".dynuserdosham").text(data.user_religion_details.dosham_choices + " - " + userdosham.slice(0, -2) + ".");
        } else {
            $(".dynuserdosham").text(data.user_religion_details.dosham_choices);
        }

    } else {
        $(".dynusercaste").text("Not mentioned");
        $(".dynusersubcaste").text("Not mentioned");
        $(".dynusergothra").text("Not mentioned");
        $(".dynuserdosham").text("Not mentioned");
    }

    if (data.religion_info != null) {
        $(".dynuserstar").text(data.religion_info.star.name);
        $(".dynuserraasi").text(data.religion_info.raasi.name);
    } else {
        $(".dynuserstar").text("Not mentioned");
        $(".dynuserraasi").text("Not mentioned");
    }

    //PARTNER DETAILS 
    if (data.partner_basic_info != null) {

        //partner starts age and end age
        $(".dynuserp_startage").text(data.partner_basic_info.from_age);
        $(".dynuserp_endage").text(data.partner_basic_info.to_age);

        //partner starts height and end height
        
        var userp_startheight = data.partner_basic_info.from_height.toString().split(".");
        if (userp_startheight[0].length > 1) {
            var showuserp_startheight = userp_startheight[0] + " cm";
        } else if (userp_startheight[1] && userp_startheight[0].length == 1){
            var showuserp_startheight = userp_startheight[0] + " ft " + userp_startheight[1] + " in";
        } else {
            var showuserp_startheight = userp_startheight[0] + " ft";
        }
        $(".dynuserp_startheight").text(showuserp_startheight);

        var userp_endheight = data.partner_basic_info.to_height.toString().split(".");
        if (userp_endheight[0].length > 1) {
            var showuserp_endheight = userp_endheight[0] + " cm";
        } else if (userp_endheight[1] && userp_endheight[0].length == 1) {
            var showuserp_endheight = userp_endheight[0] + " ft " + userp_endheight[1] + " in";
        } else{
            var showuserp_endheight = userp_endheight[0] + " ft";
        }
        $(".dynuserp_endheight").text(showuserp_endheight);

        //partner starts weight and end weight
        $(".dynuserp_startweight").text(data.partner_basic_info.from_weight + " Kgs");
        $(".dynuserp_endweight").text(data.partner_basic_info.to_weight + " Kgs");

        //partner marital status
        if (data.marital_status.length != 0) {
            var partnermaritalstatus = "";
            for (var i = 0; i < data.marital_status.length; i++) {
                partnermaritalstatus += data.marital_status[i].marital_status.name + " , ";
            }
            $(".dynuserp_maritalstatus").text(partnermaritalstatus.slice(0, -2) + ".");
        } else {
            $(".dynuserp_maritalstatus").text("Not mentioned");
        }

        //physical status fn
        if (data.partner_basic_info.physical_status == 1) {
            $(".dynuserp_physicalstatus").text("Normal");
        } else if (data.partner_basic_info.physical_status == 2) {
            $(".dynuserp_physicalstatus").text("Physically Challenged");
        } else {
            $(".dynuserp_physicalstatus").text("Doesn't matter");
        }

        $(".dynuserp_eatinghabits").text(data.partner_basic_info.eating_habit);
        $(".dynuserp_drinkinghabits").text(data.partner_basic_info.drinking_habit);
        $(".dynuserp_smokinghabits").text(data.partner_basic_info.smoking_habit);

    } else {
        $(".dynuserpartnerage").text("Not mentioned");
        $(".dynuserpartnerheight").text("Not mentioned");
        $(".dynuserpartnerweight").text("Not mentioned");
        $(".dynuserp_physicalstatus").text("Not mentioned");
        $(".dynuserp_eatinghabits").text("Not mentioned");
        $(".dynuserp_drinkinghabits").text("Not mentioned");
        $(".dynuserp_smokinghabits").text("Not mentioned");
        $(".dynuserp_maritalstatus").text("Not mentioned");
    }

    //partner Religion
    if (data.partner_religion.length != 0) {
        var partnerreligion = "";
        for (var i = 0; i < data.partner_religion.length; i++) {
            partnerreligion += data.partner_religion[i].religion.name + " , ";
        }
        $(".dynuserp_religion").text(partnerreligion.slice(0, -2) + ".");
    } else {
        $(".dynuserp_religion").text("Not mentioned");
    }

    //partner Mother Tongue
    if (data.partner_mother_tongue.length != 0) {
        var partnermothertongue = "";
        for (var i = 0; i < data.partner_mother_tongue.length; i++) {
            partnermothertongue += data.partner_mother_tongue[i].mother_tongue.name + " , ";
        }
        $(".dynuserp_mothertongue").text(partnermothertongue.slice(0, -2) + ".");
    } else {
        $(".dynuserp_mothertongue").text("Not mentioned");
    }

    //partner caste
    if (data.partner_caste != null) {
        $(".dynuserp_caste").text(data.partner_caste.caste.name);
        $(".dynuserp_manglik").text(data.partner_caste.manglik);
    } else {
        $(".dynuserp_caste").text("Not Mentioned");
        $(".dynuserp_manglik").text("Not Mentioned");
    }

    //partner Star 
    if (data.partner_star.length != 0) {
        var partnerstar = "";
        for (var i = 0; i < data.partner_star.length; i++) {
            partnerstar += data.partner_star[i].star.name + " , ";
        }
        $(".dynuserp_star").text(partnerstar.slice(0, -2) + ".");
    } else {
        $(".dynuserp_star").text("Not mentioned");
    }

    //partner country 
    if (data.partner_country.length != 0) {
        var partnercountry = "";
        for (var i = 0; i < data.partner_country.length; i++) {
            partnercountry += data.partner_country[i].country.name + " , ";
        }
        $(".dynuserp_country").text(partnercountry.slice(0, -2) + ".");
    } else {
        $(".dynuserp_country").text("Not mentioned");
    }

    //partner citizenship 
    if (data.partner_citizenship.length != 0) {
        var partnercitizenship = "";
        for (var i = 0; i < data.partner_citizenship.length; i++) {
            partnercitizenship += data.partner_citizenship[i].citizenship.name + " , ";
        }
        $(".dynuserp_citizenship").text(partnercitizenship.slice(0, -2) + ".");
    } else {
        $(".dynuserp_citizenship").text("Not mentioned");
    }

    //partner education 
    if (data.partner_education.length != 0) {
        var partnereducation = "";
        for (var i = 0; i < data.partner_education.length; i++) {
            partnereducation += data.partner_education[i].education.name + " , ";
        }
        $(".dynuserp_education").text(partnereducation.slice(0, -2) + ".");
    } else {
        $(".dynuserp_education").text("Not mentioned");
    }

    //partner occupation 
    if (data.partner_occupation.length != 0) {
        var partneroccupation = "";
        for (var i = 0; i < data.partner_occupation.length; i++) {
            partneroccupation += data.partner_occupation[i].occupation.name + " , ";
        }
        $(".dynuserp_occupation").text(partneroccupation.slice(0, -2) + ".");
    } else {
        $(".dynuserp_occupation").text("Not mentioned");
    }

    //partner description
    if (data.partner_description != null) {
        if (data.partner_description.partner_income == 1) {
            var salary = "1 lakh to 3 lakhs";
        } else if (data.partner_description.partner_income == 2) {
            var salary = "3 lakhs to 5 lakhs";
        } else if (data.partner_description.partner_income == 3) {
            var salary = "5 lakhs to 10 lakhs";
        } else if (data.partner_description.partner_income == 4) {
            var salary = "10 lakhs and more";
        } else if (data.partner_description.partner_income == 5) {
            var salary = "1 lakhs and below";
        }
        $(".dynuserp_annualincome").text(data.partner_description.currency.name + " " + salary + " per annum");

        // $(".dynuserp_annualincome").text(data.partner_description.currency.name + " " + data.partner_description.income_from + " to " + data.partner_description.income_to + " per annum");
        $(".dynuserp_partnerdesc").text(data.partner_description.about_partner);
    } else {
        $(".dynuserp_annualincome").text("Not Mentioned");
        $(".dynuserp_partnerdesc").text("Not Mentioned");
    }

    if (data.user_address != null) {
        // $(".dynuseraddr").text(data.user_address.street + " , " + data.user_address.area + " , " + data.user_address.city.name + " , " + data.user_address.city.state.name + " , " + data.user_address.city.state.country.name + " , " + data.user_address.zipcode + " .");
        $(".dynuseraddr").text(data.user_address.city.name + " , " + data.user_address.city.state.name + " , " + data.user_address.city.state.country.name + ".");
    } else {
        $(".dynuseraddr").text("Not Mentioned");
    }

    var gentereq = (localStorage.gender == "Male") ? "Mr. " : "Ms. ";
    $(".dynusernamebtm").text(gentereq + data.first_name + " " + data.last_name);



} // gotoshowprofile_fn fn ends headers

//accept n deny user fn starts here
// ajax call for request in new matches page
function acceptanddenyusers(type) {

    if (type == 1) {
        var url = sendnewrequest_intrestedprof_api;
    } else {
        var url = denynewrequest_intrestedprof_api;
    }

    var postData = JSON.stringify({

        "actor": localStorage.userid_toseeprof

    });

    $.ajax({
        url: url,
        type: 'post',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {

            // $(elem).find(".interests").removeClass("actionactive");

            if (type == 1) {
                $("#snackbarsuccs").text("Your Request has been sent to " + $(".dynusername").text());
            } else {
                $("#snackbarsuccs").text("You are denied " + $(".dynusername").text() + " profile");
            }
            showsuccesstoast();

            $(".likeperson").remove();
        },
        error: function(data) {
            console.log("error occured during accept request");
        }
    });

} // ajax call for request in new matches page

//phone no view fn starts here
function showuserphoeno() {

    if (JSON.parse(localStorage.myprofiledata1).username) {
        var postData = JSON.stringify({
            "actor": localStorage.userid_toseeprof
        });

        $.ajax({
            url: viewmobileno_api,
            type: 'post',
            data: postData,
            headers: {
                "content-type": 'application/json',
                "Authorization": "Token " + localStorage.wutkn
            },
            success: function(data) {
                // var mydata = JSON.parse(localStorage.myprofiledata);
                // $(".dynuserphoneno").text(mydata.username).attr("href", "tel:" + mydata.username + "");
                // document.getElementById('welcomeDiv').style.display = "block";
                // document.getElementById('textbtn').style.display = "none";
                $(".hidephoneno").hide();
                $(".showphoneno").show();
                //20/11 -v
            },
            error: function(data) {
                console.log("error occured during phoneno view");
                for (var key in JSON.parse(data.responseText)) {
                    $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
                }
                showerrtoast();
            }
        });
    } else {
        $(".hidephoneno").hide();
        $(".showphoneno").show();
    }//20/11 -v
}



//18/11 -v request photo function
function requestphoto() {
    var postData = JSON.stringify({
        actor: localStorage.userid_toseeprof,
        user: JSON.parse(localStorage.useracccred).id
    });
    $.ajax({
        url: requestphoto_api,
        type: "post",
        data: postData,
        headers: {
            "Authorization": "Token " + localStorage.wutkn,
            "content-type": "application/json"
        },
        success: function(data) {
            $("#snackbarsuccs").text("Request For Photo has been Sent");
            showsuccesstoast();
        },
        error: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
} //function ends here



//18/11 -v request number function
function requestnumber() {
    var postData = JSON.stringify({
        actor: localStorage.userid_toseeprof,
        user: JSON.parse(localStorage.useracccred).id
    });
    $.ajax({
        url: requestphonenumber_api,
        type: "post",
        data: postData,
        headers: {
            "Authorization": "Token " + localStorage.wutkn,
            "content-type": "application/json"
        },
        success: function(data) {
            $("#snackbarsuccs").text("Request For Phone Number has been Sent");
            showsuccesstoast();
        },
        error: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
} //function ends here