$(function() {

    $('.loadericon,.loadericontwo,.loadericonthree,.resendtimeshow').hide();

    $(".cliend_id").text(localStorage.matrimonyid);
    $(".client_phoneno").text(localStorage.userregphoneno);

    sendOTP(); //send otp fn

});

$("#otpcode,#editedmobileno").click(function() {
    $("input").removeClass("iserror");
});

//send otp fn starts here
function sendOTP() {

    $("#loaderspinner").show();
    $.ajax({
        url: sendotptouser_api,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": 'Token ' + localStorage.wutkn + ''
        },
        success: function(data) {
            $("#loaderspinner").hide();
            $("#snackbarsuccs").text("OTP has been sent to your register phone no " + localStorage.userregphoneno);
            showsuccesstoast();
        },
        error: function(data) {
            $("#loaderspinner").hide();
            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    });

}

//gotochecktime fn starts here
function gotocheck() {
    $(".resendtimeshow").hide();
    $(".resentotpatag").show();
}

//verify otp fn starts here
function verifyotp() {

    if ($('#otpcode').val().trim() == '') {
        $("#snackbarerror").text("OTP code is required");
        $('#otpcode').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $(".otpverifyBtn").attr("disabled", true);
    $('.loadericon').show();

    var postData = JSON.stringify({
        "otp": $("#otpcode").val()
    });

    $.ajax({
        url: otpverification_api,
        type: 'POST',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": 'Token ' + localStorage.wutkn + ''
        },
        success: function(data) {
            $(".otpverifyBtn").attr("disabled", false);
            $('.loadericon').hide();
        },
        error: function(data) {

            $(".otpverifyBtn").attr("disabled", false);
            $('.loadericon').hide();

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {
        // window.location.href = "register-phase3.html";
        window.location.replace("register-phase3.html");
    });

} //verify otp fn starts here

//resend opt fn starts here
function resendotp() {

    $('.loadericontwo').show();

    $.ajax({
        url: sendotptouser_api,
        type: 'GET',
        headers: {
            "content-type": 'application/json',
            "Authorization": 'Token ' + localStorage.wutkn + ''
        },
        success: function(data) {
            $('.loadericontwo').hide();
            $("#snackbarsuccs").text("OTP has been sent to your register phone no " + localStorage.userregphoneno);
            showsuccesstoast();
            //timer fn starts here
            $(".resentotpatag").hide();
            $(".resendtimeshow").show();
            $(".timershow").text("05:00");
            var interval = setInterval(function() {
                var timer = $('.timershow').html();
                timer = timer.split(':');
                var minutes = parseInt(timer[0], 10);
                var seconds = parseInt(timer[1], 10);
                seconds -= 1;
                if (minutes < 0) return clearInterval(interval);
                if (minutes < 10 && minutes.length != 2) minutes = '0' + minutes;
                if (seconds < 0 && minutes != 0) {
                    minutes -= 1;
                    seconds = 59;
                } else if (seconds < 10 && length.seconds != 2) seconds = '0' + seconds;
                $('.timershow').html(minutes + ':' + seconds);

                if (minutes == 0 && seconds == 0) {
                    clearInterval(interval);
                    gotochecktime();
                }

            }, 1000);

        },
        error: function(data) {
            $('.loadericontwo').hide();
            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    });

} //resend opt fn starts here

//change my mobile number fn starts here
function changemymobileno() {

    if ($('#editedmobileno').val().trim() == '') {
        $("#snackbarerror").text("Mobile No is required");
        $('#editedmobileno').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $(".updateBtn").attr("disabled", true);
    $('.loadericonthree').show();

    var postData = JSON.stringify({
        "username": $("#editedmobileno").val()
    });

    $.ajax({
        url: editmobileno_api,
        type: 'PUT',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": 'Token ' + localStorage.wutkn + ''
        },
        success: function(data) {
            $(".updateBtn").attr("disabled", false);
            $('.loadericonthree').hide();
        },
        error: function(data) {

            $(".updateBtn").attr("disabled", false);
            $('.loadericonthree').hide();

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {

        $("#snackbarsuccs").text("Your mobile number has been changed successfully!");
        showsuccesstoast();
        localStorage.userregphoneno = $("#editedmobileno").val();
        $(".client_phoneno").text(localStorage.userregphoneno);
        $(".updatenumbersection").hide();
        $(".verifynumbersection").show();

        sendOTP();

    });

} //change my mobile number fn starts here
