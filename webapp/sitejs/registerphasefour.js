var hobbiescollection = [];
var musiccollection = [];
var sportscollection = [];
var spokenlangcollection = [];
var hobbiesothersarr = [];
var favmusicothersarr = [];
var sportsothersarr = [];
var langothersarr = [];
$(function() {

    if (localStorage.registerphase4data == undefined) {
        getregphase4data();
    } else {
        var currentdate = new Date().getDate();
        if (currentdate != localStorage.datastoreddate_p4) {
            localStorage.removeItem("registerphase4data");
            getregphase4data();
        } else {
            getregp4datafromlocal(1); //this fn can happen 
        }
    }


    $(".loadericon").hide();

});

//get register phase 4 data from server
function getregphase4data() {

    $.ajax({
        url: getregp4data_api,
        type: 'get',
        headers: {
            'Authorization': 'Token ' + localStorage.wutkn
        },
        success: function(data) {
            localStorage.registerphase4data = JSON.stringify(data);
            localStorage.datastoreddate_p4 = new Date().getDate();

            getregp4datafromlocal(0); //this fn is common for all
        },
        error: function(edata) {
            console.log("error occured in get register phase 4 checkbox data");
        }
    });

} //get register phase 4 data from server

//get phaseone details from local
function getregp4datafromlocal(type) {

    var data = JSON.parse(localStorage.registerphase4data);

    //hobbies dyn append
    $('.hobbiesdynlist').empty();
    for (var i = 0; i < data.hobbies.length; i++) {
        // if (i == 0) {
        //     $('.hobbiesdynlist').append(" <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'> <label class='control control--checkbox displayinline doshacheckbox'>" + data.hobbies[i].name + "<input type='checkbox' checked name='hobbies' value='" + data.hobbies[i].id + "'/> <div class='control__indicator'></div></label> </div>");
        // } else {
            $('.hobbiesdynlist').append(" <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'> <label class='control control--checkbox displayinline doshacheckbox'>" + data.hobbies[i].name + "<input type='checkbox' name='hobbies' value='" + data.hobbies[i].id + "'/> <div class='control__indicator'></div></label> </div>");
        // }
    }

    //music dyn append
    $('.favmusicdynlist').empty();
    for (var i = 0; i < data.music.length; i++) {
        // if (i == 0) {
        //     $('.favmusicdynlist').append("<div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'> <label class='control control--checkbox displayinline doshacheckbox'>" + data.music[i].name + "<input type='checkbox' checked name='favmusics' value='" + data.music[i].id + "'/> <div class='control__indicator'></div></label> </div>");
        // } else {
            $('.favmusicdynlist').append("<div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'> <label class='control control--checkbox displayinline doshacheckbox'>" + data.music[i].name + "<input type='checkbox' name='favmusics' value='" + data.music[i].id + "'/> <div class='control__indicator'></div></label> </div>");
        // }
    }

    //sports dyn append
    $('.favsportsdynlist').empty();
    for (var i = 0; i < data.sports.length; i++) {
        // if (i == 0) {
        //     $('.favsportsdynlist').append(" <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'> <label class='control control--checkbox displayinline doshacheckbox'>" + data.sports[i].name + "<input type='checkbox' checked name='favsports' value='" + data.sports[i].id + "'/> <div class='control__indicator'></div></label> </div>");
        // } else {
            $('.favsportsdynlist').append(" <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'> <label class='control control--checkbox displayinline doshacheckbox'>" + data.sports[i].name + "<input type='checkbox' name='favsports' value='" + data.sports[i].id + "'/> <div class='control__indicator'></div></label> </div>")
        // }
    }

    //spokelang dyn append
    $('.spokenlangdynlist').empty();
    for (var i = 0; i < data.languages.length; i++) {
        // if (i == 0) {
        //     $('.spokenlangdynlist').append(" <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'> <label class='control control--checkbox displayinline doshacheckbox'>" + data.languages[i].name + "<input type='checkbox' checked name='spokenlang' value='" + data.languages[i].id + "'/> <div class='control__indicator'></div></label> </div>");
        // } else {
            $('.spokenlangdynlist').append(" <div class='col-lg-4 col-md-4 col-sm-6 col-xs-12'> <label class='control control--checkbox displayinline doshacheckbox'>" + data.languages[i].name + "<input type='checkbox' name='spokenlang' value='" + data.languages[i].id + "'/> <div class='control__indicator'></div></label> </div>");
        // }
    }

} //get phaseone details from local


//register phase 4 fn starts here
function savereg_phase4() {

     if ($('input[name="hobbies"]:checked').length == 0) {
        $("#snackbarerror").text("Hobbies and interests is required");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('input[name="favmusics"]:checked').length == 0) {
        $("#snackbarerror").text("Favorite musics list is required");
        $("html, body").animate({ scrollTop: 700 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('input[name="favsports"]:checked').length == 0) {
        $("#snackbarerror").text("Sports and Fitness  is required");
        $("html, body").animate({ scrollTop: 1100 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('input[name="spokenlang"]:checked').length == 0) {
        $("#snackbarerror").text("Spoken languages is required");
        $("html, body").animate({ scrollTop: 1600 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $(".loadericon").show();
    $(".saveBtn").attr("disabled", true);

    for (var i = 0; i < $('[name="hobbies"]:checked').length; i++) {
        hobbiescollection.push({ "hobby": $('[name="hobbies"]:checked').eq(i).val() })
    }
    for (var i = 0; i < $('[name="favmusics"]:checked').length; i++) {
        musiccollection.push({ "music": $('[name="favmusics"]:checked').eq(i).val() })
    }
    for (var i = 0; i < $('[name="favsports"]:checked').length; i++) {
        sportscollection.push({ "sport": $('[name="favsports"]:checked').eq(i).val() })
    }
    for (var i = 0; i < $('[name="spokenlang"]:checked').length; i++) {
        spokenlangcollection.push({ "language": $('[name="spokenlang"]:checked').eq(i).val() })
    }

    // if ($(".addedTag0").length != 0) {
    //     for (var i = 0; i < $(".addedTag0").length; i++) {
    //         hobbiesothersarr.push({ "hobby": $("#hobbiesothers li input").eq(i).val() });
    //     }
    // }
    // if ($(".addedTag1").length != 0) {
    //     for (var i = 0; i < $(".addedTag1").length; i++) {
    //         favmusicothersarr.push({ "music": $("#favmusicsothers li input").eq(i).val() });
    //     }
    // }
    // if ($(".addedTag2").length != 0) {
    //     for (var i = 0; i < $(".addedTag2").length; i++) {
    //         sportsothersarr.push({ "sport": $("#sportsandfitnessothers li input").eq(i).val() });
    //     }
    // }
    // if ($(".addedTag3").length != 0) {
    //     for (var i = 0; i < $(".addedTag3").length; i++) {
    //         langothersarr.push({ "language": $("#spkoenlangothers li input").eq(i).val() });
    //     }
    // }


    var Datatosend = {
        "hobbies": hobbiescollection,
        "favourite_music": musiccollection,
        "sports": sportscollection,
        "spoken_languages": spokenlangcollection
        // "other_hobbies": hobbiesothersarr,
        // "other_music": favmusicothersarr,
        // "other_sport": sportsothersarr,
        // "other_language": langothersarr
    };


    // if (hobbiesothersarr.length == 0) {
    //     delete Datatosend.hobbiespthers;
    // }
    // if (favmusicothersarr.length == 0) {
    //     delete Datatosend.favmusicspthers;
    // }
    // if (sportsothersarr.length == 0) {
    //     delete Datatosend.favsportspthers;
    // }
    // if (langothersarr.length == 0) {
    //     delete Datatosend.spokenlangpthers;
    // }

    var postData = JSON.stringify(Datatosend);

    $.ajax({
        url: sendrp4data_api,
        type: 'POST',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": 'Token ' + localStorage.wutkn + ''
        },
        success: function(data) {
            $(".saveBtn").attr("disabled", false);
            $('.loadericon').hide();
        },
        error: function(data) {

            $(".saveBtn").attr("disabled", false);
            $('.loadericon').hide();

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {
        hobbiescollection = [];
        musiccollection = [];
        sportscollection = [];
        spokenlangcollection = [];
        // window.location.href = "register-phase5.html";
        window.location.replace("register-phase5.html");
    });

} //register phase 4 fn starts here
