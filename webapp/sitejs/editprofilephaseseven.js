var maritalstatus = [];
var aboutthem = [];
var aboutthierhobbies = [];
$(function() {

    $(".loadericon").hide();
    $(".helpmewritethis").hide();

    if (localStorage.editphase7data == undefined) {
        geteditphase7data();
    } else {
        var currentdate = new Date().getDate();
        if (currentdate != localStorage.datastoreddate_ep7) {
            localStorage.removeItem("editphase7data");
            geteditphase7data();
        } else {
            geteditp7datafromlocal(1); //this fn can happen 
        }
    }

    usergivendata();

    $("#ep7_pendage").change(function() {
        if ($("#ep7_pendage").val() < $("#ep7_pstartage").val()) {
            $("#snackbarerror").text("End Age Should be higher than Start Age");
            $('#select2-ep7_pendage-container').addClass("iserror");
            showiperrtoast();
            event.stopPropagation();
            return;
        }
    });

}); //initial fn ends here

// geteditphase7data fn starts here
function geteditphase7data() {

    $.ajax({
        url: editprofilep7_dd_api,
        type: 'get',
        headers: {
            "content-type": 'application/json',
        },
        success: function(data) {

            localStorage.editphase7data = JSON.stringify(data);
            localStorage.datastoreddate_ep7 = new Date().getDate();

            geteditp7datafromlocal(0); //this fn is common for all
        },
        error: function(edata) {
            console.log("error occured in get edit phase 7 dd and others data");
        }
    });

} // geteditphase7data fn ends here

//Help me write this phase starts here

$("#ep7_paboutpartner").keyup(function() {
    $(".totalcharacters").text($("#ep7_paboutpartner").val().length);
});

$(".helpmemodal").click(function() {
    aboutthem = [];
    aboutthierhobbies = [];
    aboutthem.push($(".aboutthemlist .active").text());
    aboutthierhobbies.push($(".aboutthierhobbieslist .active").text());
});

$(".aboutthem").click(function() {

    $(this).find(".descr").toggleClass("active");
    if ($(".aboutthemlist .active").length < 1) {
        $(this).find(".descr").toggleClass("active");
    }
    if ($(".aboutthemlist .active").length > 5) {
        $(this).find(".descr").removeClass("active");
    }

    aboutthem = [];
    for (var i = 0; i < $(".aboutthemlist .active").length; i++) {
        aboutthem.push($(".aboutthemlist .active").eq(i).text());
    }

    // aboutthierhobbies
    if (localStorage.profilecratedforname != "Myself") {
        var genderctgry = (localStorage.gender == "Male") ? "His" : "Her";
        var texttoshow = "My " + localStorage.profilecratedforname + " is very " + aboutthem + " and " + genderctgry + " Hobbies and Interests are " + aboutthierhobbies + " . ";
    } else {
        var texttoshow = "People around me used to tell that am very " + aboutthem + " and my Hobbies and Interests are " + aboutthierhobbies + " . ";
    }

    $("#aboutuscreation").val(texttoshow);
    $("#ep7_paboutpartner").val(texttoshow);
    $(".totalcharacters").text(texttoshow.length);

});

$(".aboutthierhobbies").click(function() {

    $(this).find(".descr").toggleClass("active");
    if ($(".aboutthierhobbieslist .active").length < 1) {
        $(this).find(".descr").toggleClass("active");
    }
    if ($(".aboutthierhobbieslist .active").length > 5) {
        $(this).find(".descr").removeClass("active");
    }
    aboutthierhobbies = [];
    for (var i = 0; i < $(".aboutthierhobbieslist .active").length; i++) {
        aboutthierhobbies.push($(".aboutthierhobbieslist .active").eq(i).text());
    }

    // aboutthierhobbies
    if (localStorage.profilecratedforname != "Myself") {
        var genderctgry = (localStorage.gender == "Male") ? "His" : "Her";
        var texttoshow = "My " + localStorage.profilecratedforname + " is very " + aboutthem + " and " + genderctgry + " hobbies and interests are " + aboutthierhobbies + " . ";
    } else {
        var texttoshow = "People around me used to tell that am very " + aboutthem + " and my hobbies and interests are " + aboutthierhobbies + " . ";
    }

    $("#aboutuscreation").val(texttoshow);
    $("#ep7_paboutpartner").val(texttoshow);
    $(".totalcharacters").text(texttoshow.length);

});


// localstorage data processing
function geteditp7datafromlocal(type) {

    var data = JSON.parse(localStorage.editphase7data);

    //relogion data - dyn
    $("#ep7_preligion").empty();
    for (var i = 0; i < data.religion.length; i++) {
        $("#ep7_preligion").append("<option value='" + data.religion[i].id + "'>" + data.religion[i].name + "</option>");
    }

    //religion , mother tongue data - dyn , caste dyn ,star syn
    $("#ep7_pmothertongue").empty();
    for (var i = 0; i < data.mother_tongue.length; i++) {
        $("#ep7_pmothertongue").append("<option value='" + data.mother_tongue[i].id + "'>" + data.mother_tongue[i].name + "</option>");
    }

    $("#ep7_pcast").empty().append('<option value="null">Select Your Partner Caste</option>');
    for (var i = 0; i < data.caste.length; i++) {
        $("#ep7_pcast").append("<option value='" + data.caste[i].id + "'>" + data.caste[i].name + "</option>");
    }

    $("#ep7_pstar").empty();
    for (var i = 0; i < data.star.length; i++) {
        $("#ep7_pstar").append("<option value='" + data.star[i].id + "'>" + data.star[i].name + "</option>");
    }

    //country and citizenship dyn loading
    $("#ep7_pcountry,#ep7_pcitizenship").empty();
    for (var i = 0; i < data.country.length; i++) {
        $("#ep7_pcountry,#ep7_pcitizenship").append("<option value='" + data.country[i].id + "'>" + data.country[i].name + "</option>");
    }

    //education dyn
    $('#ep7_peducation').empty();
    for (var i = 0; i < data.education.length; i++) {
        $('#ep7_peducation').append("<optgroup class='a educationsubmenu" + data.education[i].id + "' label='" + data.education[i].name + "'></optgroup>");
        for (var j = 0; j < data.education[i].branch.length; j++) {
            $(".educationsubmenu" + data.education[i].id).append("<option value='" + data.education[i].branch[j].id + "'>" + data.education[i].branch[j].name + "</option>");
        }
    }

    //occupation dyn
    $('#ep7_poccupation').empty();
    for (var i = 0; i < data.occupation.length; i++) {
        $('#ep7_poccupation').append("<optgroup class='a occupationsubmenu" + data.occupation[i].id + "' label='" + data.occupation[i].name + "'></optgroup>");
        for (var j = 0; j < data.occupation[i].occupation.length; j++) {
            $(".occupationsubmenu" + data.occupation[i].id).append("<option value='" + data.occupation[i].occupation[j].id + "'>" + data.occupation[i].occupation[j].name + "</option>");
        }
    }

    //incometype dyn loading
    $("#ep7_pincomeincurrency").empty().append('<option value="null">Select Currency Type</option>');
    for (var i = 0; i < data.currency.length; i++) {
        $("#ep7_pincomeincurrency").append("<option value='" + data.currency[i].id + "'>" + data.currency[i].name + "</option>");
    }
     $('#ep7_pincomeincurrency option[value="98"]').attr("selected", true);

} // localstorage data processing

function usergivendata() {

    var data = JSON.parse(localStorage.myprofiledata);

    //physical status , eating , drinking , smoking
    if (data.partner_basic_info != null) {
        $('#ep7_pstartage option[value="' + data.partner_basic_info.from_age + '"]').attr("selected", true);
        $('#ep7_pendage option[value="' + data.partner_basic_info.to_age + '"]').attr("selected", true);
        $('#ep7_pstartheight option[value="' + data.partner_basic_info.from_height + '"]').attr("selected", true);
        $('#ep7_pendheight option[value="' + data.partner_basic_info.to_height + '"]').attr("selected", true);
        $('#ep7_pstartweight option[value="' + data.partner_basic_info.from_weight + '"]').attr("selected", true);
        $('#ep7_pendweight option[value="' + data.partner_basic_info.to_weight + '"]').attr("selected", true);

        $('input:radio[name="ep7_pphysicalstatus"]').filter('[value="' + data.partner_basic_info.physical_status + '"]').attr('checked', true);
        $('input:radio[name="ep7_peatinghabits"]').filter('[value="' + data.partner_basic_info.eating_habit + '"]').attr('checked', true);
        $('input:radio[name="ep7_pdrinkinghabits"]').filter('[value="' + data.partner_basic_info.drinking_habit + '"]').attr('checked', true);
        $('input:radio[name="ep7_psmokinghabits"]').filter('[value="' + data.partner_basic_info.smoking_habit + '"]').attr('checked', true);
    }

    //marital status values get fn
    if (data.marital_status.length != 0) {
        for (var i = 0; i < data.marital_status.length; i++) {
            $('input:checkbox[name="ep7_pmaritalstatus"]').filter('[value="' + data.marital_status[i].marital_status.id + '"]').attr('checked', true);
        }
    }

    //religion user given data
    if (data.partner_religion.length != 0) {
        for (var i = 0; i < data.partner_religion.length; i++) {
            $('#ep7_preligion option[value="' + data.partner_religion[i].religion.id + '"]').attr("selected", true);
        }
    }

    //mothertongue user given data
    if (data.partner_mother_tongue.length != 0) {
        for (var i = 0; i < data.partner_mother_tongue.length; i++) {
            $('#ep7_pmothertongue option[value="' + data.partner_mother_tongue[i].mother_tongue.id + '"]').attr("selected", true);
        }
    }

    //caste user given data , //willing to marry other community checkbox ,  //manglik check fn
    if (data.partner_caste != null) {
        $('#ep7_pcast option[value="' + data.partner_caste.caste.id + '"]').attr("selected", true);
        if (data.partner_caste.is_willing == true) {
            $('input:checkbox[name="ep7_willingtomarryothers"]').attr('checked', true);
        }
        $('input:radio[name="ep7_pmanglik"]').filter('[value="' + data.partner_caste.manglik + '"]').attr('checked', true);
    }

    //star user given data
    if (data.partner_star.length != 0) {
        for (var i = 0; i < data.partner_star.length; i++) {
            $('#ep7_pstar option[value="' + data.partner_star[i].star.id + '"]').attr("selected", true);
        }
    }

    //country validation
    if (data.partner_country.length != 0) {
        for (var i = 0; i < data.partner_country.length; i++) {
            $('#ep7_pcountry option[value="' + data.partner_country[i].country.id + '"]').attr("selected", true);
        }
    }

    //citizenship validation
    if (data.partner_citizenship.length != 0) {
        for (var i = 0; i < data.partner_citizenship.length; i++) {
            $('#ep7_pcitizenship option[value="' + data.partner_citizenship[i].citizenship.id + '"]').attr("selected", true);
        }
    }

    //education , occupation , income type , incomevalue
    if (data.partner_education.length != 0) {
        for (var i = 0; i < data.partner_education.length; i++) {
            $('#ep7_peducation option[value="' + data.partner_education[i].education.id + '"]').attr("selected", true);
        }
    }

    if (data.partner_occupation.length != 0) {
        for (var i = 0; i < data.partner_occupation.length; i++) {
            $('#ep7_poccupation option[value="' + data.partner_occupation[i].occupation.id + '"]').attr("selected", true);
        }
    }

    if (data.partner_description != null) {
        $('#ep7_pincomeincurrency option[value="' + data.partner_description.currency.id + '"]').attr("selected", true);
        $('#ep7_pincomeinvalues option[value="' + data.partner_description.partner_income + '"]').attr("selected", true);
        $("#ep7_paboutpartner").val(data.partner_description.about_partner);
        $(".totalcharacters").text(data.partner_description.about_partner.length);
         $(".helpmewritethis").hide();
    }else{
        $(".helpmewritethis").show();
    }

   
      
}

//click - error removing fn
$(".ep7_form").click(function() {
    $("input,select,textarea").removeClass("iserror");
    $(".select2-selection__rendered").removeClass("iserror");
});

$("#ep7_paboutpartner").keyup(function() {
    $(".charactertyped").text($("#ep7_paboutpartner").val().length);
});
// ==================================================================================================================


//send edit phase 7data to server
function editp7_fn() {

    //start age and end age - validation
    if ($("#ep7_pstartage").val() == "null") {
        $("#snackbarerror").text("Start Age is required");
        $('#select2-ep7_pstartage-container').addClass("iserror");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#ep7_pendage").val() == "null") {
        $("#snackbarerror").text("End Age is required");
        $('#select2-ep7_pendage-container').addClass("iserror");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if (parseFloat($("#ep7_pstartage").val()) == parseFloat($("#ep7_pendage").val())) {
        $("#snackbarerror").text("Start age and End age should not be same");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if (parseFloat($("#ep7_pendage").val()) < parseFloat($("#ep7_pstartage").val())) {
        $("#snackbarerror").text("End Age Should be higher than Start Age");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        $('#select2-ep7_pendage-container').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    //height validation
    if ($("#ep7_pstartheight").val() == "null") {
        $("#snackbarerror").text("Start Height is required");
        $('#select2-ep7_pstartheight-container').addClass("iserror");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#ep7_pendheight").val() == "null") {
        $("#snackbarerror").text("End Height is required");
        $('#select2-ep7_pendheight-container').addClass("iserror");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if (parseFloat($("#ep7_pstartheight").val()) == parseFloat($("#ep7_pendheight").val())) {
        $("#snackbarerror").text("Start height and End height should not be same");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if (parseFloat($("#ep7_pendheight").val()) < parseFloat($("#ep7_pstartheight").val())) {
        $("#snackbarerror").text("End height Should be higher than Start height");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        $('#select2-ep7_pendheight-container').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    //weight validation
    if ($("#ep7_pstartweight").val() == "null") {
        $("#snackbarerror").text("Start Weight is required");
        $('#select2-ep7_pstartweight-container').addClass("iserror");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#ep7_pendweight").val() == "null") {
        $("#snackbarerror").text("End Weight is required");
        $('#select2-ep7_pendweight-container').addClass("iserror");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if (parseFloat($("#ep7_pstartweight").val()) == parseFloat($("#ep7_pendweight").val())) {
        $("#snackbarerror").text("Start Weight and End Weight should not be same");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if (parseFloat($("#ep7_pendweight").val()) < parseFloat($("#ep7_pstartweight").val())) {
        $("#snackbarerror").text("End Weight Should be higher than Start Weight");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        $('#select2-ep7_pendweight-container').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    //marital status
    if ($('input[name="ep7_pmaritalstatus"]:checked').length == 0) {
        $("#snackbarerror").text("Marital Status is required");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    //physical status , eating habits , drinking habits , smoking habits
    if ($('input[name="ep7_pphysicalstatus"]:checked').length == 0) {
        $("#snackbarerror").text("Physical Status is required");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('input[name="ep7_peatinghabits"]:checked').length == 0) {
        $("#snackbarerror").text("Eating habits is required");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('input[name="ep7_pdrinkinghabits"]:checked').length == 0) {
        $("#snackbarerror").text("Drinking habits is required");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('input[name="ep7_psmokinghabits"]:checked').length == 0) {
        $("#snackbarerror").text("Smoking habits is required");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    //religion , mother tongue , caste , star , manglik

    if ($("#ep7_preligion").val().length == 0) {
        $("#snackbarerror").text("Religion is required");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#ep7_pmothertongue").val().length == 0) {
        $("#snackbarerror").text("Mother Tongue is required");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#ep7_pcast").val() == "null") {
        $("#snackbarerror").text("Caste is required");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#ep7_pstar").val().length == 0) {
        $("#snackbarerror").text("Star is required");
        $("html, body").animate({ scrollTop: 400 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('input[name="ep7_pmanglik"]:checked').length == 0) {
        $("#snackbarerror").text("Manglik is required");
        $("html, body").animate({ scrollTop: 400 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    //country and citizenship validation
    if ($("#ep7_pcountry").val().length == 0) {
        $("#snackbarerror").text("Country is required");
        $("html, body").animate({ scrollTop: 550 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#ep7_pcitizenship").val().length == 0) {
        $("#snackbarerror").text("Citizenship is required");
        $("html, body").animate({ scrollTop: 550 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    //education , occupation
    if ($("#ep7_peducation").val().length == 0) {
        $("#snackbarerror").text("Education is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#ep7_poccupation").val().length == 0) {
        $("#snackbarerror").text("Occupation is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($("#ep7_pincomeincurrency").val() == '' || $("#ep7_pincomeincurrency").val() == null || $("#ep7_pincomeincurrency").val() == "null") {
        $("#snackbarerror").text("Currency Type is required");
        $('#select2-ep7_pincomeincurrency-container').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#ep7_pincomeinvalues").val() == '' || $("#ep7_pincomeinvalues").val() == null || $("#ep7_pincomeinvalues").val() == "null") {
        $("#snackbarerror").text("Income Type is required");
        $('#select2-ep7_pincomeinvalues-container').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($("#ep7_paboutpartner").val() == "") {
        $("#snackbarerror").text("About Partner is required");
        $("#ep7_paboutpartner").addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $('.loadericon').show();
    $(".updateBtn").attr("disabled", true);

    for (var i = 0; i < $('input[name="ep7_pmaritalstatus"]:checked').length; i++) {
        maritalstatus.push($('[name="ep7_pmaritalstatus"]:checked').eq(i).val());
    }

    if ($('[name="ep7_willingtomarryothers"]').is(":checked") == true) {
        var willingtomaaryothers = 1;
    } else {
        var willingtomaaryothers = 0;
    }

    var Datatosend = {
        "from_age": parseFloat($("#ep7_pstartage").val()),
        "to_age": parseFloat($("#ep7_pendage").val()),
        "from_height": parseFloat($("#ep7_pstartheight").val()),
        "to_height": parseFloat($("#ep7_pendheight").val()),
        "from_weight": parseFloat($("#ep7_pstartweight").val()),
        "to_weight": parseFloat($("#ep7_pendweight").val()),
        "marital_status": maritalstatus,
        "physical_status": $('[name="ep7_pphysicalstatus"]:checked').val(),
        "eating_habit": $('[name="ep7_peatinghabits"]:checked').val(),
        "drinking_habit": $('[name="ep7_pdrinkinghabits"]:checked').val(),
        "smoking_habit": $('[name="ep7_psmokinghabits"]:checked').val(),
        "partner_religion": $("#ep7_preligion").val(),
        "partner_mother_tongue": $("#ep7_pmothertongue").val(),
        "caste": $("#ep7_pcast").val(),
        "partner_star": $("#ep7_pstar").val(),
        "is_willing": willingtomaaryothers,
        "manglik": $('[name="ep7_pmanglik"]:checked').val(),
        "partner_country": $("#ep7_pcountry").val(),
        "partner_citizenship": $("#ep7_pcitizenship").val(),
        "partner_education": $("#ep7_peducation").val(),
        "partner_occupation": $("#ep7_poccupation").val(),
        "currency": $("#ep7_pincomeincurrency").val(),
        "partner_income": $("#ep7_pincomeinvalues").val(),
        "about_partner": $("#ep7_paboutpartner").val()
    }


    if (willingtomaaryothers == 0) {
        delete Datatosend.willingtomarry;
    }

    var postData = JSON.stringify(Datatosend);

    $.ajax({
        url: sendeditp7data_api,
        type: 'PUT',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {

            $('.loadericon').hide();
            $(".updateBtn").attr("disabled", false);

        },
        error: function(data) {

            $('.loadericon').hide();
            $(".updateBtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {
        // alert("success")
        window.location.href = "myprofile.html";
    });

} //send edit phase 7data to server
