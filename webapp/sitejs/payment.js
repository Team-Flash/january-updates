$(function() {
    packagelist();
});

//list requests
function packagelist() {
    $.ajax({
        url: listPackages_api,
        type: "GET",
        headers: {
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".packagelist").empty();
            if (data.length == 0) {
                $(".requestpagelist").append(`<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><center><img src="img/assets/nodata.svg" style="width:40%"></center></div>`);
            } else {
                for (var i = 0; i < data.length; i++) {
                    $(".packagelist").append(`
                         <div class="col-sm-6 col-md-4">
                            <div class="pricing pricing-1 boxed boxed--lg boxed--border">
                                <h3 class="packagenametxt packagename${data[i].id}">${data[i].name}</h2>
                                <span class="label">50% Off</span>
                                <div class="payonlytxt"> Rs. ${2 * data[i].cost}</div>
                                <span class="h2 packageamt">
                                <span class="payonlytxt"></span><strong>Rs. ${data[i].cost}</strong>
                                </span>
                                <hr>
                                <ul>
                                    <li>
                                        <span class="checkmark bg--primary-1"></span>
                                        <span>Duration ${data[i].validity} Months</span>
                                    </li>
                                    <li>
                                        <span class="checkmark bg--primary-1"></span>
                                        <span>Access Upto ${data[i].view_count} Numbers</span>
                                    </li>
                                    <li>
                                        <span class="checkmark bg--primary-1"></span>
                                        <span>Unlimited Chats</span>
                                    </li>
                                    <li>
                                        <span class="checkmark bg--primary-1"></span>
                                        <span>Send Messages</span>
                                    </li>
                                    <li>
                                        <span class="checkmark bg--primary-1"></span>
                                        <span>Recieve Messages</span>
                                    </li>
                                    <li>
                                        <span class="checkmark bg--primary-1"></span>
                                        <span>View Unlimited Photos</span>
                                    </li>
                                    <li>
                                        <span class="checkmark bg--primary-1"></span>
                                        <span>Request Photo/ Phone Number</span>
                                    </li>
                                </ul>
                                <a class="btn btn--primary" onclick="pay(${data[i].id});">
                                    <span class="btn__text">
                                        Purchase Plan
                                    </span>
                                </a>
                            </div>
                        </div>
                    `);
                }
            }
        },
        error: function(data) {
            $(".packagelist").empty();
            $(".packagelist").append(`<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><center><img src="img/assets/nodata.svg" style="width:40%"></center></div>`);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}

function pay(id) {
    $.ajax({
        url: payment_api,
        type: "post",
        headers: {
            "content-type": 'application/json',
            "Authorization": "token " + localStorage.wutkn
        },
        data: JSON.stringify({ package: id }),
        success: function(data) {
            console.log(data);
            //razor pay fn starts here
            var options = {
                "key": "rzp_live_D74LBLsG3UXPAN",
                "amount": data.amount,
                "name": "Beautiful Matrimony",
                "description": $(".packagename" + id).text(),
                "image": "https://www.beautifulmatrimony.com/img/assets/bm-123.svg",
                "order_id": data.order_id,
                handler: function(response) {
                    // console.log(response);
                    console.log(response.razorpay_payment_id);
                    console.log(response.razorpay_order_id);
                    console.log(response.razorpay_signature);

                    var postData = JSON.stringify({
                        "razorpay_payment_id": response.razorpay_payment_id,
                        "razorpay_order_id": response.razorpay_order_id,
                        "razorpay_signature": response.razorpay_signature,
                        "matrimony_id": id
                    });

                    $.ajax({
                        url: paymentsuccess_api,
                        type: 'POST',
                        data: postData,
                        headers: {
                            "content-type": 'application/json',
                            "Authorization": "Token " + localStorage.wutkn
                        },
                        success: function(pdata) {
                            $("#snackbarsuccs").text("Payment Success");
                            showsuccesstoast();
                        },
                        error: function(pdata) {
                            console.log("Error Occured in payment after razorpay");
                            var errtext = "";
                            for (var key in JSON.parse(pdata.responseText)) {
                                errtext = JSON.parse(pdata.responseText)[key][0];
                            }
                            $("#snackbarerror").text('Payment failure');
                            showerrtoast();
                        }
                    });
                },
                "prefill": {
                    "uid": JSON.parse(localStorage.myprofiledata).id,
                    "contact": JSON.parse(localStorage.myprofiledata).username,
                    "name": JSON.parse(localStorage.myprofiledata).first_name + " " + JSON.parse(localStorage.myprofiledata).last_name,
                    "email": JSON.parse(localStorage.myprofiledata).email
                },
                "notes": {
                    "address": "Matrimony payment"
                },
                "theme": {
                    "color": "#0058ae"
                }
            };

            rzp1 = new Razorpay(options);

            rzp1.open();
        },
        error: function(data) {
            console.log(data);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}