$(function() {

    $(".loadericon").hide();

    // if (localStorage.editphase2data == undefined) {
    //     geteditphase2data();
    // } else {
    //     var currentdate = new Date().getDate();
    //     if (currentdate != localStorage.datastoreddate_ep2) {
    //         localStorage.removeItem("editphase2data");
    //         geteditphase2data();
    //     } else {
    //         geteditp2datafromlocal(1); //this fn can happen 
    //     }
    // }

    usergivendata(); // user given data

});


//get edit phase 2 data from server
// function geteditphase2data() {

//     $.ajax({
//         url: editprofilep2_dd_api,
//         type: 'get',
//         headers: {
//             "content-type": 'application/json',
//             "Authrization": 'Token ' + localStorage.token + ''
//         },
//         success: function(data) {

//             localStorage.editphase2data = JSON.stringify(data);
//             localStorage.datastoreddate_ep2 = new Date().getDate();

//             geteditp2datafromlocal(0); //this fn is common for all
//         },
//         error: function(edata) {
//             console.log("error occured in get edit phase 2 dd and others data");
//         }
//     });

// } //get register phase 2 data from server


$("#ep2_height").change(function() {


    if ($("#ep2_height").val() != "null") {
        var userheight = $("#ep2_height").val().split(".");
        var heightincms = parseInt(((parseInt(userheight[0]) * 12) + parseInt(userheight[1])) * 2.54);
        $("#ep2_heightincms option").removeAttr("selected");
        $("#ep2_heightincms option[value='" + heightincms + "']").attr("selected", "selected");
        $("#select2-ep2_heightincms-container").text(heightincms + " cms");
    } else {
        $("#ep2_heightincms option").removeAttr("selected");
        $("#ep2_heightincms option[value='" + null + "']").attr("selected", "selected");
        $("#select2-ep2_heightincms-container").text("Your Height in cms");
    }


});

$("#ep2_heightincms").change(function() {

    if ($("#ep2_heightincms").val() != "null") {
        var realFeet = ((parseInt($("#ep2_heightincms").val()) * 0.393700) / 12);
        var feet = Math.floor(realFeet);
        var inches = Math.round((realFeet - feet) * 12);

        if (inches.toString().length == 1) {
            var inchescustom = "0" + inches;
        } else {
            var inchescustom = inches;
        }

        var feetandinches = feet + "." + inchescustom;
        console.log(feetandinches);
        $("#ep2_height option").removeAttr("selected");
        $("#ep2_height option[value='" + feetandinches + "']").attr("selected", "selected");
        $("#select2-ep2_height-container").text(feet + "ft " + inchescustom + " in");
    } else {
        $("#ep2_height option").removeAttr("selected");
        $("#ep2_height option[value='" + null + "']").attr("selected", "selected");
        $("#select2-ep2_height-container").text("Select Your Height");
    }

});

function checkheightincms() {
    var heightincms = ((parseInt($("#ep2_height").val()) * 0.393700) / 12);

    $("#ep2_heightincms option").removeAttr("selected");
    $("#ep2_heightincms option[value='" + heightincms + "']").attr("selected", "selected");
    $("#select2-ep2_heightincms-container").text(heightincms);
}

//get edit phase 2 details from local
function usergivendata(type) {

    var data = JSON.parse(localStorage.myprofiledata);

    //marital status and no of children condition
    if (data.user_personal_details != null) {

        $('input:radio[name="ep2_mstatus"]').filter('[value="' + data.user_personal_details.marital_status.id + '"]').attr('checked', true);

        if (data.user_personal_details.marital_status.name != "Never Married") {
            $(".childrensection").show();
            $('input:radio[name="ep2_nofchildren"]').filter('[value="' + data.user_personal_details.number_of_children + '"]').attr('checked', true);
        } else {
            $(".childrensection").hide();
        }
    }

    //height cond
    if (data.user_personal_details != null) {

        $("#ep2_height option[value='" + data.user_personal_details.height + "']").attr("selected", "selected");

        var heightintext = data.user_personal_details.height.toString().split(".");
        if (heightintext.length == 2) {
            heightintext = heightintext[0] + " ft " + heightintext[1] + " in";
        } else {
            heightintext = heightintext[0] + " ft ";
        }
        $("#select2-ep2_height-container").text(heightintext);

        var heightincms = parseInt(((parseInt(data.user_personal_details.height.split(".")[0]) * 12) + parseInt(data.user_personal_details.height.split(".")[1])) * 2.54);




        $("#ep2_heightincms option").removeAttr("selected");
        $("#ep2_heightincms option[value='" + heightincms + "']").attr("selected", "selected");
        $("#select2-ep2_heightincms-container").text(heightincms + "cm");

    }

    //family status , values, type and person disablity cond
    if (data.user_personal_details != null) {
        $('input:radio[name="ep2_familystatus"]').filter('[value="' + data.user_personal_details.family_status.id + '"]').attr('checked', true);
        $('input:radio[name="ep2_familytype"]').filter('[value="' + data.user_personal_details.family_type + '"]').attr('checked', true);
        $('input:radio[name="ep2_familyvalue"]').filter('[value="' + data.user_personal_details.family_values.id + '"]').attr('checked', true);
        $('input:radio[name="ep2_anydisability"]').filter('[value="' + data.user_personal_details.is_physically_challenged + '"]').attr('checked', true);
    }

} //get edit phase 2 details from local

$(".editphase2form").click(function() {
    $(".editphase2form input,.editphase2form select,.editphase2form textarea").removeClass("iserror");
    $(".select2-selection__rendered").removeClass("iserror");
});


//update fn phase one starts here
function editp2_fn() {

    if ($('input[name="ep2_mstatus"]:checked').length == 0) {
        $("#snackbarerror").text("Marital Status is required");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('input[name="ep2_mstatus"]:checked').val() != "1" && $('input[name="ep2_nofchildren"]:checked').length == 0) {
        $("#snackbarerror").text("No of Childrens is required");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($("#ep2_height").val() == 'null') {
        $("#snackbarerror").text("Height is required");
        $('#select2-ep2_height-container').addClass("iserror");
        $("html, body").animate({ scrollTop: 650 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('input[name="ep2_familystatus"]:checked').length == 0) {
        $("#snackbarerror").text("Family Status is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('input[name="ep2_familytype"]:checked').length == 0) {
        $("#snackbarerror").text("Family Type is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('input[name="ep2_familyvalue"]:checked').length == 0) {
        $("#snackbarerror").text("Family Value is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('input[name="ep2_anydisability"]:checked').length == 0) {
        $("#snackbarerror").text("Any Disability is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $('.loadericon').show();
    $(".updateBtn").attr("disabled", true);

    var Datatosend = {
        "marital_status": $('[name="ep2_mstatus"]:checked').val(),
        "number_of_children": $('[name="ep2_nofchildren"]:checked').val(),
        "height": parseFloat($("#ep2_height").val()),
        "family_status": $('[name="ep2_familystatus"]:checked').val(),
        "family_type": $('[name="ep2_familytype"]:checked').val(),
        "family_values": $('[name="ep2_familyvalue"]:checked').val(),
        "is_physically_challenged": $('[name="ep2_anydisability"]:checked').val()
    }

    if ($('input[name="ep2_mstatus"]:checked').val() == "Never Married") {
        delete Datatosend.number_of_children;
    }

    var postData = JSON.stringify(Datatosend);

    $.ajax({
        url: sendeditp2data_api,
        type: 'PUT',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {

            $('.loadericon').hide();
            $(".updateBtn").attr("disabled", false);

        },
        error: function(data) {

            $('.loadericon').hide();
            $(".updateBtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {
        // alert("success");
        window.location.href = "myprofile.html";
    });

} //update fn phase one ends here
