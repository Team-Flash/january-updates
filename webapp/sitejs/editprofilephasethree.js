$(function() {

    $(".loadericon").hide();

    if (localStorage.editphase3data == undefined) {
        geteditphase3data();
    } else {
        var currentdate = new Date().getDate();
        if (currentdate != localStorage.datastoreddate_ep3) {
            localStorage.removeItem("editphase3data");
            geteditphase3data();
        } else {
            geteditp3datafromlocal(1); //this fn can happen 
        }
    }

    usergivendata();

});


//get edit phase 3 data from server
function geteditphase3data() {

    $.ajax({
        url: editprofilep3_dd_api,
        type: 'get',
        headers: {
            "content-type": 'application/json'
        },
        success: function(data) {

            localStorage.editphase3data = JSON.stringify(data);
            localStorage.datastoreddate_ep3 = new Date().getDate();

            geteditp3datafromlocal(0); //this fn is common for all
        },
        error: function(edata) {
            console.log("error occured in get edit phase 3 dd and others data");
        }
    });

} //get register phase 2 data from server

//get edit phase 3 details dynamic loading
function geteditp3datafromlocal(type) {

    var data = JSON.parse(localStorage.editphase3data);

    //highest education dyn append
    $('#ep3_education').empty();
    for (var i = 0; i < data.education.length; i++) {

        $('#ep3_education').append("<optgroup class='a educationsubmenu" + data.education[i].id + "' label='" + data.education[i].name + "'></optgroup>");

        for (var j = 0; j < data.education[i].branch.length; j++) {
            if (i == 0 && j == 0) {
                $(".educationsubmenu" + data.education[i].id).append("<option value='null'>Select Your Education</option>");
            } else {
                $(".educationsubmenu" + data.education[i].id).append("<option value='" + data.education[i].branch[j].id + "'>" + data.education[i].branch[j].name + "</option>");
            }

        }
    }

    //occupation dyn append
    $('#ep3_occupation').empty();
    for (var i = 0; i < data.occupation_sector.length; i++) {

        $('#ep3_occupation').append("<optgroup class='a occupationsubmenu" + data.occupation_sector[i].id + "' label='" + data.occupation_sector[i].name + "'></optgroup>");

        for (var j = 0; j < data.occupation_sector[i].occupation.length; j++) {
            if (i == 0 && j == 0) {
                $(".occupationsubmenu" + data.occupation_sector[i].id).append("<option value='null'>Select Your Occupation</option>");
            } else {
                $(".occupationsubmenu" + data.occupation_sector[i].id).append("<option value='" + data.occupation_sector[i].occupation[j].id + "'>" + data.occupation_sector[i].occupation[j].name + "</option>");
            }


        }
    }

    //currency dyn append
    $('#ep3_currency').empty().append("<option value='null'>Select Your Currency Type</option>");
    for (var i = 0; i < data.currency.length; i++) {
        $('#ep3_currency').append("<option value='" + data.currency[i].id + "'>" + data.currency[i].name + "</option>");
    }
    $('#ep3_currency option[value="98"]').attr("selected", true);

} //get edit phase 3 details dynamic loading 

//get edit phase 3 details from local
function usergivendata() {

    var data = JSON.parse(localStorage.myprofiledata);

    if (data.user_professional_details != null) {
        $("#ep3_education option[value='" + data.user_professional_details.highest_education.id + "']").attr("selected", "selected");
        $('input:radio[name="employedin"]').filter('[value="' + data.user_professional_details.employed_in.id + '"]').attr('checked', true);
        $("#ep3_occupation option[value='" + data.user_professional_details.occupation.id + "']").attr("selected", "selected");
        $("#ep3_currency option[value='" + data.user_professional_details.currency.id + "']").attr("selected", "selected");
        $("#ep3_incomevalue option[value='" + data.user_professional_details.annual_income + "']").attr("selected", "selected");
    }

}

//click - error removing fn
$(".ep3_form").click(function() {
    $("input,select,textarea").removeClass("iserror");
    $(".select2-selection__rendered").removeClass("iserror");
});

//update fn phase 3 starts here
function editp3_fn() {

    if ($("#ep3_education").val() == '' || $("#ep3_education").val() == null || $("#ep3_education").val() == "null") {
        $("#snackbarerror").text("Highest Education is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('input[name="employedin"]:checked').length == 0) {
        $("#snackbarerror").text("Employed In is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($("#ep3_occupation").val() == '' || $("#ep3_occupation").val() == null || $("#ep3_occupation").val() == "null") {
        $("#snackbarerror").text("Occupation is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($("#ep3_currency").val() == '' || $("#ep3_currency").val() == null || $("#ep3_currency").val() == "null") {
        $("#snackbarerror").text("Currency Type is required");
        $('#select2-ep3_currency-container').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#ep3_incomevalue').val() == "" || $("#ep3_incomevalue").val() == null || $("#ep3_incomevalue").val() == "null") {
        $("#snackbarerror").text("Income Type is required");
        $('#ep3_incomevalue').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    $('.loadericon').show();
    $(".updateBtn").attr("disabled", true);


    var Datatosend = {
        "highest_education": $("#ep3_education").val(),
        "employed_in": $('[name="employedin"]:checked').val(),
        "occupation": $("#ep3_occupation").val(),
        "currency": $("#ep3_currency").val(),
        "annual_income": $("#ep3_incomevalue").val()
    }
    var postData = JSON.stringify(Datatosend);

    $.ajax({
        url: sendeditp3data_api,
        type: 'PUT',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {

            $('.loadericon').hide();
            $(".updateBtn").attr("disabled", false);

        },
        error: function(data) {

            $('.loadericon').hide();
            $(".updateBtn").attr("disabled", false);

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {
        // alert("success");
        window.location.href = "myprofile.html";
    });

} //update fn phase 3 starts here