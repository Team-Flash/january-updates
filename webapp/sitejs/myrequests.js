$(function() {
	$(".switchtab"+sessionStorage.requestpagetab).addClass("activeunderline");
    listrequestpage(requestspage_api + sessionStorage.requestpagetab);
});

var page_no = 1;

//list requests
function listrequestpage(Url) {
    $.ajax({
        url: Url,
        type: "GET",
        headers: {
            "Authorization": "Token " + localStorage.wutkn
        },
        success: function(data) {
            $(".requestpagelist").empty();
            if (data.results.length == 0) {
                $(".dynpagination").empty();
                $(".requestpagelist").append(`<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><center><img src="img/assets/nodata.svg" style="width:40%"></center></div>`);
            } else {
                for (var i = 0; i < data.results.length; i++) {
                    $(".requestpagelist").append(`
						<div class="col-lg-2 col-md-2 col-sm-3 col-xs-6">
	                        <div class="card card-2 text-center">
	                            <div class="card__top">
	                                <a class="pointer" onclick="visitProfile(${data.results[i].id});" style="background: url(${data.results[i].userprofile.profile_pic?data.results[i].userprofile.profile_pic:'img/assets/coupleindex.svg'}) top center / cover no-repeat;display: block;height: 140px;">
	                                </a>
	                            </div>
	                            <div class="card__body p10" ${Number(sessionStorage.requestpagetab)==2||Number(sessionStorage.requestpagetab)==4?'style="border-bottom: 1px solid #ececec;"':''}>
	                                <p class="username">${data.results[i].first_name} ${data.results[i].last_name}</p>
	                                <p class="livesin">Lives in <span class="spanlivesin">${data.results[i].user_address?data.results[i].user_address.city.name:'- -'}</span></p>
	                            </div>
	                            <div class="card__bottom text-center ptb3" ${Number(sessionStorage.requestpagetab)==2||Number(sessionStorage.requestpagetab)==4?'style="display:none;"':''}>
	                                <div class="card__action accept_margin">
	                                    <a class="roun_acc pointer" onclick="acceptuserrequest(${data.results[i].id},${data.results[i].uid},1);">
	                                        <i class="fa fa-check accepticon raccepted${data.results[i].id}" aria-hidden="true" style="color: ${data.results[i].status=='True'?"#5cdc5c":"white"};"></i>
	                                    </a>
	                                </div>
	                                <div class="card__action deny_margin">
	                                    <a href="#" class="roun_acc" onclick="acceptuserrequest(${data.results[i].id},${data.results[i].uid},0);">
	                                        <i class="fa fa-times rejecticon rdenied${data.results[i].id}" aria-hidden="true" style="color: ${data.results[i].status=='False'?"#5cdc5c":"white"};"></i>
	                                    </a>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
					`);
                }
                pagination(data.next_url, data.prev_url, data.count, data.page_max_size);
            }
        },
        error: function(data) {
            $(".requestpagelist").empty();
            $(".dynpagination").empty();
            $(".requestpagelist").append(`<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><center><img src="img/assets/nodata.svg" style="width:40%"></center></div>`);
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}



//visit other people profiles fn starts here
function visitProfile(id) {
    localStorage.userid_toseeprof = id;
    window.open("profile.html", '_blank');
}


//switch tab
function switchtab(id) {
	$(".switchtab").removeClass("activeunderline");
	$(".switchtab"+id).addClass("activeunderline");
    page_no = 1;
    sessionStorage.requestpagetab = id;
    listrequestpage(requestspage_api + id);
} //function ends here


//pagination starts here
function pagination(next, prev, count, size) {
    $(".dynpagination").empty();
    var pages = Math.ceil(count / size);
    if (pages > 1) {
        prev ? $(".dynpagination").append(`<li class="cptr mright10 pageprev"><a class="pointer cptr font12 paginationtext" onclick="prevpage('${prev}')">« Previous</a></li>`) : '';
        for (var i = (page_no < 3 ? 1 : page_no - 2); i <= pages && i <= (page_no < 3 ? page_no + (5 - page_no) : page_no + 2); i++) {
            $(".dynpagination").append(`<li class="mright10 pgs pg${i}"><a class="pointer font12 paginationtext" onclick="navigate(${i})">${i}</a></li>`);
        }
        next ? $(".dynpagination").append(`<li class="mright10 pagenext"><a class="pointer cptr font12 paginationtext" onclick="nextpage('${next}')">Next »</a></li>`) : '';
    }
    $(".pgs").removeClass("active");
    $(".pg" + page_no).addClass("active");
    $(".pg" + page_no+" a").addClass("pageactive"); 
}

function navigate(page) {
    page_no = Number(page);
    listrequestpage(requestspage_api + sessionStorage.requestpagetab + "&page=" + page_no);
}

function nextpage(next) {
    page_no++;
    listrequestpage(next);
}

function prevpage(prev) {
    page_no--;
    listrequestpage(prev);
}
//pagination ends here

function acceptuserrequest(id, uid, stat) {
    var postData = JSON.stringify({
        status: stat
    });
    $.ajax({
        url: Number(sessionStorage.requestpagetab) == 1 ? `${acceptusernumberrequest_api+uid}/` : `${acceptuserphotorequest_api+uid}/`,
        type: "put",
        data: postData,
        headers: {
            "Authorization": "Token " + localStorage.wutkn,
            "content-type": "application/json"
        },
        success: function(data) {
            if (stat == 1) {
                $(".raccepted" + id).css("color", "#5cdc5c");
                $(".rdenied" + id).css("color", "white");
                $("#snackbarsuccs").text("You have Accepted Request");
            } else {
                $(".raccepted" + id).css("color", "white");
                $(".rdenied" + id).css("color", "#5cdc5c");
                $("#snackbarsuccs").text("You have Denied Request");
            }
            showsuccesstoast();
        },
        error: function(data) {
            for (var key in JSON.parse(data.responseText)) {
                $("#snackbarerror").text(JSON.parse(data.responseText)[key] != undefined ? JSON.parse(data.responseText)[key] : JSON.parse(data.responseText)[key].non_field_errors[0]);
            }
            showerrtoast();
        }
    });
}