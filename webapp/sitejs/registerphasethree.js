$(function() {

    $('.loadericon').hide();

}); //initial fn starts here

//userprofile pics fn starts here
function userprofilepicsfn(e) {

    var files = e.files;
    $(".dynamicprofimages").empty();
    if (files.length != 0) {
        for (var i = 0; i <= files.length; i++) {
            var file = files[i];
            var reader = new FileReader();

            reader.onload = (function(file) {
                return function(event) {
                    $(".dynamicprofimages").append("<div class='col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-12 mt10 paddlr3'> <div class='hovereffectclose'> <img class='img-responsive responsiveimg profileselectedimage profilepicupimg' src='" + event.target.result + "' alt='' style='height:150px;'> <div class='overlay'> <a class='info' href='#'><i class='font-icon-close-2'></i></a> </div></div></div>")
                };
            })(file);

            reader.readAsDataURL(file);
        } // end for;
    } else {
        $(".dynamicprofimages").append("<div class='col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mt10 paddlr3'> <div class='hovereffectclose'> <img class='img-responsive responsiveimg profileselectedimage profilepicupimg' src='img/assets/noimgbg.jpg' alt='' style='height:150px;'> <div class='overlay'> <a class='info' href='#'><i class='font-icon-close-2'></i></a> </div></div></div>")
    }

};

//save and contineu phase 3 fn starts here
function savereg_phase3() {

    var postData = new FormData();
    var userprofimage = $("#userprofilepics")[0].files;

    if (userprofimage.length == 0) {
        $("#snackbarerror").text("Atleast one profile photo is required");
        showiperrtoast();
        event.stopPropagation();
        return;
    } else {
        for (var i = 0; i < userprofimage.length; i++) {
            postData.append("images", userprofimage[i]);
        }
    }

    postData.append("privacy_status", $('[name="rp3_photoprivacy"]:checked').val());
    postData.append("is_number_public", $('[name="rp3_phonenoprivacy"]:checked').val());

    $('.loadericon').show();
    $(".saveBtn").attr("disabled", true);

    $.ajax({
        url: sendrp3data_api,
        type: 'POST',
        data: postData,
        crossDomain: true,
        contentType: false,
        processData: false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Authorization", "Token " + localStorage.wutkn)
        },
        success: function(data) {
            $('.loadericon').hide();
            $(".saveBtn").attr("disabled", false);
        },
        error: function(data) {

            $('.loadericon').hide();
            $(".saveBtn").attr("disabled", false);
            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {
        localStorage.wuupldedpropic = dataJson.image;
        // window.location.href = "register-phase4.html";
        window.location.replace("register-phase4.html");
    });
} //save and contineu phase 3 fn starts here
