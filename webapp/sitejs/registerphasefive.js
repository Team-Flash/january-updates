var starone, raasione, parentstatusone;
var parentphoneno = [];
$(function() {

    $(".loadericon").hide();

    if (localStorage.registerphase5data == undefined) {
        getregphase5data();
    } else {
        var currentdate = new Date().getDate();
        if (currentdate != localStorage.datastoreddate) {
            localStorage.removeItem("registerphase5data");
            getregphase5data();
        } else {
            getregp5datafromlocal(1); //this fn can happen 
        }
    }

}); //initial fn ends here

//get register phase 1 data from server
function getregphase5data() {

    $.ajax({
        url: getregp5data_api,
        type: 'get',
        success: function(data) {

            localStorage.registerphase5data = JSON.stringify(data);
            localStorage.datastoreddate_p5 = new Date().getDate();

            getregp5datafromlocal(0); //this fn is common for all
        },
        error: function(edata) {
            console.log("error occured in get register phase 5 dd data");
        }
    });

} //get register phase 1 data from server

//get phaseone details from local
function getregp5datafromlocal(type) {

    var data = JSON.parse(localStorage.registerphase5data);

    //stars dyn append
    $('#rp5_star').empty().append('<option value="null">Select Your Star</option>');
    $('#rp5_raasi').empty().append('<option value="null">Select Your Raasi</option>');
    for (var i = 0; i < data.star.length; i++) {
        // if (i == 0) {
        //     $('#rp5_star').append("<option selected='' value='" + data.star[i].id + "'>" + data.star[i].name + "</option>");
        //     localStorage.selectedstarcode = data.star[i].id;
        // } else {
        $('#rp5_star').append("<option value='" + data.star[i].id + "'>" + data.star[i].name + "</option>");
        // }
    }
    // getraasilist();

    //parent status dyn append
    $('#rp5_fatherstatus,#rp5_motherstatus').empty();
    $('#rp5_fatherstatus').append('<option value="null">Select Your Father Status</option>');
    $('#rp5_motherstatus').append('<option value="null">Select Your Mother Status</option>');
    for (var i = 0; i < data.employment.length; i++) {
        // if (i == 0) {
        //     $('#rp5_fatherstatus,#rp5_motherstatus').append("<option selected='' value='" + data.employment[i].id + "'>" + data.employment[i].name + "</option>");
        // } else {
        $('#rp5_fatherstatus,#rp5_motherstatus').append("<option value='" + data.employment[i].id + "'>" + data.employment[i].name + "</option>");
        // }
    }
} //get phaseone details from local

//get raasi list fn starts here
function getraasilist() {

    $.ajax({
        url: getregp5raasidata_api + localStorage.selectedstarcode + '/',
        type: 'get',
        success: function(data) {
            //states dyn append
            $('#rp5_raasi').empty();
            for (var i = 0; i < data.length; i++) {
                if (i == 0) {
                    $('#rp5_raasi').append("<option selected='' value='" + data[i].id + "'>" + data[i].name + "</option>");
                } else {
                    $('#rp5_raasi').append("<option value='" + data[i].id + "'>" + data[i].name + "</option>");
                }
            }
        },
        error: function(edata) {
            console.log("error occured in get register phase 5 raasi dd data");
        }
    });
} //get raasi list fn ends here

$("#rp5_star").change(function() {

    if ($("#rp5_star").val() != "null") {
        localStorage.selectedstarcode = $("#rp5_star").val();
        getraasilist();
    } else {
        $('#rp5_raasi').empty().append('<option value="null">Select Your Raasi</option>');
    }

});

$(".registerphase5box").click(function() {
    $("input,select,textarea").removeClass("iserror");
    $(".select2-selection__rendered").removeClass("iserror");
});

//register phase 5 fn starts here
function savereg_phase5() {

    //body type , skin tone , weight
    if ($('input[name="bodytype"]:checked').length == 0) {
        $("#snackbarerror").text("Body Type is required");
        $("html, body").animate({ scrollTop: 300 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('input[name="skintone"]:checked').length == 0) {
        $("#snackbarerror").text("Skin Tone is required");
        $("html, body").animate({ scrollTop: 400 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($('#rp5_weight').val() == 'null' || $('#rp5_weight').val() == null) {
        $("#snackbarerror").text("Weight is required");
        $("html, body").animate({ scrollTop: 400 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    //eating habits , drinking habits , smoking habits
    if ($('input[name="eatinghabit"]:checked').length == 0) {
        $("#snackbarerror").text("Eating habit is required");
        $("html, body").animate({ scrollTop: 400 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('input[name="drinkinhabit"]:checked').length == 0) {
        $("#snackbarerror").text("Drinking habit is required");
        $("html, body").animate({ scrollTop: 400 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }
    if ($('input[name="smokinghabit"]:checked').length == 0) {
        $("#snackbarerror").text("Smoking habit is required");
        $("html, body").animate({ scrollTop: 400 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    //star , raasi 
    if ($("#rp5_star").val() == '' || $("#rp5_star").val() == null || $("#rp5_star").val() == "null") {
        $("#snackbarerror").text("Star is required");
        $('#select2-rp5_star-container').addClass("iserror");
        $("html, body").animate({ scrollTop: 500 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($("#rp5_raasi").val() == '' || $("#rp5_raasi").val() == null || $("#rp5_raasi").val() == "null") {
        $("#snackbarerror").text("Raasi is required");
        $('#select2-rp5_raasi-container').addClass("iserror");
        $("html, body").animate({ scrollTop: 500 }, "slow");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    //father and mother status
    if ($("#rp5_fatherstatus").val() == '' || $("#rp5_fatherstatus").val() == null || $("#rp5_fatherstatus").val() == "null") {
        $("#snackbarerror").text("Father Status is required");
        $('#select2-rp5_fatherstatus-container').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    if ($("#rp5_motherstatus").val() == '' || $("#rp5_motherstatus").val() == null || $("#rp5_motherstatus").val() == "null") {
        $("#snackbarerror").text("Mother Status is required");
        $('#select2-rp5_motherstatus-container').addClass("iserror");
        showiperrtoast();
        event.stopPropagation();
        return;
    }

    //parent phone no

    // if ($(".parentphonenumbers li.addedTag").length == 0) {
    //     $("#snackbarerror").text("Parent's Contact Number is required");
    //     showiperrtoast();
    //     event.stopPropagation();
    //     return;
    // } else {
    //     for (var i = 0; i < $(".parentphonenumbers li.addedTag").length; i++) {
           
    //     }
    // }
     parentphoneno.push("9094483144");

    $(".loadericon").show();
    $(".saveBtn").attr("disabled", true);

    var Datatosend = {
        "basic_information": {
            "body_type": $('[name="bodytype"]:checked').val(),
            "skin_tone": $('[name="skintone"]:checked').val(),
            "weight": parseFloat($("#rp5_weight").val())
        },
        "lifestyle": {
            "eating_habit": $('[name="eatinghabit"]:checked').val(),
            "drinking_habit": $('[name="drinkinhabit"]:checked').val(),
            "smoking_habit": $('[name="smokinghabit"]:checked').val()
        },
        "religion_info": {
            "star": $("#rp5_star").val(),
            "raasi": $("#rp5_raasi").val()
        },
        "family_info": {
            "father_status": $("#rp5_fatherstatus").val(),
            "mother_status": $("#rp5_motherstatus").val(),
            "contact_number": parentphoneno.toString()
        }
    };

    var postData = JSON.stringify(Datatosend);

    $.ajax({
        url: sendrp5data_api,
        type: 'POST',
        data: postData,
        headers: {
            "content-type": 'application/json',
            "Authorization": 'Token ' + localStorage.wutkn + ''
        },
        success: function(data) {
            $(".saveBtn").attr("disabled", false);
            $('.loadericon').hide();
        },
        error: function(data) {

            $(".saveBtn").attr("disabled", false);
            $('.loadericon').hide();

            var errtext = "";
            for (var key in JSON.parse(data.responseText)) {
                errtext = JSON.parse(data.responseText)[key][0];
            }
            $("#snackbarerror").text(errtext);
            showerrtoast();

        }
    }).done(function(dataJson) {
        // parentphoneno = [];
        // window.location.href = "register-phase6.html";
        window.location.replace("register-phase6.html");
    });

} //register phase 5 fn starts here
